package com.l.nailscheckin;

import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import java.util.Arrays;
import java.util.List;

import com.reactnativenavigation.NavigationApplication;
import com.reactnativenavigation.react.NavigationReactNativeHost;
import com.reactnativenavigation.react.ReactGateway;
import com.microsoft.codepush.react.CodePush;
//import com.airbnb.android.react.maps.MapsPackage;
import com.imagepicker.ImagePickerPackage;
import com.azendoo.reactnativesnackbar.SnackbarPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;

import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.zoontek.rnpermissions.RNPermissionsPackage;


public class MainApplication extends NavigationApplication {

    @Override
    protected ReactGateway createReactGateway() {
        ReactNativeHost host = new NavigationReactNativeHost(this, isDebug(), createAdditionalReactPackages()) {
            @Override
            protected String getJSMainModuleName() {
                return "index";
            }

            @Override
            protected String getJSBundleFile() {
                return CodePush.getJSBundleFile();
            }
        };
        return new ReactGateway(this, isDebug(), host);
    }

    @Override
    public boolean isDebug() {
        return BuildConfig.DEBUG;
    }

    protected List<ReactPackage> getPackages() {
        // Add additional packages you require here
        // No need to add RnnPackage and MainReactPackage
        return Arrays.<ReactPackage>asList(
                // new RNCWebViewPackage() account gesodev 
                new CodePush("deT7CrcYdsKLqLuF-WRUQq8uTSydjUNaeX3S1", MainApplication.this, BuildConfig.DEBUG), new RNFusedLocationPackage(),
                //new MapsPackage(),
                new ImagePickerPackage(),
                new SnackbarPackage(),
                new RNCWebViewPackage(),
                new RNPermissionsPackage()
        );
    }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        return getPackages();
    }

}
