
import {
  SHOW_APP,
  SHOW_LOADING,
  SAVE_CURRENT_NPP,
  SAVE_LIST_BRANCH,
} from './action';
const initState = {
  test: 0,
  showApp: false,
  showLoading: false,
  currentNPP: '',
  isConnected: true,
  dataApp:null,
  StaffsList:[],
  ServicesList:[],
  listserveceSearch:[],
  QuickMenu:[]
};

export default function(state = initState, action) {
  switch (action.type) {
    case 'test':
      return {
        ...state,
        test: 10,
      };
    case SHOW_APP:
      return {
        ...state,
        showApp: action.payload,
      };
    case SHOW_LOADING:
      return {
        ...state,
        showLoading: action.payload,
      };
    case SAVE_CURRENT_NPP:
      return {
        ...state,
        currentNPP: action.payload,
      };
    case 'CHANGE_NET_INFO':
      return {
        ...state,
        isConnected: action.value,
      };
    case SAVE_LIST_BRANCH:
      return {
        ...state,
        listBranch: action.payload,
        LoginInfo:action.payload,
      };
    case 'SAVE_DATA_APP':
      return {
        ...state,
        dataApp:action.payload,
      };
    case 'SAVE_DATA_Staffs':
      return {
        ...state,
        StaffsList:action.payload,
      };
    case 'SAVE_DATA_QuickMenu':
    return {
      ...state,
      QuickMenu:action.payload,
    };
    case 'SAVE_DATA_ServicesList':
      return {
        ...state,
        ServicesList:action.payload,
        listserveceSearch:action.payload,
      };
    default:
      return state;
  }
}
