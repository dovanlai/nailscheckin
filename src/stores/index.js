/**
 * @author: thai.nguyen 
 * @date: 2018-11-29 16:41:26 
 *  
 * 
 */
import { combineReducers, } from 'redux';

import { testReducer, testTypes, testActions, testSelectors, } from './test';
import { homeReducer, homeTypes, homeActions, homeSelectors, } from './home';
import { authReducer, authTypes, authActions, authSelectors, } from './auth';
import { accountReducer, accountTypes, accountActions, accountSelectors, } from './account';

import app from './app/index.js';
import SearchData from '../screens/Geso/SearchData/Search.reducer';
const rootReducer = combineReducers({
  test: testReducer,
  home: homeReducer,
  auth: authReducer,
  account: accountReducer,
  app: app,
  SearchData:SearchData,
});

const types = {
  test: testTypes,
  home: homeTypes,
  auth: authTypes,
  account: accountTypes,
};

const actions = {
  test: testActions,
  home: homeActions,
  auth: authActions,
  account: accountActions,
};

const selectors = {
  test: testSelectors,
  home: homeSelectors,
  auth: authSelectors,
  account: accountSelectors,
};

export { rootReducer, types, actions, selectors };