/**
 * @author: thai.nguyen 
 * @date: 2018-11-29 16:34:11 
 *  
 * 
 */
import { Types as testTypes, Actions as testActions, } from './actions';
import testReducer from './reducer';
import * as testSelectors from './selectors';

export { testTypes, testActions, testReducer, testSelectors };