
export const BottomBarAssets = {
  Account: require('./bottom_tab/account.png'),
  home: require('./bottom_tab/home.png'),
  market: require('./bottom_tab/market.png'),
  trade: require('./bottom_tab/trade.png'),
  fund: require('./bottom_tab/fund.png'),
  lock: require('./home/lock.png'),
  khthang: require ('./bottom_tab/khthang.png'),
  khngay:require ('./bottom_tab/khngay.png'),
  bacao:require ('./bottom_tab/baocao.png'),
};

export const CommonIcons = {
  close: require('./common_icon/close.png'),
  back: require('./common_icon/back.png'),
  done:  require('./common_icon/done.png'),
  dots:  require('./common_icon/dots-vertical.png'),
  filter: require ('./common_icon/filter.png'),
  checkok: require ('./common_icon/iconscheck.png'),
 
};
export const HomeICon = {
  volume: require('./home/baseline_volume.png'),
  double: require('./home/double-right.png'),
  support: require('./home/support.png'),
  starbox: require('./home/star-box.png'),
  deposit: require('./home/deposit.png'),
  Withdrawal :require('./home/elevator.png'),
  chessqueen : require('./home/chess-queen.png'),
  right: require('./home/right.png'),
  filepdfbox: require('./home/file-pdf-box.png'),
  send: require('./home/send.png'),
};
export const ImagesIcons = {
  xdtd_w: require('./ings/xdtd_w.png'),
  take: require('./ings/take.png'),
  roidi_w: require('./ings/roidi_w.png'),
  vt_w: require('./ings/vt_w.png'),
  cods: require('./ings/cods.png'),
  davt: require('./ings/davt.png'),
  khach: require('./ings/khach.png'),
  logooneone: require('./ings/logoonone2.png'),
  logonails:require('./ings/logonails.png'),
  downloadApp:require('./ings/downloadApp.png'),
  qrnganhnails:require('./ings/qrnganhnails.png'),
  qrnganhnailsAndroid:require('./ings/qrcode_adcatalog.png'),

  qrnailspa :require('./ings/nailsspa.png'),
  qrnailspaAndroid :require('./ings/qrcodeNailSpaA.png'),
  backspace : require('./ings/backspace.png'),
  deleteKeyboard : require('./ings/delete_keyboear.png'),
  happyicon : require('./ings/happy.png'),
  unhappyicon : require('./ings/unhappy.png'),
  chplayicon : require('./ings/chplay.png'),
  appStoreicon : require('./ings/appstore.png'),
};
