import { createIconSetFromIcoMoon, } from 'react-native-vector-icons';
import icoMoonConfig from './selection.json';
export default Icons = createIconSetFromIcoMoon(icoMoonConfig);