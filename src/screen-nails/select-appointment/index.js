/**
 * @author: thai.nguyen
 * @date: 2018-11-29 16:24:38
 *
 *
 */
import { connect, } from 'react-redux';

import SelectAppointment from './select-appointment-screen';
import { IDs, } from '../../screens';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch, { navigator, }) => {
  const onPressBtn1 = async () => {
    navigator._pop(IDs.Welcome);
  };
  const onPressYes = async (phone) => {
    navigator._push({
      name: IDs.ConfirmAppointmentTime,
      passProps: {
        phone:phone,
      },
    });
  };
  const onPressNo = async (phone,checkin_setting,storeCode,appointment ) => {
    let data = {
      phone: phone,
      Staffs: [],
      checkin_setting: checkin_setting,
      storeCode:storeCode,
      appointment
    };

    if(checkin_setting==='Show Service'||checkin_setting=='Show Quickmenu'){
      return  navigator._push({
        name: IDs.YourService,
        passProps:{
          data:data,
        },
      });
    }

    navigator._push({
      name: IDs.YourNailsTech,
      passProps: {
        phone:phone,
        checkin_setting:checkin_setting,
        storeCode, 
        appointment
      },
    });
  };
  return {
    onPressBtn1,
    onPressYes,
    onPressNo,
    dispatch,
    navigator
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SelectAppointment);
