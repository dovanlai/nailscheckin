import React, { Component, } from 'react';

import { StyleSheet, SafeAreaView, Alert,Modal,View,Text,TouchableOpacity } from 'react-native';

import Label from './Label/index';
import YesNo from './yes-no/index';
import ButtonBottom from '../component-check-in/button-bottom';
import LogoView from '../old-customer/LogoView';
import { AppCheckIn_StoreSetting, } from './../../screens/Nails/Login/action';
import { AppCheckIn_CustomerSignIn ,AppCheckIn_CustomerCheckExits,} from '../../screens/Nails/Login/action';
import { Navigation, } from 'react-native-navigation';
import { IDs, } from '../../screens';
import { showError, } from '../../utils/SnackBar';


export default class SelectAppointment extends Component {
  static options(passprops) {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      checkin_setting: '',
      loading:true,
      showDiaalog2:false,
      sms2:'',
      result:null
    };
  }
  async componentDidMount() {
    //alert(JSON.stringify(this.props.storeCode));
    let params = { storeCode: this.props.storeCode, };
    await this.props.dispatch(AppCheckIn_StoreSetting(params)).then((res) => {
      this.setState({loading:false,});
      if (res) {
        this.setState({ checkin_setting: res.dataArray[2].checkin_setting, });
       // alert(JSON.stringify(res.dataArray[2].checkin_setting));
      }
      else {
        //Alert.alert('','')
        showError('error api');
      }
    });
  

  }
  
  onPressCheckin = (appointment) => {
    //return  Navigation.popToRoot('Welcomeid');
    //alert(JSON.stringify(this.props.data));
    let params = {
      phone: this.props.phone,
      storeCode: this.props.storeCode,
      haveAppt: appointment,
    };

    params.staffId = '';
    params.serviceId = '';

    this.props.dispatch(AppCheckIn_CustomerSignIn(params)).then((result) => {
      //Alert.alert('', result.dataArray[1].ErrorMessege);
      this.setState({result,sms2:result.dataArray[1].ErrorMessege,showDiaalog2:true})
      if (result&& result.dataArray[0].Status == 'Success'){
        setTimeout(() => {
          this.setState({showDiaalog2:false})
          Navigation.popToRoot('Welcomeid');
        }, 3000);
      }
      // Alert.alert(
      //   '',
      //   result.dataArray[1].ErrorMessege,
      //   [
      //     {
      //       text: 'Ok',
      //       onPress: () => {
      //         if (result.dataArray[0].Status == 'Success')
      //         {Navigation.popToRoot('Welcomeid');}
      //       },
      //       // style: 'cancel',
      //     },
      //     // {text: 'OK', onPress: () => console.log('OK Pressed'),},
      //   ],
      //   { cancelable: false, }
      // );
    });
  };
  pressNo(appointment) {
    //return this.onPressCheckin();

    const { onPressNo, } = this.props;
    if (this.state.checkin_setting === 'Check In Only'||appointment=='1') {
      this.onPressCheckin(appointment);
    } else
  
    {onPressNo(
      this.props.phone,
      this.state.checkin_setting,
      this.props.storeCode,
      appointment
    );}
  }

  render() {
    const { onPressBtn1, onPressNo, onPressYes, } = this.props;
    const {result}=this.state
    return (
      <SafeAreaView style={styles.container}>
        <LogoView></LogoView>
        <Label />
        {!this.state.loading  && (
          <YesNo
            onPressYes={() => this.pressNo('1') }
            onPressNo={() => this.pressNo('0')}
          ></YesNo>
        )}
        <ButtonBottom
          containerBtn={{ height: '57%', }}
          containerStyles={{ flex: 2, }}
          onPressBtn1={onPressBtn1}
          txtBtn1="BACK"
          onPressBtn2={()=>this.onPressCheckin()}
          txtBtn2={this.state.checkin_setting === 'Check In Only' ? 'Check in Now' : false}
        ></ButtonBottom>
         <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showDiaalog2}
          onRequestClose={() => {
            this.setState({ showDiaalog2: false, });
          }}
        >
          <View style={styles.centeredView2}>
            <View style={styles.modalView}>
              <Text style={{ marginTop: 15, fontSize: 22, fontWeight: '500', }}>
                {this.state.sms2}
              </Text>
              <TouchableOpacity onPress={() => {
                if(result){
                  if (result.dataArray[0].Status == 'Success') {
                    Navigation.popToRoot('Welcomeid');
                  
                  }
                  else {
                   this.setState({showDiaalog2:false})
                  }
                }else{
                  this.setState({showDiaalog2:false})
                }
               

              }} style={[styles.button, styles.buttonClose,]}>
                <Text style={styles.textStyle}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: 'white', },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '80%',
    //height:'60%',
    margin: 50,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  button: {
    justifyContent:'center',
    alignItems:'center',
    margin: 30,
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    width:80
  },
  centeredView2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  textStyle:{color:'white', fontSize: 22, fontWeight: '500',}
});
