import React, { Component, } from 'react';

import { StyleSheet, View, Image,  Platform,TouchableOpacity, } from 'react-native';
import themes from '../../../config/themes';
import { normalize, } from '../../../utils/FontSize';
import { TextCmp, } from '../../../common-components/index';
import { ImagesIcons, } from '../../../assets';
export default class NailsTechnicianApp extends Component {
  render() {
    const {showQR,}=this.props;
    return (
      <View style={styles.NailsTechnicianApp}>
        <TextCmp  style={styles.txtnailsTech}>Nails Technicians APP</TextCmp>
        <View style={styles.containerImageNails}>
          {/* <TouchableOpacity onPress={()=>showQR(ImagesIcons.qrnganhnailsAndroid)}>
            <Image  resizeMode="stretch"
              style={styles.iconchplay}
              source= {ImagesIcons.chplayicon}
            ></Image>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>showQR(ImagesIcons.qrnganhnails)}  style={{marginTop:10, width:'100%', justifyContent:'center',alignItems:'center',}}>
            <Image  resizeMode="stretch"
              style={styles.iconchplay}
              source= {ImagesIcons.appStoreicon}
            ></Image>
          </TouchableOpacity> */}
          <Image
            source={Platform.OS=='android'?ImagesIcons.qrnganhnailsAndroid: ImagesIcons.qrnganhnails} 
            resizeMode="contain"
            style={styles.imageNailTech}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  NailsTechnicianApp: {
    flex: 1,
    backgroundColor: 'white',
    padding: 5,
    width:themes.width/2,
    alignItems:'center',
    justifyContent:'center',
  },
  txtnailsTech: {
    fontSize: normalize(themes.H5),
    color: 'gray',
    textAlign:'center',
    width:themes.width/2,

  },
  containerImageNails: {
    height: ((themes.height * 2) / 6.5) * 0.7,
    width: ((themes.height * 2) / 6.5) * 0.7,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: '10%',
    marginTop: 5,
  },
  imageNailTech: {
    height: ((themes.height * 2) / 6.5) * 0.7,
    width: ((themes.height * 2) / 6.5) * 0.7,
    marginRight: '10%',
  },
  iconchplay:{borderRadius:8,justifyContent:'center',alignItems:'center',height: ((themes.height * 2) / 6.5) * 0.25,width:themes.width/5,},
});
