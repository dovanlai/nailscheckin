import React, { Component, } from 'react';

import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  Image,
  Platform,
} from 'react-native';
import themes from '../../../config/themes';
import { Colors, } from '../../../theme';
import { normalize, } from '../../../utils/FontSize';
import { TextCmp, } from '../../../common-components/index';
import {ImagesIcons,} from '../../../assets';
export default class DownloadCustomer extends Component {
  render() {
    const{showQR,}=this.props;
    return (
      <View style={styles.containerDownloadCus}>
        <TextCmp style={styles.txtDownloadCus}>
           Download Customer Reward APP
        </TextCmp>
        <View style={styles.containerImagedownload}>
          {/* <TouchableOpacity onPress={()=>showQR(ImagesIcons.qrnailspaAndroid)}>
            <Image  resizeMode="stretch"
              style={styles.iconchplay}
              source= {ImagesIcons.chplayicon}
            ></Image>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>showQR(ImagesIcons.qrnailspa)}  style={{marginTop:10, width:'100%', justifyContent:'center',alignItems:'center',}}>
            <Image  resizeMode="stretch"
              style={styles.iconchplay}
              source= {ImagesIcons.appStoreicon}
            ></Image>
          </TouchableOpacity> */}
          <Image
            resizeMode="stretch"
            style={styles.imageDownload}
            source={Platform.OS=='android'? ImagesIcons.qrnailspaAndroid: ImagesIcons.qrnailspa}
          ></Image>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerDownloadCus: { flex: 1, backgroundColor: 'white', padding: 5,justifyContent:'center',alignItems:'center', width:themes.width/2,},
  txtDownloadCus: {
    fontSize: normalize(themes.H5),
    color: 'gray',
    //marginLeft: 10,
    textAlign:'center',
    width:themes.width/2,
  },
  containerImagedownload: {
    height: ((themes.height * 2) / 6.5) * 0.7,
    width: ((themes.height * 2) / 6.5) * 0.7,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: '10%',
    marginTop: 5,
  },
  imageDownload: { height: '100%', width: '100%', overflow: 'hidden', },
  iconchplay:{borderRadius:8,justifyContent:'center',alignItems:'center',padding:10,height: ((themes.height * 2) / 6.5) * 0.25,width:themes.width/5,},
});
