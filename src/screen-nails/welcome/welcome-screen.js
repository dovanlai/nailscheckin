import React, { Component, } from 'react';

import {
  StyleSheet,
  View,
  AsyncStorage,
  Alert,
  TouchableOpacity,
  BackHandler,
  Modal,
  Text,
  Image
} from 'react-native';
import { connect, } from 'react-redux';
import WelcomeLogoLabel from './welcome-logo-label/index';
import WelcomeHour from './hour/index';
import WelcomClick from './welcome-click/index';
import DownloadCustomer from './download-customer/index';
import NailsTechniciansApp from './nails-technicians-app/index';
import CheckInList from './check-in-list/index';
import codePush from 'react-native-code-push';

let codePushOptions = { checkFrequency: codePush.CheckFrequency.ON_APP_START, };
codePush.sync(codePushOptions);

import {
  AppCheckIn_SignInList,
  AppCheckIn_CheckOwnerPIN,
  AppCheckIn_GetStaffs,
  AppCheckIn_GetServices,
  AppCheckIn_SendSMSReviewLink,
  AppCheckIn_StoreSetting,
  AppCheckIn_GetQuickMenu_V2
} from '../../screens/Nails/Login/action';
import moment from 'moment';
import ModalCreateStoreCdoe from '../../screens/Nails/Login/ModalCreateStoreCdoe';
import ModalReview from '../../screens/Nails/Login/ModalReview';
import { Icon, } from 'react-native-elements';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import { Logg, } from '../../utils';
import ModalListSignIn from '../../screens/Nails/Login/ModalListSignIn';
import { Navigation, } from 'react-native-navigation';
import LogoView from '../old-customer/LogoView';
import Checkupdate from '../../screens/Geso/login/checkupdate';
import { initSignalR, } from '../../common-components/InitSignal';
import { showSuccess, } from '../../utils/SnackBar';
import ModalQRView from '../../screens/Nails/Login/ModalQRView';
import { WebView, } from 'react-native-webview';
class WelcomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      showEnterStoreCode: false,
      showReview: false,
      ticketId: '',//'120756',
      storeCode: null,
      PINCode: '',
      qr: null,
      showmodeQR: false,
      checkin_setting: '',
      showQRCodeCheckin: false,
      StoreId: null
      //
    };
  }
  // componentWillUnmount() {
  //   // Not mandatory
  //   if (this.navigationEventListener) {
  //     this.navigationEventListener.remove();
  //   }
  // }
  componentDidAppear() {
    //alert('xxx');
    if (this.state.storeCode) {
      this.AppCheckIn_SignInList({ storeCode: this.state.storeCode, });

    }

    let multi_set_pairs = [['sms', '',],];
    AsyncStorage.multiSet(multi_set_pairs, (err) => { });


    // AsyncStorage.getAllKeys((err, keys) => {
    //   AsyncStorage.multiGet(keys, (err, stores) => {
    //     stores.map((result, i, store) => {
    //       let key = store[i][0];
    //       let val = store[i][1];

    //       if (key === 'sms') {
    //         let sms = val;
    //         alert('sms '+sms);
    //       }
    //     });
    //   });
    // });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressed);
  }
  onBackButtonPressed() {
    return true;
  }
  componentDidMount() {
    this.navigationEventListener = Navigation.events().bindComponent(this);
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressed);
    let t = { storeCode: '', Logo: '', };

    AsyncStorage.getAllKeys((err, keys) => {
      AsyncStorage.multiGet(keys, (err, stores) => {
        stores.map((result, i, store) => {
          // get at each store's key/value so you can work with it
          let key = store[i][0];
          let val = store[i][1];

          console.log('Logo---------' + val);

          if (key === 'storeCode' && val.length > 0) {
            t.storeCode = val;

            this.setState({ storeCode: val, });
            this.AppCheckIn_SignInList({ storeCode: val, });
            this.AppCheckIn_GetStaffs({ storeCode: val, });
            // //SAVE_DATA_ServicesList
            this.AppCheckIn_GetServices({ storeCode: val, });

            this.getsetting(val);
            //this.AppCheckIn_WhyDownLoadApp({storeCode:val,});

          }
          if (key === 'PINCode' && val.length > 0) {
            this.setState({ PINCode: val, });
          }
          if (key === 'Logo' && val.length > 0) {
            this.setState({ Logo: val, });
            t.Logo = val;
          }
          if (key === 'WelcomeText' && val.length > 0) {
            this.setState({ WelcomeText: val, });
          }
          if (key === 'StoreId' && val.length > 0) {
            this.setState({ StoreId: val, });
            this.AppCheckIn_QuickMenu({ storeId: val, });
            //alert(val)
          }

          this.props.dispatch({
            type: 'SAVE_DATA_APP',
            payload: t,
          });
        });
      });
    });
    this.handleCreateConnection();

  }

  getsetting(storeCode) {

    let params = { storeCode, };
    this.props.dispatch(AppCheckIn_StoreSetting(params)).then((res) => {

      if (res) {

        this.setState({ checkin_setting: res.dataArray[2].checkin_setting, });

      }
      else {
        //Alert.alert('','')
        showError('error api');
      }
    });
  }
  UNSAFE_componentWillMount() {
    this.connection = null;
  }
  handleCreateConnection = () => {
    clearInterval(this.intervalConnection);

    this.intervalConnection = setInterval(() => {
      if (this.connection === null) {
        this.connection = initSignalR((data) => {
          this._handleSignal(data);
        });
      }
    }, 5000);
  };
  _handleSignal = (data) => {
    //alert(JSON.stringify(data));
    console.log('_handleSignal  ', data);
    let splitData = data.split('__');
    let storeId = splitData[0];
    if (storeId == this.state.storeId) {
      let action = splitData[1];
      if (action === 'CustomerReview') {
        // tạm thời bỏ đi
        //this.setState({showReview:true,ticketId:splitData[2],});
      }
    }


  };
  _onPressCheckListIn = () => {
    // componentDidMount(){
    this.navigationEventListener = Navigation.events().bindComponent(this);
    let t = { storeCode: '', Logo: '', };

    AsyncStorage.getAllKeys((err, keys) => {
      AsyncStorage.multiGet(keys, (err, stores) => {
        stores.map((result, i, store) => {
          // get at each store's key/value so you can work with it
          let key = store[i][0];
          let val = store[i][1];
          console.log('Logo---------' + val);

          if (key === 'storeCode' && val.length > 0) {
            t.storeCode = val;
            this.setState({ storeCode: val, });
            this.AppCheckIn_SignInList({ storeCode: val, });
            this.AppCheckIn_GetStaffs({ storeCode: val, });
            //SAVE_DATA_ServicesList
            this.AppCheckIn_GetServices({ storeCode: val, });
            //this.AppCheckIn_WhyDownLoadApp({storeCode:val,});
          }
          if (key === 'Logo' && val.length > 0) {
            this.setState({ Logo: val, });
            t.Logo = val;
          }
          if (key === 'WelcomeText' && val.length > 0) {
            this.setState({ WelcomeText: val, });
          }
          this.props.dispatch({
            type: 'SAVE_DATA_APP',
            payload: t,
          });
        });
      });
    });
  };
  AppCheckIn_GetServices(params) {
    (params.date = moment(new Date()).format('YYYY-MM-DD')),
      this.props.dispatch(AppCheckIn_GetServices(params)).then((result) => {
        //alert(JSON.stringify(result))
        Logg.info('Services lai xxxx ' + JSON.stringify(result));
        let listserveceSearch = [{ catname: 'All Services', id: -1, name: '', },];
        if (result) {
          result.map((item) => {
            if (item.id == '-1') {
              listserveceSearch.push(item);
            }
          });
        }

        this.setState({
          ServicesList: result,
          listserveceSearch: listserveceSearch,
        });
        this.props.dispatch({
          type: 'SAVE_DATA_ServicesList',
          payload: {
            ServicesList: result,
            listserveceSearch: listserveceSearch,
          },
        });

        if (result.dataArray && result.dataArray[1].ErrorMessege) {
          Alert.alert('', result.dataArray[1].ErrorMessege);
        } else {
          if (result.dataArray && result.dataArray[0].Status) {
            // Alert.alert('', result.dataArray[0].Status )
          } else {
            Alert.alert('', JSON.stringify(result));
          }
        }
        this.setState({ loadding: false, });
      });
  }
  AppCheckIn_QuickMenu(params) {

    this.props.dispatch(AppCheckIn_GetQuickMenu_V2(params)).then((result) => {
      //alert(JSON.stringify(result))
      // Logg.info('AppCheckIn_GetQuickMenu_V2 lai xxxx ' + JSON.stringify(result));
      //let listserveceSearch = [{ catname: 'All Services', id: -1, name: '', },];
      let bgColor = [
        '#00a1f1',
        '#7cbb00',

      ]
      let bgColor2 = [
        '#ffbb00',
        '#f65314',
      ]
      if (result) {
        let arr=[]
        if (result && result.length > 0) {

          result.map((item, index) => {
            let it = item
            if (index % 2 == 0)
              var xcolor = bgColor[Math.floor(Math.random() * bgColor.length)];
            else {
              var xcolor = bgColor2[Math.floor(Math.random() * bgColor.length)];
            }
            it.color= xcolor
            arr.push(it)
          })
        }
        this.props.dispatch({
          type: 'SAVE_DATA_QuickMenu',
          payload: arr,
        });
      }

      // this.setState({
      //   ServicesList: result,
      //   listserveceSearch: listserveceSearch,
      // });
      // this.props.dispatch({
      //   type: 'SAVE_DATA_ServicesList',
      //   payload: {
      //     ServicesList: result,
      //     listserveceSearch: listserveceSearch,
      //   },
      // });

      // if (result.dataArray && result.dataArray[1].ErrorMessege) {
      //   Alert.alert('', result.dataArray[1].ErrorMessege);
      // } else {
      //   if (result.dataArray && result.dataArray[0].Status) {
      //     // Alert.alert('', result.dataArray[0].Status )
      //   } else {
      //     Alert.alert('', JSON.stringify(result));
      //   }
      // }
      this.setState({ loadding: false, });
    });
  }
  AppCheckIn_GetStaffs(params) {
    (params.date = moment(new Date()).format('YYYY-MM-DD')),
      this.props.dispatch(AppCheckIn_GetStaffs(params)).then((result) => {
        //alert(JSON.stringify(result));
        Logg.info('Staffs lai xxxx ' + JSON.stringify(result));
        this.setState({ StaffsList: result, });
        this.props.dispatch({
          type: 'SAVE_DATA_Staffs',
          payload: {
            StaffsList: result,
          },
        });
        if (result.dataArray && result.dataArray[1].ErrorMessege) {
          Alert.alert('', result.dataArray[1].ErrorMessege);
        } else {
          if (result.dataArray && result.dataArray[0].Status) {
            // Alert.alert('', result.dataArray[0].Status )
          } else {
            Alert.alert('', JSON.stringify(result));
          }
        }
        this.setState({ loadding: false, });
      });
  }
  AppCheckIn_SignInList(params) {
    (params.date = moment(new Date()).format('YYYY-MM-DD')),
      //this.xoa()
      // this.setState({loadding:true})
      this.props.dispatch(AppCheckIn_SignInList(params)).then((result) => {
        Logg.info('lai xxxx AppCheckIn_SignInList ' + JSON.stringify(result));
        // if(JSON.stringify(result)!='[]')
        this.setState({ SignInList: result, });

        if (result.dataArray && result.dataArray[1].ErrorMessege) {
          Alert.alert('', result.dataArray[1].ErrorMessege);
        } else {
          if (result.dataArray && result.dataArray[0].Status) {
            // Alert.alert('', result.dataArray[0].Status )
          } else {
            Alert.alert('', JSON.stringify(result));
          }
        }
        this.setState({ loadding: false, });
      });
  }
  AppCheckIn_CheckOwnerPIN(PINCode, storeCode) {
    let params = {
      PINCode: PINCode,
      storeCode: storeCode,
    };

    this.setState({ loadding: true, showEnterStoreCode: false, });
    this.props.dispatch(AppCheckIn_CheckOwnerPIN(params)).then((result) => {
      Logg.info('AppCheckIn_CheckOwnerPIN' + JSON.stringify(result));

      //alert('xxxx');
      if (result.dataArray && result.dataArray[1].ErrorMessege) {
        Alert.alert('', result.dataArray[1].ErrorMessege);
      } else {
        if (result.dataArray && result.dataArray[0].Status) {
          if (result.dataArray[0].Status == 'Success') {
            //alert(result.dataArray[4].StoreName);
            let multi_set_pairs = [
              ['storeCode', '' + storeCode,],
              ['Logo', '' + result.dataArray[2].Logo,],
              ['PINCode', '' + PINCode,],
              ['WelcomeText', '' + result.dataArray[3].WelcomeText,],
              ['StoreName', '' + result.dataArray[4].StoreName,],
              ['StoreId', '' + result.dataArray[5].StoreId],
            ];
            AsyncStorage.multiSet(multi_set_pairs, (err) => { });

            let Logo = null,
              WelcomeText = null;
            if (result.dataArray[2].Logo) {
              Logo = result.dataArray[2].Logo;
            }
            if (result.dataArray[3].WelcomeText) {
              WelcomeText = result.dataArray[3].WelcomeText;
            }
            let StoreId = result.dataArray[5].StoreId;
            this.setState({ storeCode: storeCode, Logo, WelcomeText, StoreId, });

            this.AppCheckIn_GetStaffs({ storeCode: storeCode, });
            this.AppCheckIn_GetServices({ storeCode: storeCode, });
            // this.AppCheckIn_WhyDownLoadApp({storeCode:storeCode,});
            this.AppCheckIn_QuickMenu({ storeId: StoreId, });

            let t = { storeCode: storeCode, Logo: Logo, StoreId, };
            this.props.dispatch({
              type: 'SAVE_DATA_APP',
              payload: t,
            });
          }

          Alert.alert('', result.dataArray[0].Status);
        } else {
          Alert.alert('', JSON.stringify(result));
        }
      }
      this.setState({ loadding: false, });
    });
  }
  closeModalReview = () => {
    const { ticketId, } = this.state;
    let params = {
      isHappy: 2,
      ticketId: ticketId,
      //storeId:'100018',// this.state.storeCode?this.state.storeCode:'',
    };
    this.setState({ showReview: !this.state.showReview, });
    this.props.dispatch(AppCheckIn_SendSMSReviewLink(params)).then((result) => {
      //alert(JSON.stringify(result));    
      // showSuccess(result.dataArray[0].Status);
    });


  }
  closeModalQRView = () => {
    this.setState({ showmodeQR: !this.state.showmodeQR, });
  }
  openRlinkReview(isHappy) {
    const { ticketId, } = this.state;
    let params = {
      // reviewLink:isHappy?'https://pos.nailspaofamerica.com/Review/'+ticketId+'/happy':
      //   'https://pos.nailspaofamerica.com/Review/'+ticketId+'/un-happy',
      isHappy: isHappy ? 1 : 0,
      ticketId: ticketId,
      //storeId:'100018',// this.state.storeCode?this.state.storeCode:'',
    };

    this.props.dispatch(AppCheckIn_SendSMSReviewLink(params)).then((result) => {
      //alert(JSON.stringify(result));    
      showSuccess(result.dataArray[0].Status);
    });
    this.setState({ showReview: false, });
  }
  showQR(qr) {
    this.setState({ qr: qr, showmodeQR: true, });
  }
  showQRCheckin = () => {
    this.setState({ showQRCodeCheckin: true, });
  }
  render() {
    const { showQRCodeCheckin, StoreId, storeCode } = this.state
    return (
      <View style={styles.containerWelcomeScreen}>

        <Checkupdate></Checkupdate>
        <WelcomeHour />
        {/* <WelcomeLogoLabel
          WelcomeText={this.state.WelcomeText}
          Logo={this.state.Logo}
        /> */}
        <LogoView></LogoView>

        <WelcomClick StoreId={storeCode} showQRCheckin={() => this.showQRCheckin()} navigator={this.props.navigator} test={() => this.props.test(this.state.storeCode)} />


        <View style={styles.containerDownload}>
          <DownloadCustomer showQR={(qr) => this.showQR(qr)} />
          <CheckInList
            openListsignin={() => {
              this._onPressCheckListIn();
              this.setState({ showlistsign: true, });
            }}
          />
          <NailsTechniciansApp showQR={(qr) => this.showQR(qr)} />
        </View>

        <ModalCreateStoreCdoe
          PINCode={this.state.PINCode}
          storeCode={this.state.storeCode}
          AppCheckIn_CheckOwnerPIN={(PINCode, storeCode) =>
            this.AppCheckIn_CheckOwnerPIN(PINCode, storeCode)
          }
          ishowWhydownloadApp={this.state.showEnterStoreCode}
          setclosemodel={() => {
            this.AppCheckIn_SignInList({ storeCode: this.state.storeCode, });
            this.setState({ showEnterStoreCode: false, });
          }}
          checkin_setting={this.state.checkin_setting}
        ></ModalCreateStoreCdoe>


        <TouchableOpacity
          style={{ position: 'absolute', left: 50, top: 50, }}
          onPress={() => {
            this.setState({ showEnterStoreCode: true, });
          }}
        >
          <Icon reverse name="ios-cog" type="ionicon" color="#517fa4" />
        </TouchableOpacity>


        {/* <TouchableOpacity
          style={{ position: 'absolute', right: 130, top: 50, }}
          onPress={() => {
            this.setState({ showEnterStoreCode: true, });
          }}
        >
          <Icon reverse name="" type="ionicon" color="#517fa4" />
        </TouchableOpacity> */}

        {this.state.showlistsign && (
          <ModalListSignIn
            SignInList={this.state.SignInList}
            setclosemodel={() => {
              this.setState({ showlistsign: false, });
            }}
            paramcus={this.state.paramcus}
          />
        )}
        {/* {this.state.showReview&&<ModalReview
          showReview={this.state.showReview}
          PresUnHappy={()=>this.openRlinkReview(false)}
          PresHappy={()=>this.openRlinkReview(true)}
          setclosemodel={this.closeModalReview}
        
        ></ModalReview>} */}

        {this.state.showmodeQR && <ModalQRView
          qr={this.state.qr}
          closeModalQRView={this.closeModalQRView}
          showReview={this.state.showmodeQR}
        ></ModalQRView>}


        {showQRCodeCheckin && <Modal
          animationType="slide"
          transparent={true}
          visible={showQRCodeCheckin}

        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              {StoreId && <Image source={{ uri: `https://chart.googleapis.com/chart?cht=qr&chs=400x400&chl=${StoreId}&choe=UTF-8` }} style={{ width: 500, height: 500 }}></Image>}
              <TouchableOpacity
                style={[styles.button, styles.buttonClose]}
                onPress={() => this.setState({ showQRCodeCheckin: !showQRCodeCheckin })}
              >
                <Text style={styles.textStyle}>Close</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        }
      </View>
    );
  }
}

export default connect(
  (state) => ({}),
  (dispatch) => ({ dispatch, })
)(codePush(codePushOptions)(WelcomeScreen));
const styles = StyleSheet.create({
  containerDownload: {
    flex: 2,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  containerWelcomeScreen: { flex: 1, backgroundColor: 'white', },

  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    height: '100%'
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,

  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
    marginTop: 50,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    paddingHorizontal: 20,
    fontSize: 25

  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});
