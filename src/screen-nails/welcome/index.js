/**
 * @author: thai.nguyen
 * @date: 2018-11-29 16:24:38
 *
 *
 */
import { connect, } from 'react-redux';

import WelcomeScreen from './welcome-screen';
import { IDs, } from '../../screens';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch, { navigator, }) => {
  const test = async (storeCode) => {
    navigator._push({
      name:IDs.OldCustomer, //IDs.NewCustomer,
      passProps: {
        storeCode:storeCode,
      },
    });
  };
  return {
    test,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeScreen);
