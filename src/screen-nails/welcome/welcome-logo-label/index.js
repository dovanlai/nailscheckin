import React, { Component, } from 'react';

import { StyleSheet, View, Image, } from 'react-native';
import { normalize, } from '../../../utils/FontSize';
import { TextCmp, } from '../../../common-components/index';
import { ImagesIcons, } from '../../../assets';
import { Fonts, } from '../../../theme';
export default class WelcomeScreen extends Component {
  render() {
    const { WelcomeText, Logo, } = this.props;

    return (
      <View style={styles.containerfx1}>
        <View style={styles.containerLogo}>
          <Image
            resizeMode="stretch"
            style={styles.logo}
            //source={ImagesIcons.logonails}
            source={Logo ? { uri: Logo, }: ImagesIcons.logonails}
          ></Image>
          {/*
          {Logo ? (
            <Image
              resizeMode={"stretch"}
              style={styles.logo}
              source={{ uri: Logo }}
            />
          ) : (
            <Image
              resizeMode="stretch"
              style={styles.logo}
              source={ImagesIcons.logonails}
            />
          )}*/}
        </View>
        <View style={styles.containerLabel}>
          <TextCmp style={styles.label}>
            {WelcomeText ? WelcomeText : 'Welcome to nganhnails'}
          </TextCmp>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerfx1: { flex: 1, backgroundColor: 'white', flexDirection: 'row', },
  containerLogo: {
    flex: 0.6,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: { height: '90%', width: '45%', backgroundColor: 'white', },
  containerLabel: { flex: 1, justifyContent: 'center', },
  label: {
    fontSize: normalize(10),
    color: 'gray',
    fontFamily: Fonts.Jura.bold,
  },
  imgPicture: {
    width: 500,
    height: 100,
  },
});
