import React, { Component } from "react";

import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import themes from "../../../../config/themes";
import { normalize } from "../../../../utils/FontSize";
import { TextCmp } from "../../../../common-components/index";
import { Colors, Fonts } from "../../../../theme";
import Icon from "react-native-vector-icons/AntDesign";
export default class Customer extends Component {
  render() {
    const { onPressCustomer ,StoreId} = this.props;
    return (
      <View style={{ flexDirection: "row", height: "30%", width: "70%" , marginTop: 20,}}>
        <TouchableOpacity
          style={styles.containerCus}
          onPress={onPressCustomer}
          activeOpacity={0.7}
        >
          <TextCmp style={styles.txtCus}>Customer/Guest Check-in</TextCmp>
        </TouchableOpacity>
        {/* <TouchableOpacity
          style={
            ([styles.containerCus],
            { justifyContent: "center", alignItems: "center",width:'20%' ,backgroundColor:'#189FBF'})
          }
          onPress={()=>this.props.showQRCheckin()}
          activeOpacity={0.7}
        >
          <Icon size={55} name="qrcode"></Icon>
        </TouchableOpacity> */}
          
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerCus: {
    width: "80%",
    backgroundColor: Colors.orange,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 100,
  },
  txtCus: {
    fontSize: normalize(themes.H10),
    color: "white",
    marginHorizontal: 10,
  },
});
