/**
  @author: lai.do 
  @date: 2018-12-27 23:40:04 
   
  
 */
import { connect } from "react-redux";

import Customer from "./customer";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => {
  const test = async () => {
    alert("test");
  };
  return {
    test
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Customer);
