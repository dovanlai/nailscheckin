import React, { Component, } from 'react';

import { StyleSheet, View, TouchableOpacity, } from 'react-native';
import { TextCmp, } from '../../../../common-components/index';
import { normalize, } from '../../../../utils/FontSize';
import themes from '../../../../config/themes';
import { IDs, } from '../../../../screens';

export default class NailEmployee extends Component {

  render() {
    return (
      <View style={styles.containerNailEmployee}>
        <TouchableOpacity onPress={()=>this.props.onPressNail(1)} style={styles.containerNailTech}>
          <TextCmp style={styles.txtNailTech}>Nails Technicians</TextCmp>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={()=>this.props.onPressNail(2)} 
          style={styles.containerEmployee}
        >
          <TextCmp style={styles.txtEmployee}>Employee</TextCmp>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerNailEmployee: {
    height: '20%',
    width: '70%',
    backgroundColor: 'white',
    marginTop: 10,
    flexDirection: 'row',
  },

  containerEmployee: {
    width:'40%',
    backgroundColor: '#189FBF',
    marginLeft: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtEmployee: { fontSize: normalize(themes.H8), color: 'white', },
  containerNailTech: {
    width:'40%',
    backgroundColor: '#1A55A4',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtNailTech: {
    color: 'white',
    fontWeight: '400',
    fontSize: normalize(themes.H8),
  },
});
