import React, { Component } from "react";

import { StyleSheet, View, Image, Text } from "react-native";
import CustomerGuest from "./customer/index";
import NailEmployee from "./nails-employee/index";
import ReviewUs from "./review-us/index";
import { IDs } from "../../../screens";
export default class WelcomClick extends Component {
  onPressNail = (t) => {
    this.props.navigator._push({
      name: IDs.OldCustomer,
      passProps: {
        data: t,
      },
    });
  };

  render() {
    const { StoreId } = this.props;
    return (
      <View style={styles.containerClick}>
        <CustomerGuest
          StoreId={this.props.StoreId}
          showQRCheckin={this.props.showQRCheckin}
          onPressCustomer={this.props.test}
        />
        <NailEmployee onPressNail={(t) => this.onPressNail(t)} />

        {/* <ReviewUs/> */}
        {StoreId && (
          <View
            style={{
              position: "absolute",
              right: 20,
               //top: 20,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Image
            resizeMode='contain'
              source={{
                uri: `https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=${StoreId}&choe=UTF-8`,
              }}
              style={{ width: 200, height: 200 }}
            ></Image>
            <Text style={{ fontSize: 18, margin: 10 }}>
              Scan here to sign in
            </Text>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerClick: {
    flex: 3,
    backgroundColor: "white",
    alignItems: "center",
  },
});
