import React, { Component, } from 'react';

import { StyleSheet, View, } from 'react-native';
import themes from '../../../config/themes';
import { Fonts, } from '../../../theme';
import { normalize, } from '../../../utils/FontSize';
import { TextCmp, } from '../../../common-components/index';
import moment from 'moment';
export default class Hour extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jun : moment(new Date()).format('hh:mm a'),
      hours: new Date().getHours(),
      min: new Date().getMinutes(),
      // full: new Date().toLocaleString('en-US', { hour: 'numeric', hour12: true, minute: 'numeric', }),
      full:moment(new Date()).format('LL'),
    };
  }
  componentDidMount() {
    setInterval(() => {
      var time = new Date();
      this.setState({
        hours: new Date().getHours(),
        min: new Date().getMinutes(),
        // full:time.toLocaleString('en-US', { hour: 'numeric', hour12: true, minute: 'numeric', }),
        full:moment(new Date()).format('LL'),
        jun : moment(new Date()).format('hh:mm a'),
      });
    }, 1000);
  }
  render() {
    // var hours = new Date().getHours(); //Current Hours
    // var min = new Date().getMinutes(); //Current Minutes
    return (
      <View style={styles.containerHour}>
        <TextCmp style={styles.txtHour}>
          {/* {this.state.hours}:{this.state.min < 10 ? 0 : null}
          {this.state.min} */}
          {this.state.full} {this.state.jun}
        </TextCmp>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerHour: {
    flex: 0.5,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  txtHour: {
    color: 'black',
    fontSize: normalize(themes.H8),
    fontFamily: Fonts.Jura.bold,
  },
});
