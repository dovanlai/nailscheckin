/**
 * @author: thai.nguyen
 * @date: 2018-11-29 16:24:38
 *
 *
 */
import { connect, } from 'react-redux';

import YourNailsTech from './your-nails-tech-screen';
import { IDs, } from '../../screens';

const mapStateToProps = (state) => ({
  StaffsList: state.app.StaffsList,

});

const mapDispatchToProps = (dispatch, { navigator, }) => {
  const onPressBtn1 = async () => {
    navigator._pop(IDs.Welcome);
  };
  const onPressBtn2 = (data) => {
    navigator._push({
      name: IDs.YourService,
      passProps: {
        data:data,
      },
    });
  };
  return {
    onPressBtn1,
    onPressBtn2,
    navigator,
    dispatch
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(YourNailsTech);
