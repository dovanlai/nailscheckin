import React, { Component, } from 'react';

import { StyleSheet, SafeAreaView, View,Dimensions, ActivityIndicator, TouchableOpacity,Alert,} from 'react-native';
import { normalize, } from '../../utils/FontSize';
import Label from '../component-check-in/label';
import SeclectYourNailsTech from '../component-check-in/select-your-prefenails';
import ButtonBottom from '../component-check-in/button-bottom';
import { AppCheckIn_CustomerSignIn, } from '../../screens/Nails/Login/action';
import ModalRefTechnician from '../../screens/Nails/Login/ModalRefTechnician';
import LogoView from '../old-customer/LogoView';
import { Navigation } from "react-native-navigation";

const {height,}=Dimensions.get('window');
export default class YourNailsTech extends Component {
  static options(passprops) {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }

  constructor() {
    super();
    this.state = {
      Staffs: [],
      Show:false,
    };
  }
  componentDidMount() {
    //alert(JSON.stringify(this.props.StaffsList.StaffsList) )
    //alert(this.props.checkin_setting);
    if(this.props.checkin_setting=='Full CheckIn'||this.props.checkin_setting=='Show Nails Technician'){
      this.setState({Show:true,});
    }
    else{
      this.setState({Show:false,});
      this.onPressBtn2();
    }
  }
  onPressBtn2  ()  {
    // alert('xxx');
   // return  this.onPressCheckin();

    if (this.props.checkin_setting === "Show Nails Technician") {
      this.onPressCheckin();
    } else{
    let data = {
      phone: this.props.phone,
      Staffs: this.state.Staffs,
      checkin_setting:this.props.checkin_setting,
      storeCode:this.props.storeCode,
      appointment:this.props.appointment
    };
    this.props.onPressBtn2(data);
   
   }
  }
  onPressCheckin = () => {
    //return  Navigation.popToRoot('Welcomeid');
    //alert(JSON.stringify(this.props.data));
    let params = {
      phone: this.props.phone,
      storeCode: this.props.storeCode,
      haveAppt: "0",
    };
    let idStaffs = '';
    this.state.Staffs.map((item) => {
      idStaffs += item.id + '_';
    });
    params.staffId = idStaffs.slice(0, idStaffs.length - 1);
    params.serviceId = "";

    this.props.dispatch( AppCheckIn_CustomerSignIn(params)).then((result) => {
      //Alert.alert('', result.dataArray[1].ErrorMessege);
      Alert.alert(
        "",
        result.dataArray[1].ErrorMessege,
        [
          {
            text: "Ok",
            onPress: () => {
              if (result.dataArray[0].Status == "Success")
                Navigation.popToRoot("Welcomeid");
            },
            // style: 'cancel',
          },
          // {text: 'OK', onPress: () => console.log('OK Pressed'),},
        ],
        { cancelable: false }
      );
    });
  };
  
  render() {
    const { onPressBtn1, } = this.props;
    if(this.state.Show)
    {return (
      <SafeAreaView style={styles.container}>
        <View style={{height:height/6,}}>
          <LogoView></LogoView>
          <Label
            containerStyle={styles.containerLabelStyle}
            labelStyle={styles.labelStyle}
            label={'PLEASE SELECT YOUR PREFER NAILS TECH!'}
          ></Label>
        </View>
       
        {/*
        <SeclectYourNailsTech/>
          <ModalRefTechnician
      modalVisible={true}
      setModalVisible={() => {
        this.setState({ showlistTechnician: false });
      }}
      StaffsList={this.props.StaffsList.StaffsList}
      setStaffs={listadd => {
        this.setState({ Staffs: listadd });
      }}
    ></ModalRefTechnician>
      */}
        <View style={{height:height-height/3,}}>
          <ModalRefTechnician
            modalVisible={true}
            setModalVisible={() => {
              this.setState({ showlistTechnician: false, });
            }}
            StaffsList={this.props.StaffsList.StaffsList}
            setStaffs={(listadd) => {
              this.setState({ Staffs: listadd, });
            }}
          ></ModalRefTechnician>
        </View>


        <ButtonBottom
          onPressBtn1={onPressBtn1}
          onPressBtn2={()=>this.onPressBtn2()}
          txtBtn1="BACK"
          txtBtn2={(this.props.checkin_setting === "Show Nails Technician"&& this.state.Staffs.length > 0 )?'Check in Now': this.state.Staffs.length > 0 ? 'NEXT' : false}
          containerStyles={{height:height/6,}}
        ></ButtonBottom>
      </SafeAreaView>
    );}
    return(<View>
      <TouchableOpacity></TouchableOpacity>
      <ActivityIndicator>

      </ActivityIndicator>
    </View>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  containerLabelStyle: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  labelStyle: { color: 'black', fontSize: normalize(10), },
});
