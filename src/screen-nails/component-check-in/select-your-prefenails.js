import React, { Component } from "react";

import {
  StyleSheet,
  View,
} from "react-native";
import { Colors } from "../../theme";
export default class SeclectYourNailsTech extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.containerStyles]}>
        <View style={styles.containerChil1}>
          <View style={[styles.View, this.props.viewStyle]}></View>
          <View
            style={[styles.View, { marginLeft: 2 }, this.props.viewStyle]}
          ></View>
          <View
            style={[styles.View, { marginLeft: 2 }, this.props.viewStyle]}
          ></View>
          <View
            style={[styles.View, { marginLeft: 2 }, this.props.viewStyle]}
          ></View>
        </View>
        <View style={styles.containerChil2}>
          <View style={[styles.View, this.props.viewStyle]}></View>
          <View
            style={[styles.View, { marginLeft: 2 }, this.props.viewStyle]}
          ></View>
          <View
            style={[styles.View, { marginLeft: 2 }, this.props.viewStyle]}
          ></View>
          <View
            style={[styles.View, { marginLeft: 2 }, this.props.viewStyle]}
          ></View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: { flex: 5, backgroundColor: "white", paddingVertical: 3 },
  containerChil1: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  containerChil2: {
    flex: 1,
    marginTop: 2,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  View: {
    width: "20%",
    height: "99%",
    borderWidth: 1,
    borderColor: Colors.darkBlue,
    backgroundColor: "white"
  }
});
