import React, {Component,} from 'react';

import {
  StyleSheet,
  View,
  Image,
} from 'react-native';
import {ImagesIcons,} from '../../assets';
export default class Logo extends React.Component {
  render(){
    return (
      <View style={[styles.containerImage,this.props.containerStyles,]}>
        <Image
          resizeMode="stretch"
          style={[styles.imagelogo ,]}
          source={ 
            this.props.Logo?{uri:this.props.Logo.Logo,}:
              ImagesIcons.logonails}></Image>
      </View>
    );
  }
 
}

const styles = StyleSheet.create({
  containerImage: {
    flex: 3,
    backgroundColor: 'white',
    justifyContent: 'flex-end',
    paddingLeft: '10%',
  },
  imagelogo: {
    height: '50%',
    width: '20%',
    overflow: 'hidden',
    backgroundColor: 'white',
  },
});
