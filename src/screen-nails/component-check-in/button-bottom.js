import React, {Component,} from 'react';

import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  Image,
} from 'react-native';
import themes from '../../config/themes';
import {Colors,} from '../../theme';
import {normalize,} from '../../utils/FontSize';
import {TextCmp,} from '../../common-components/index';

export default (props = {}) => {
  return (
    <View style={[styles.container,props.containerStyles]}>
      <TouchableOpacity onPress={props.onPressBtn1} style={[styles.btn,props.containerBtn]}>
        <TextCmp style={styles.txt}>{props.txtBtn1}</TextCmp>
      </TouchableOpacity>
      {props.txtBtn3 ? (
        <TouchableOpacity onPress={props.onPressBtn3} style={[styles.btn,props.containerBtn]}>
          <TextCmp style={styles.txt}>{props.txtBtn3}</TextCmp>
        </TouchableOpacity>
      ) : (
        <View style = {{width : '15%'}}></View>
      )}

      {props.txtBtn2 ? (
        <TouchableOpacity onPress={props.onPressBtn2} style={[styles.btn,props.containerBtn]}>
          <TextCmp style={styles.txt}>{props.txtBtn2}</TextCmp>
        </TouchableOpacity>
      ) : (
        <View style = {{width : '15%'}}></View>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    height:75,
    backgroundColor: 'white',
    justifyContent: 'space-between',
    paddingHorizontal: '5%',
    alignItems: 'flex-end',
    flexDirection: 'row',
    paddingBottom: 10,

  },
  btn: {
    width: '25%',
    height: '100%',
    borderRadius: 5,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txt: {color: 'white', fontSize: normalize(themes.H8),},
});
