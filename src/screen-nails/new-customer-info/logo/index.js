import React, {Component} from 'react';

import {
  StyleSheet,
  View,
  Image,
} from 'react-native';
import {Colors} from '../../../theme';
import {normalize} from '../../../utils/FontSize';
import {TextCmp} from '../../../common-components/index';
import {ImagesIcons} from '../../../assets'
export default (props = {}) => {
  return (
    <View style={styles.container}>
      <View style={styles.containerImg}>
        <Image
          resizeMode="stretch"
          style={styles.img}
          source={ImagesIcons.logonails}></Image>
      </View>
      <View style={styles.ctxt}>
        <TextCmp style={styles.txt}>NEW CUSTOMER INFOR</TextCmp>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueBack,
    flexDirection: 'row',
  },
  containerImg: {flex: 1, alignItems: 'center', justifyContent: 'flex-end'},
  img: {width: '60%', height: '80%'},
  ctxt: {
    flex: 2,
    backgroundColor: Colors.blueBack,
    justifyContent: 'flex-end',
    paddingBottom: 5,
  },
  txt: {color: 'white', fontSize: normalize(15)},
});
