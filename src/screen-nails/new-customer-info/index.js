/**
 * @author: thai.nguyen
 * @date: 2018-11-29 16:24:38
 *
 *
 */
import { connect, } from 'react-redux';

import CustomerInfo from './customer-info-screen';
import { IDs, } from '../../screens';

const mapStateToProps = (state) => ({
  dataApp: state.app.dataApp,
});

const mapDispatchToProps = (dispatch, { navigator, }) => {
  const onPressBtn1 = async () => {
    navigator._pop(IDs.Welcome);
  };
  const onPressBtn2 = async (t,storeCode,sms) => {
    navigator._push({
      // name: IDs.YourNailsTech,
      name: IDs.SelectAppointment,
      passProps: {
        phone:t,
        storeCode,
        sms,
      },
    });
  };

  return {
    onPressBtn1,
    onPressBtn2,
    dispatch,
    navigator,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CustomerInfo);
