import React, { Component, } from 'react';

import {
  StyleSheet,
  View,
  SafeAreaView,
  Alert,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import themes from '../../config/themes';
import { Colors, } from '../../theme';
import ButtonBottom from '../component-check-in/button-bottom';
import Label from './logo/index';
import Input from './input/index';
import CreateAccount from '../../screens/Nails/CreateAccount';
import { Logg, } from '../../utils';
import { AppCheckIn_CustomerSignUp, } from '../../screens/Nails/Login/action';
import { IDs, } from '../../screens';
export default class CustomerInfo extends Component {
  static options(passprops) {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      showCreateAccount: true,
      paramcus: {
        phone: props.phone ? props.phone : 'Phone nummer',
        firstName: '',
        lastName: '',
        birthday: '',
      },
      loadding: false,
    };
  }
  AppCheckIn_CustomerSignUp(params, loai, sms) {
    params.isCheckin = '0';
    params.storeCode = this.props.dataApp ? this.props.dataApp.storeCode : '';

    if (params.phone == '') {
      return Alert.alert('', 'Please Enter Phone');
    } else if (params.firstName == '') {
      return Alert.alert('', 'Please Enter First Name');
    } else if (params.lastName == '') {
      return Alert.alert('', 'Please Enter Last Name');
    } else {
      this.setState({ showCreateAccount: false, });
      this.setState({ loadding: true, });
      this.props.dispatch(AppCheckIn_CustomerSignUp(params)).then((result) => {
        this.setState({ loadding: false, });
        if (result.dataArray[0].Status == 'Success') {
          this.props.onPressBtn2(params.phone, this.props.storeCode, sms);
          let multi_set_pairs = [['sms', '' + sms,],];
          AsyncStorage.multiSet(multi_set_pairs, (err) => {});
        } else {
          Alert.alert('', result.dataArray[1].ErrorMessege);
        }
      });
    }
  }
  render() {
    const { onPressBtn1, onPressBtn2, } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        {/* <Label />
        <Input /> 
      */}

        <CreateAccount
          //islock={props.phone ? props.phone :''}
          PINCode={this.state.paramcus.phone}
          AppCheckIn_CustomerSignUp={(pr, loai, sms) => {
            // if (this.props.phone && this.props.phone.length > 0) {
            //     onPressBtn2(this.state.paramcus.phone, this.props.storeCode);
            // } else {
            this.AppCheckIn_CustomerSignUp(pr, loai, sms);
            // }
          }}
          ishowWhydownloadApp={this.state.showCreateAccount}
          setclosemodel={() => {
            this.setState({ showCreateAccount: false, });
          }}
          onPressBtn1={onPressBtn1}
          onPressBtn2={() =>
            onPressBtn2(this.state.paramcus.phone, this.props.storeCode)
          }
        ></CreateAccount>

        {this.state.loadding && (
          <ActivityIndicator color={'red'} size="large"></ActivityIndicator>
        )}

        {/*
           <ButtonBottom
          txtBtn1="BACK"
          txtBtn2="NEXT"
          onPressBtn1={onPressBtn1}
          onPressBtn2={onPressBtn2}
          containerStyles={{
            flex: 1,
            backgroundColor: "#4F8CB8"
          }}
          containerBtn={{ height: "85%" }}
        ></ButtonBottom>

        
        <View style={styles.line}></View> */}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueBack,
  },
  line: {
    width: '70%',
    height: 3,
    backgroundColor: 'white',
    bottom: ((themes.height * 1) / 5) * 0.85,
    marginLeft: '15%',
  },
});
