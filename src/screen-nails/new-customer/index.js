/**
 * @author: thai.nguyen
 * @date: 2018-11-29 16:24:38
 *
 *
 */
import { connect, } from 'react-redux';

import NewCustomer from './new-customer-screen';
import { IDs, } from '../../screens';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch, { navigator, }) => {
  const onPressBtn1 = async () => {
    navigator._pop(IDs.Welcome);
  };
  const onPressYes = async (storeCode) => {
    navigator._push({
      name: IDs.NewCustomerInfo,
      passProps: {storeCode:storeCode,},
    });
  };
  const onPressNo = async (storeCode) => {
    navigator._push({
      name: IDs.OldCustomer,
      passProps: {storeCode,},
    });
  };
  return {
    onPressBtn1,
    onPressYes,
    onPressNo,
    dispatch,
    navigator,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(NewCustomer);
