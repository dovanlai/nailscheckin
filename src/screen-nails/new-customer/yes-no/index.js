import React, { Component } from "react";

import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Navigation } from "react-native-navigation";
import themes from "../../../config/themes";
import { Colors } from "../../../theme";
import { normalize } from "../../../utils/FontSize";
import { TextCmp } from "../../../common-components/index";

export default class YesNo extends Component {
  render() {
    const { onPressYes, onPressNo } = this.props;
    return (
      <View style={styles.container}>
        <View style={{ height: "20%" }}></View>
        <View style={styles.containerYes}>
          <TouchableOpacity
            onPress={onPressYes}
            activeOpacity={0.7}
            style={styles.btnYes}
          >
            <TextCmp style={styles.txtYes}>YES</TextCmp>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={onPressNo}
            activeOpacity={0.7}
            style={styles.btnNo}
          >
            <TextCmp style={styles.txtNo}>NO</TextCmp>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 5, backgroundColor: "white" },
  containerYes: {
    height: "25%",
    width: "100%",
    backgroundColor: "white",
    flexDirection: "row"
  },
  btnYes: {
    backgroundColor: Colors.darkBlue,
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  txtYes: { color: "white", fontSize: normalize(10) },
  btnNo: {
    backgroundColor: Colors.blue,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 5
  },
  txtNo: { color: "white", fontSize: normalize(10) }
});
