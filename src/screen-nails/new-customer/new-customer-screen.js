import React, { Component, } from 'react';

import { StyleSheet, SafeAreaView,Dimensions, } from 'react-native';
import Logo from '../component-check-in/logo';
import Label from '../component-check-in/label';
import YesNo from './yes-no/index';
import ButtonBottm from '../component-check-in/button-bottom';
import { normalize, } from '../../utils/FontSize';
import{connect,}from 'react-redux';
import { AppCheckIn_CustomerSignIn ,AppCheckIn_CustomerCheckExits} from "../../screens/Nails/Login/action";
class NewCustomer extends Component {
  static options(passprops) {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }
  constructor(props){
    super(props);
  }
  componentDidMount(){
    //alert(this.props.storeCode);
  }
  onPressYes(){
    this.props.onPressYes(this.props.storeCode)
    
  }
  render() {
    const { onPressBtn1,onPressYes,onPressNo, } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <Logo containerStyles={{ flex: 3, }} Logo={this.props.dataApp}></Logo>
        <Label
          labelStyle={styles.labelStyle}
          containerStyle={styles.containerStyle}
          label="Are you new customer?"
        ></Label>
        <YesNo onPressYes = {()=>this.onPressYes()} onPressNo = {()=>onPressNo(this.props.storeCode)}></YesNo>
        <ButtonBottm
          onPressBtn1={onPressBtn1}
          containerStyles={{ flex: 1, }}
          txtBtn1="BACK"
        ></ButtonBottm>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    dataApp: state.app.dataApp,
  }),
  (dispatch) => ({ dispatch, }),
)(NewCustomer);
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: 'white', },
  labelStyle: {
    color: 'black',
    fontSize: normalize(12),
  },
  containerStyle: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingLeft: '10%',
    alignItems: 'center',
  },
});
