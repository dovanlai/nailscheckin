/**
 * @author: thai.nguyen
 * @date: 2018-11-29 16:24:38
 *
 *
 */
import { connect, } from 'react-redux';

import YourService from './your-service-screen';
import { IDs, } from '../../screens';


const mapStateToProps = (state) => ({
  ServicesList:state.app.ServicesList,
  listserveceSearch: state.app.listserveceSearch,
  dataApp: state.app.dataApp,
  QuickMenu: state.app.QuickMenu,
});

const mapDispatchToProps = (dispatch, { navigator, }) => {
  const onPressBtn1 = async () => {
    navigator._pop(IDs.Welcome);
  };
  const onPressBtn2 = async () => {
    alert('a');
  };
  return {
    onPressBtn1,
    onPressBtn2,
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(YourService);
