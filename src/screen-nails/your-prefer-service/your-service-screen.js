import React, { Component, } from 'react';

import {
  StyleSheet,
  SafeAreaView,
  Alert,
  View,
  AsyncStorage,
  Modal,
  Text,
  TouchableOpacity,
} from 'react-native';
import { normalize, } from '../../utils/FontSize';
import Label from '../component-check-in/label';
import SeclectYourNailsTech from '../component-check-in/select-your-prefenails';
import ButtonBottom from '../component-check-in/button-bottom';
import Logo from '../component-check-in/logo';
import LogoView from '../old-customer/LogoView';
import ModalRefTechnician from '../../screens/Nails/Login/ModalRefTechnician';
import ModalListServices from '../../screens/Nails/Login/ModalListServices';
import { AppCheckIn_CustomerSignIn, } from '../../screens/Nails/Login/action';
import { Navigation, } from 'react-native-navigation';
import ScreenIDs from '../../screens/ScreenIDs';
import ModalQuickMenu from '../../screens/Nails/Login/ModalQuickMenu';
export default class YourService extends Component {
  static options(passprops) {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }
  constructor() {
    super();
    this.state = {
      Services: [],
      Show: false,
      showDiaalog: false,
      sms: '',
      showDiaalog2: false,
      sms2: 'xxxxx',
      result: null,
    };
  }
  componentDidMount() {
    // alert(JSON.stringify(this.props) );
    AsyncStorage.getAllKeys((err, keys) => {
      AsyncStorage.multiGet(keys, (err, stores) => {
        stores.map((result, i, store) => {
          let key = store[i][0];
          let val = store[i][1];
          if (key === 'sms' && val.length > 0) {
            let sms = val;
            this.setState({ sms, });
          }
        });
      });
    });

    if (
      this.props.data.checkin_setting == 'Full CheckIn' ||
      this.props.data.checkin_setting == 'Show Service'|| 
         this.props.data.checkin_setting == 'Show Quickmenu'
    ) {
      this.setState({ Show: true, });
    }
   // alert(JSON.stringify(this.props.QuickMenu))
  }
  onPressBtn2 = () => {
    //return  Navigation.popToRoot('Welcomeid');
    //alert(JSON.stringify(this.props.data));
    const { data, } = this.props;
    let params = {
      phone: data.phone,
      storeCode: this.props.data.storeCode ? this.props.data.storeCode : '',
      haveAppt: this.props.data.appointment ? this.props.data.appointment : '0',
    };

    let sv = '';
    this.state.Services.map((item) => {
      sv += item.id + '_';
    });
    let idStaffs = '';
    data.Staffs.map((item) => {
      idStaffs += item.id + '_';
    });
    params.staffId = idStaffs.slice(0, idStaffs.length - 1);
    params.serviceId = sv.slice(0, sv.length - 1);

    this.props.dispatch(AppCheckIn_CustomerSignIn(params)).then((result) => {

      //Alert.alert('', result.dataArray[1].ErrorMessege);
      this.setState({ result, showDiaalog2: true, sms2: result.dataArray[1].ErrorMessege, })
      if (result&&result.dataArray[0].Status == 'Success') {
        setTimeout(() => {
          Navigation.popToRoot('Welcomeid');
          this.setState({showDiaalog2:false})
        }, 3000);
      }
      // Alert.alert(
      //   '',
      //   result.dataArray[1].ErrorMessege,
      //   [
      //     {
      //       text: 'Ok',
      //       onPress: () => {
      //         if (result.dataArray[0].Status == 'Success') {
      //           this.checkinok();
      //         }
      //         else {
      //           if (result.dataArray[1].ErrorMessege == 'Error: You don’t have appointment. We will check you in as walk in')
      //             Navigation.popTo('SelectAppointment');
      //         }

      //       },
      //       // style: 'cancel',
      //     },
      //     // {text: 'OK', onPress: () => console.log('OK Pressed'),},
      //   ],
      //   { cancelable: false, }
      // );
    });
  };
  checkinok() {
    if (this.state.sms != '') this.setState({ showDiaalog: true, });
    else{
      Navigation.popToRoot('Welcomeid');
      this.setState({showDiaalog2:false})
    } 
  }
  render() {
    const { onPressBtn1, } = this.props;
const {result}=this.state
    return (
      <SafeAreaView style={styles.container}>
        {this.state.Show && (
          <View style={{ flex: 14, }}>
            <LogoView></LogoView>
            <Label
              containerStyle={styles.containerLabelStyle}
              labelStyle={styles.labelStyle}
              label={'PLEASE SELECT YOUR PREFER SERVICES!'}
            ></Label>
            {/* <SeclectYourNailsTech></SeclectYourNailsTech> */}

            <ModalQuickMenu
              modalVisible={true}
              setModalVisible={() => {
                this.setState({ showServicesList: false, });
              }}
              StaffsList={this.props.ServicesList.ServicesList}
              setStaffs={(item) => {
                this.setState({ Services: item, });
              }}
              listserveceSearch={this.props.listserveceSearch.listserveceSearch}
              QuickMenu={this.props.QuickMenu}
            ></ModalQuickMenu>
          </View>
        )}
        <ButtonBottom
          onPressBtn1={onPressBtn1}
          onPressBtn2={this.onPressBtn2}
          onPressBtn3={() => {
            Navigation.popToRoot('Welcomeid');
          }}
          txtBtn1="BACK"
          txtBtn2={
            this.state.Services.length > 0 || this.state.Show == false
              ? 'Check Me In'
              : false
          }
          // txtBtn2={'Check Me In'}
          txtBtn3="HOME"
          containerStyles={{ flex: 1, }}
          containerBtn={{ height: 70, }}
        ></ButtonBottom>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showDiaalog}
          onRequestClose={() => {
            this.setState({ showDiaalog: false, });
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={{ marginTop: 15, fontSize: 22, fontWeight: '500', }}>
                {this.state.sms}
              </Text>
              <TouchableOpacity
                style={[styles.button, styles.buttonClose,]}
                onPress={() => {
                  this.setState({ showDiaalog: false, });
                  let multi_set_pairs = [['sms', '',],];
                  AsyncStorage.multiSet(multi_set_pairs, (err) => { });
                  Navigation.popToRoot('Welcomeid');

                }}
              >
                <Text style={styles.textStyle}>AGREE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showDiaalog2}
          onRequestClose={() => {
            this.setState({ showDiaalog2: false, });
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={{ marginTop: 15, fontSize: 22, fontWeight: '500', }}>
                {this.state.sms2}
              </Text>
              <TouchableOpacity onPress={() => {
                if(result){
                  if (result.dataArray[0].Status == 'Success') {
                    this.checkinok();
                  
                  }
                  else {
                    if (result.dataArray[1].ErrorMessege == 'Error: You don’t have appointment. We will check you in as walk in')
                      Navigation.popTo('SelectAppointment');
                  }
                }else{
                  this.setState({showDiaalog2:false})
                }
               

              }} style={[styles.button, styles.buttonClose,]}>
                <Text style={styles.textStyle}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  containerLabelStyle: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  labelStyle: { color: 'black', fontSize: normalize(10), },

  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    //padding: 20,
    margin: 20,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '80%',
    //height:'60%',
    margin: 50,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    margin: 30,
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
});
