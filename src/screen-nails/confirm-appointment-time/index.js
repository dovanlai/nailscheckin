/**
 * @author: thai.nguyen
 * @date: 2018-11-29 16:24:38
 *
 *
 */
import { connect, } from 'react-redux';

import ConfirmAppointmentTime from './confirm-screen';
import { IDs, } from '../../screens';

const mapStateToProps = (state) => ({
  dataApp: state.app.dataApp,
  StaffsList: state.app.StaffsList,
  ServicesList:state.app.ServicesList,
  listserveceSearch: state.app.listserveceSearch,
});

const mapDispatchToProps = (dispatch, { navigator, }) => {
  const onPressBtn1 = async () => {
    navigator._pop(IDs.Welcome);
  };

  return {
    onPressBtn1,
    dispatch,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmAppointmentTime);
