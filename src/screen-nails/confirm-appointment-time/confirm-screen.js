import React, { Component, } from 'react';

import {
  StyleSheet,
  View,
  SafeAreaView,
  Alert,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import themes from '../../config/themes';
import { Colors, } from '../../theme';
import { normalize, } from '../../utils/FontSize';
import Logo_ from '../component-check-in/logo';
import Label_ from '../component-check-in/label';
import ButtonBottom from '../component-check-in/button-bottom';
import ButtonLine1 from './button-line-1/index';
import Button from './button-line-1/btn';
import { Navigation, } from 'react-native-navigation';
import { Logg, } from '../../utils';
import {
  AppCheckIn_GetAppoimentTime,
  AppCheckIn_ConfirmAppoimentTime,
} from '../../screens/Nails/Login/action';
import ModalTime from '../../screens/Nails/Login/ModalTime';
import LogoView from '../old-customer/LogoView';
import TextCmp from '../../common-components/TextCmp';
import ModalRefTechnician from '../../screens/Nails/Login/ModalRefTechnician';
import ModalListServices from '../../screens/Nails/Login/ModalListServices';
const { width, height, } = Dimensions.get('window');
const datatime = [
  { id: '9:00',  appoimentTime: '9:00 AM', },
  { id: '9:15',  appoimentTime: '9:15 AM', },
  { id: '9:30',  appoimentTime: '9:30 AM', },
  { id: '9:45',  appoimentTime: '9:45 AM', },
  { id: '10:00', appoimentTime: '10:00 AM', },
  { id: '10:15', appoimentTime: '10:15 AM', },
  { id: '10:30', appoimentTime: '10:30 AM', },
  { id: '10:45', appoimentTime: '10:45 AM', },
  { id: '11:00', appoimentTime: '11:00 AM', },
  { id: '11:15', appoimentTime: '11:15 AM', },
  { id: '11:30', appoimentTime: '11:30 AM', },
  { id: '11:45', appoimentTime: '11:45 AM', },
  { id: '12:00', appoimentTime: '12:00 PM', },
  { id: '12:15', appoimentTime: '12:15 PM', },
  { id: '12:30', appoimentTime: '12:30 PM', },
  { id: '12:45', appoimentTime: '12:45 PM', },
  { id: '13:00', appoimentTime: '1:00 PM', },
  { id: '13:15', appoimentTime: '1:15 PM', },
  { id: '13:30', appoimentTime: '1:30 PM', },
  { id: '1:45',  appoimentTime: '1:45 PM', },
  { id: '14:00', appoimentTime: '2:00 PM', },
  { id: '14:15', appoimentTime: '2:15 PM', },
  { id: '14:30', appoimentTime: '2:30 PM', },
  { id: '14:45', appoimentTime: '2:45 PM', },
  { id: '15:00', appoimentTime: '3:00 PM', },
  { id: '15:15', appoimentTime: '3:15 PM', },
  { id: '15:30', appoimentTime: '3:30 PM', },
  { id: '15:45', appoimentTime: '3:45 PM', },
  { id: '16:00', appoimentTime: '4:00 PM', },
  { id: '16:15', appoimentTime: '4:15 PM', },
  { id: '16:30', appoimentTime: '4:30 PM', },
  { id: '16:45', appoimentTime: '4:45 PM', },
  { id: '17:00', appoimentTime: '5:00 PM', },
  { id: '17:15', appoimentTime: '5:15 PM', },
  { id: '17:30', appoimentTime: '5:30 PM', },
  { id: '17:45', appoimentTime: '5:45 PM', },
  { id: '18:00', appoimentTime: '6:00 PM', },
  { id: '18:15', appoimentTime: '6:15 PM', },
  { id: '18:30', appoimentTime: '6:30 PM', },
  { id: '18:45', appoimentTime: '6:45 PM', },
  { id: '19:00', appoimentTime: '7:00 PM', },
  { id: '19:15', appoimentTime: '7:15 PM', },
  { id: '19:30', appoimentTime: '7:30 PM', },
  { id: '19:45', appoimentTime: '7:45 PM', },
  { id: '20:00', appoimentTime: '8:00 PM', },
];
export default class ConfirmAppointmentTime extends Component {
  static options(passprops) {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }
  constructor() {
    super();
    this.state = {
      showServicesList: false,
      services: '',
      nailsTech: '',
      showlistTechnician: false,
      istime: { appoimentId: '', appoimentTime: '', nailsTech: '', },
      modalVisible: false,
      Datatime: null, //JSON.parse('[{\"appoimentId\":100078,\"appoimentTime\":\"11:15\"},{\"appoimentId\":100077,\"appoimentTime\":\"1:00P\"}]')
      DatatimeDefault: datatime,
    };
  }
  componentDidMount() {
    // alert(this.props.phone);
    this.AppCheckIn_GetAppoimentTime();
  }
  AppCheckIn_GetAppoimentTime() {
    let params = {
      phone: this.props.phone,
      storeCode: this.props.dataApp ? this.props.dataApp.storeCode : '',
    };
    this.props.dispatch(AppCheckIn_GetAppoimentTime(params)).then((result) => {
      //alert(JSON.stringify(result));
      Logg.info(
        'AppCheckIn_GetAppoimentTime result = ' + JSON.stringify(result)
      );
      // if(result.dataArray[2].Data){
      this.setState({
        Datatime: result,
        istime: result[0],
        nailsTech: result[0].nailsTech,
        services: result[0].services,
      });
      // }
    });
  }

  AppCheckIn_ConfirmAppoimentTime() {
    if (this.state.istime) {
      const { Datatime, istime, } = this.state;
      // if(Datatime&&Datatime.length===0||!istime)
      // {return this.setState({ modalVisible:false,});}
      // this.setState({istime: Datatime?Datatime[index]:istime, modalVisible:false,});
      console.log('istime.appoimentId' + this.state);
      let prams = {
        phone: this.props.phone,
        storeCode: this.props.dataApp ? this.props.dataApp.storeCode : '',
        appoimentId: istime.appoimentId,
        checkinId: '', //this.state.checkinId,
      };
      this.setState({ loadding: true, });
      this.props
        .dispatch(AppCheckIn_ConfirmAppoimentTime(prams))
        .then((result) => {
          if (result.dataArray && result.dataArray[1].ErrorMessege) {
            if (result.dataArray[0].Status == 'Success') {
              // Alert.alert(
              //   result.dataArray[1].ErrorMessege,
              //   'Please download Customer App to keep track your reward points.'
              // );
              Alert.alert(
                result.dataArray[1].ErrorMessege,
                'Please download Customer App to keep track your reward points.',
                [
                  {
                    text: 'Ok',
                    onPress: () => {
                      if (result.dataArray[0].Status == 'Success')
                      {Navigation.popToRoot('Welcomeid');}
                    },
                    // style: 'cancel',
                  },
                  // {text: 'OK', onPress: () => console.log('OK Pressed'),},
                ],
                { cancelable: false, }
              );

              let paramcus = {
                phone: 'Phone nummer',
                firstName: '',
                lastName: '',
                birthday: '',
              };
              let istime = { appoimentId: '', appoimentTime: '', };
              this.setState({
                paramcus,
                istime,
                checkinId: '',
                Datatime: null,
              });
            } else {
              Alert.alert('', result.dataArray[1].ErrorMessege);
            }
            //if(result.dataArray[2].Data)
            //this.setState({Datatime:JSON.parse(result.dataArray[2].Data),istime: JSON.parse(result.dataArray[2].Data)[0]})
          } else {
            if (result.dataArray && result.dataArray[0].Status) {
              Alert.alert('', result.dataArray[0].Status);
            } else {
              Alert.alert('', JSON.stringify(result));
            }
          }
          this.setState({ loadding: false, });
        });
    } else {
      alert('Enter your phone number');
    }
  }
  onPressTime = () => {
    this.setState({
      modalVisible: true,
    });
  };
  render() {
    const {
      onPressPedicure,
      onPressBtn1,
      onPressNails,
      onPressTime,
    } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        {/* <Logo_ containerStyles={{ flex: 3 }}></Logo_> */}
        <View style={{ height: 200, }}>
          <LogoView></LogoView>
          <Label_
            labelStyle={styles.labelStyle}
            containerStyle={styles.containerStyle}
            label="PLEASE CONFIRM YOUR APPOINTMENT TIME"
          />
        </View>

        <View style={styles.containerBtn}>
          <ButtonLine1
            onPressNails={() =>
              this.setState({
                showlistTechnician: !this.state.showlistTechnician,
              })
            }
            onPressTime={this.onPressTime}
            nailsTech={this.state.istime ? this.state.nailsTech : ''}
            time={
              this.state.istime.appoimentId !== ''
                ? this.state.istime.appoimentTime
                : 'What Time Is Your Appointment'
            }
          />
          <Button
            textStyles={{ color: 'black', fontSize: normalize(12), }}
            onPress={() =>
              this.setState({ showServicesList: !this.state.showServicesList, })
            }
            label={this.state.istime ? this.state.services : ''}
            style={styles.btnPedicure}
          ></Button>
        </View>
        <View>
          <TextCmp
            style={{ color: 'black', fontSize: normalize(6), marginLeft: 10, }}
          >
            if you want to change nails technicians or service just tap on the
            button you like change
          </TextCmp>
        </View>
        <ButtonBottom
          onPressBtn1={onPressBtn1}
          onPressBtn2={() => this.AppCheckIn_ConfirmAppoimentTime()}
          txtBtn1="BACK"
          txtBtn2="CHECK IN"
          containerStyles={{ height: height / 10, marginTop: 20, }}
        ></ButtonBottom>

        {this.state.modalVisible && (
          <ModalTime
            modalVisible={this.state.modalVisible}
            setModalVisible={() => {
              this.setState({ modalVisible: false, });
            }}
            Datatime={
              this.state.Datatime !== null
                ? this.state.Datatime
                : this.state.DatatimeDefault
            }
            settime={(index) => {
              // alert(index)
              if (index !== '' && this.state.Datatime !== null) {
                this.setState({
                  modalVisible: false,
                  istime: this.state.Datatime[index],
                  nailsTech: this.state.Datatime[index].nailsTech,
                  services: this.state.Datatime[index].services,
                });
              }else
              {this.setState({
                modalVisible: false,
                istime: this.state.DatatimeDefault[index],
                //nailsTech: this.state.Datatime[index].nailsTech,
                //services: this.state.Datatime[index].services,
              });}
              //this.setState({ modalVisible: false, });
            }}
          ></ModalTime>
        )}

        {this.state.showlistTechnician && (
          <View
            style={{
              position: 'absolute',
              bottom: 0,
              flex: 1,
              backgroundColor: 'white',
              height: '70%',
              width: '100%',
            }}
          >
            <ModalRefTechnician
              modalVisible={true}
              setModalVisible={() => {
                this.setState({ showlistTechnician: false, });
              }}
              StaffsList={this.props.StaffsList.StaffsList}
              setStaffs={(listadd) => {
                let a = '';
                listadd.map((i) => {
                  a += ' - ' + i.name;
                });
                this.setState({ Staffs: listadd, nailsTech: a, });
              }}
            ></ModalRefTechnician>
            {/* <ButtonBottom
            onPressBtn1={onPressBtn1}
            onPressBtn2={this.onPressBtn2}
            txtBtn1="ok"
         
            containerStyles={{ flex: 2, }}
          ></ButtonBottom> */}
            <View
              style={{
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <TouchableOpacity
                onPress={() => this.setState({ showlistTechnician: false, })}
                style={{
                  backgroundColor: 'red',
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: 200,
                  height: 70,
                  borderRadius: 10,
                }}
              >
                <TextCmp style={{ fontSize: 25, color: 'white', }}>ok</TextCmp>
              </TouchableOpacity>
            </View>
          </View>
        )}

        {this.state.showServicesList && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              backgroundColor: 'white',
              height: '100%',
              width,
            }}
          >
            <ModalListServices
              modalVisible={true}
              setModalVisible={() => {
                this.setState({ showServicesList: false, });
              }}
              StaffsList={this.props.ServicesList.ServicesList}
              setStaffs={(item) => {
                let a = '';
                item.map((i) => {
                  a += i.name + ' - ';
                });
                this.setState({ Services: item, services: a, });
              }}
              listserveceSearch={this.props.listserveceSearch.listserveceSearch}
              showok={true}
            ></ModalListServices>
          </View>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  containerBtn: {
    height: height - 400,
    backgroundColor: 'white',
    paddingVertical: 10,
  },
  btnPedicure: { backgroundColor: Colors.orange, marginTop: 10, },
  labelStyle: {
    color: 'black',
    fontSize: normalize(themes.H10),
  },
  containerStyle: {
    flex: 2,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    paddingLeft: '10%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
