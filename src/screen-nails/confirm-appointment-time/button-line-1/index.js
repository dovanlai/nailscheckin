import React, { Component, } from 'react';

import { StyleSheet, View, } from 'react-native';
import Button from './btn';
export default class ButtonLine1 extends Component {
  render() {
    const { onPressNails, onPressTime,time,nailsTech } = this.props;
    return (
      <View style={styles.containerBtnLine1}>
        <Button onPress={onPressTime} label={time}></Button>
        <Button
          onPress={onPressNails}
          label={'NAILS TECH: '+nailsTech}
          style={{ marginLeft: 3, }}
        ></Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerBtnLine1: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
});
