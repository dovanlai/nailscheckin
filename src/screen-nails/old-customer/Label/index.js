import React, { Component, } from 'react';

import { StyleSheet, View, } from 'react-native';
import { normalize, } from '../../../utils/FontSize';
import { TextCmp, } from '../../../common-components/index';

export default class Label extends Component {
  render() {
    return (
      <View style={styles.containerLabel}>
        <View style={{ alignItems: 'center', }}>
          <TextCmp style={{ fontSize: normalize(10), }}>
            {this.props.data && this.props.data==1?'Please type in your Nails Technician Code': this.props.data==2?'Please key in your Employee code' :'Please type in your phone number!'}
          </TextCmp>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerLabel: { flex: 0.5, backgroundColor: 'white', },
});
