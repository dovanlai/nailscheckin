/**
 * @author: thai.nguyen
 * @date: 2018-11-29 16:24:38
 *
 *
 */
import { connect, } from 'react-redux';

import OldCustomer from './old-customer-screen';
import { IDs, } from '../../screens';

const mapStateToProps = (state) => ({
  dataApp: state.app.dataApp,
  
});

const mapDispatchToProps = (dispatch, { navigator, }) => {
  const onPressBack = async () => {
    navigator._pop(IDs.Welcome);
  };
  const onPressNext = async (t,storeCode) => {
    
    navigator._push({
      name: IDs.SelectAppointment,
      passProps: {
        phone:t,
        storeCode,
      },
    });
  };
  // const TechnicianCheckIn= ()=>{


  // }

  const onPressNewCus = async (t,storeCode,) => {
    
    navigator._push({
      name: IDs.NewCustomerInfo,
      passProps: {
        phone:t,
        storeCode,
      },
    });
  };

  return {
    onPressBack,
    onPressNext,
    onPressNewCus,
    dispatch,
    navigator
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(OldCustomer);
