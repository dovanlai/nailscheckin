import React, { Component, } from 'react';

import {
  StyleSheet,
  View,
  SafeAreaView,
  Alert,
  ActivityIndicator,
  Dimensions,
  Platform
} from 'react-native';

import Label from './Label/index';
import Button from './button/index';
import Keyboard from './keyboard/index';
import LogoView from './LogoView';
import {
  AppCheckIn_TechnicianCheckIn,
  AppCheckIn_EmployeeCheckIn,
  AppCheckIn_TechnicianCheckOut,
  AppCheckIn_EmployeeCheckOut,
  AppCheckIn_CustomerCheckExits,
} from '../../screens/Nails/Login/action';
import { IDs, } from '../../screens';
import { Navigation } from 'react-native-navigation';
import { showSuccess } from '../../utils/SnackBar';
const {height,}=Dimensions.get('window');
export default class OldCustomer extends Component {
  static options(passprops) {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }
  constructor() {
    super();
    this.state = {
      phone: '',
      Clockin: 'NEXT',
    };
  }
  componentDidMount() {
    // alert(JSON.stringify(this.props.storeCode) );
    if (this.props.data === 1 || this.props.data === 2) {
      this.setState({
        Clockin: 'CLOCKIN',
      });
    }
  }
  next = () => {
    if (this.props.data) {
      if (this.props.data == 1) {
        this.TechnicianCheckIn();
      } else {
        this.AppCheckIn_EmployeeCheckIn();
      }
    } else {

      let params2={
        phone:this.state.phone,
        storeCode:this.props.storeCode,
      };
      // return(alert(JSON.stringify (params2)))
      this.props.dispatch( AppCheckIn_CustomerCheckExits(params2)).then((res) => {
        // 
        if (res.dataArray[0].Status !== 'Success'){
          // alert(JSON.stringify(res));
          this.props.onPressNewCus(this.state.phone,this.props.storeCode);
        }
        else{
           
          this.props.onPressNext(this.state.phone,this.props.storeCode);
        }
      });
  
  

    }
  };
  ClockOut=()=>{
    if (this.props.data) {
      if (this.props.data == 1) {
        this.TechnicianCheckOut();
      } else {
        this.AppCheckIn_EmployeeCheckOut();

      }
    }
  }
  AppCheckIn_EmployeeCheckOut(){
    let params={
      PINCode: this.state.phone,
      storeCode: this.props.dataApp? this.props.dataApp.storeCode:'',
    };
    this.setState({loadding:true,});
    this.props.dispatch( AppCheckIn_EmployeeCheckOut(params)).then((result)=>{
     
      if(result.dataArray && result.dataArray[1].ErrorMessege){
        {this.xoa();}
        if(result.dataArray[0].Status=='Success')
       
        {Alert.alert('', result.dataArray[1].ErrorMessege);}
      }
      this.setState({loadding:false,});
    });
  }
  xoa=()=>{
    this.setState({PINCode:'',phone:'',});
  }
  TechnicianCheckOut(){
    let params={
      PINCode: this.state.phone,
      storeCode: this.props.dataApp? this.props.dataApp.storeCode:'',
    };
    this.setState({loadding:true,});
    this.props.dispatch( AppCheckIn_TechnicianCheckOut(params)).then((result)=>{
     
      if(result.dataArray && result.dataArray[1].ErrorMessege){
        if(result.dataArray[0].Status=='Success')
        {this.xoa();}
        Alert.alert('', result.dataArray[1].ErrorMessege);

      }
      this.setState({loadding:false,});
    });
  }

  AppCheckIn_EmployeeCheckIn() {
    let params = {
      PINCode: this.state.phone,
      storeCode: this.props.dataApp? this.props.dataApp.storeCode:'',
    };
    this.setState({ loadding: true, });
    this.props.dispatch(AppCheckIn_EmployeeCheckIn(params)).then((result) => {
      if (result.dataArray && result.dataArray[1].ErrorMessege) {
        this.xoa();
        // if (result.dataArray[0].Status == 'Success') {
        //   this.xoa();
        // }
        Alert.alert('', result.dataArray[1].ErrorMessege);
        if( result.dataArray[1].ErrorMessege=='CheckIn Success'){
          showSuccess(result.dataArray[1].ErrorMessege)
          setTimeout(() => {
            Navigation.popToRoot('Welcomeid');
          
          }, 1);
        }else
        Alert.alert('', result.dataArray[1].ErrorMessege);
      } else {
        if (result.dataArray && result.dataArray[0].Status) {
          Alert.alert('', result.dataArray[0].Status);
        } else {
          Alert.alert('', JSON.stringify(result));
        }
      }
      this.setState({ loadding: false, });
    });
  }
  TechnicianCheckIn() {
   


    let params = {
      PINCode: this.state.phone,
      storeCode:this.props.dataApp? this.props.dataApp.storeCode:'',
    };
    this.setState({ loadding: true, });

    this.props.dispatch(AppCheckIn_TechnicianCheckIn(params)).then((result) => {
      this.xoa();
      if (result.dataArray && result.dataArray[1].ErrorMessege) {
       
        // if (result.dataArray[0].Status == 'Success') {
        //   this.xoa();
        // }
        //Alert.alert('xxx', result.dataArray[1].ErrorMessege);
        if( result.dataArray[1].ErrorMessege=='CheckIn Success'){
          showSuccess(result.dataArray[1].ErrorMessege)
          setTimeout(() => {
            Navigation.popToRoot('Welcomeid');
          
          }, 1);
        }else
        Alert.alert('', result.dataArray[1].ErrorMessege);
      } else {
        if (result.dataArray && result.dataArray[0].Status) {
          Alert.alert('', result.dataArray[0].Status);
        } else {
          Alert.alert('', JSON.stringify(result));
        }
      }
      this.setState({ loadding: false, });
    });
  }

  setvalueInput(t) {
    this.setState({ phone: t, });
  }
  openQRcodeScan=()=>{ 
    const {navigator,} = this.props;
    navigator._push({
      name:  IDs.QRcode,
      options:{
        bottomTabs: {
          visible: false,
          ...Platform.select({ android: { drawBehind: true, }, }), 
        },
      },
      passProps: {
        data:(res)=> this.setvalueInput(res),
      },
     

    });
  }
  render() {
    const { onPressBack, onPressNext, } = this.props;
    //console.log('this.props.dataaaaaaaaaaaaaaaaaaaaaaa' + this.props.data);
    return (
      <SafeAreaView style={styles.container}>
        <LogoView>


        </LogoView>
        <Label data={this.props.data}></Label>
        <View style={styles.conainerBtnKB}>
          <View style={styles.containerBtnBack}>
            <Button
              style={styles.btnBack}
              onPressBack={onPressBack}
              label={'BACK'}
            ></Button>
          </View>
         
          <View style={{width:'70%',}}>
            <Keyboard
            openQRcodeScan={this.openQRcodeScan}
              phone={this.state.phone}
              setvalueInp={(t) => {
                this.setvalueInput(t);
              }}
            ></Keyboard>
          </View>
         
          <View style={{width:'15%',}}>
            <View style={styles.containerBtnNext}>
              {!this.state.loadding ? (
                <View style={{width:'120%', }}>             
                  <Button
                    style={styles.btnBack}
                    onPressBack={this.next}
                    label={this.state.Clockin}

                  ></Button>
                  {this.props.data&&<Button
                    style={[styles.btnBack,{width:'100%', }]}
                    onPressBack={this.ClockOut}
                    label={'CLOCKOUT'}

                  ></Button>}
                </View>
             
              ) : (
                <View style={styles.btnNext}>
                  <ActivityIndicator
                    color="blue"
                    size="large"
                  ></ActivityIndicator>
                </View>
              )}
            </View>
         
          </View>
        
        </View>
        
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: 'white', justifyContent:'center',alignItems:'center',},
  conainerBtnKB: { flex: 5, flexDirection: 'row', marginTop: 3,justifyContent:'space-between', },
  containerBtnBack: {
    width:'15%',
    backgroundColor: 'white',
    padding: 5,
    justifyContent: 'flex-end',
  },
  btnBack: {
    width: '100%',
    height: height/10,
    backgroundColor: 'red',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerBtnNext: {
    //flex:1,
    width:'120%',
    position:'absolute',
    bottom:0,right:0,
    //backgroundColor: 'white',
    padding: 5,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    // backgroundColor:'red',

    
  },

  btnNext: {
    width: '100%',
    height: height/10,
    backgroundColor: 'red',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    minWidth:200,
  },

});
