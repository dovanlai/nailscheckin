import React, { Component } from "react";

import { StyleSheet, View, Image } from "react-native";
import { ImagesIcons } from "../../../assets";
import { connect } from "react-redux";
class LogoOld extends Component {
  componentDidMount(){
    //console.log('this.props.dataApp.Logo',this.props.dataApp.Logo)
  }
  render() {
    return (
      <View style={[styles.containerImage, this.props.containerStyles]}>
        <Image
          resizeMode="stretch"
          style={[styles.imagelogo, this.props.imageStyles]}
          source={
            this.props.dataApp
              ? { uri: this.props.dataApp.Logo }
              : ImagesIcons.logonails
          }
        ></Image>
      </View>
    );
  }
}
export default connect(
  state => ({
    dataApp: state.app.dataApp
  }),
  dispatch => ({ dispatch })
)(LogoOld);
const styles = StyleSheet.create({
  containerImage: {
    flex: 1,
    justifyContent: "flex-end",
    paddingLeft: "10%" , 
  },
  imagelogo: {
    height: "70%",
    width: "15%",
    overflow: "hidden",
    backgroundColor: "white"
  }
});
