import React, { Component, } from 'react';

import { StyleSheet, TouchableOpacity, View, } from 'react-native';
import Number from './button';
import { normalize, } from '../../../utils/FontSize';

import { TextCmp, } from '../../../common-components/index';
import { Colors, } from '../../../theme';
import {ImagesIcons,} from '../../../assets';
import  AntDesign  from 'react-native-vector-icons/AntDesign';
export default class Label extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valueInp: '',
    };
  }
  _PressKey = (number) => {
    this.props.setvalueInp(this.props.phone + number);
    // this.setState({
    //   valueInp: this.state.valueInp + number,
    // });
  };
  
  onPressKey = (a) => {
    if ('1' === a) {
      this._PressKey(a);
    } else if ('2' === a) {
      this._PressKey(a);
    } else if ('3' === a) {
      this._PressKey(a);
    } else if ('4' === a) {
      this._PressKey(a);
    } else if ('5' === a) {
      this._PressKey(a);
    } else if ('6' === a) {
      this._PressKey(a);
    } else if ('7' === a) {
      this._PressKey(a);
    } else if ('8' === a) {
      this._PressKey(a);
    } else if ('9' === a) {
      this._PressKey(a);
    } else if ('0' === a) {
      this._PressKey(a);
    } else if ('X' === a) {
      this.props.setvalueInp('');
      // this.setState({
      //   valueInp: '',
      // });
    } else if ('<<' === a) {
      this.props.setvalueInp( this.props.phone.substring(
        0,
        this.props.phone.length - 1
      ));
      // this.setState({
      //   valueInp: this.state.valueInp.substring(
      //     0,
      //     this.state.valueInp.length - 1
      //   ),
      // });
    }
  };
  componentDidMount(){

  }
  render() {
    return (
      <View
        style={{
          flex: 2,
          backgroundColor: 'white',
          marginHorizontal: 10,
        }}
      >
        <View style={styles.container}>
          <View style={styles.containerInputText}>
            <TextCmp style={styles.inp}>{this.props.phone}</TextCmp>
            {/* <TouchableOpacity onPress={ this.props.openQRcodeScan }style={{padding:15, justifyContent:'center',alignItems:'center', height:'100%'}}>
               <AntDesign size={26}  name ='qrcode'></AntDesign>
            </TouchableOpacity> */}
          
          </View>
        </View>

        <View style={{ flex: 0.2, }}></View>
        <View style={styles.containerNumber}>
          <View style={{ flexDirection: 'row', flex: 1, }}>
            <Number onPress={() => this.onPressKey('1')} text="1"></Number>
            <Number onPress={() => this.onPressKey('2')} text="2"></Number>
            <Number onPress={() => this.onPressKey('3')} text="3"></Number>
          </View>

          <View style={{ flexDirection: 'row', flex: 1, }}>
            <Number onPress={() => this.onPressKey('4')} text="4"></Number>
            <Number onPress={() => this.onPressKey('5')} text="5"></Number>
            <Number onPress={() => this.onPressKey('6')} text="6"></Number>
          </View>
          <View style={{ flexDirection: 'row', flex: 1, }}>
            <Number onPress={() => this.onPressKey('7')} text="7"></Number>
            <Number onPress={() => this.onPressKey('8')} text="8"></Number>
            <Number onPress={() => this.onPressKey('9')} text="9"></Number>
          </View>
          <View style={{ flexDirection: 'row', flex: 1, }}>
            <Number ImagesIcons = {ImagesIcons.deleteKeyboard} onPress={() => this.onPressKey('X')} text="X"></Number>
            <Number onPress={() => this.onPressKey('0')} text="0"></Number>
            <Number ImagesIcons = {ImagesIcons.backspace} onPress={() => this.onPressKey('<<')} text="<<"></Number>
          </View>
          <View style={{ flex: 0.5, }}></View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerInputText: {
    width: '70%',
    height: '80%',
    borderWidth: 1,
    borderColor: Colors.darkBlue,
    justifyContent: 'center',
    borderRadius: 100 , 
    flexDirection:'row',alignItems:'center',justifyContent:'center'
  },
  inp: {
    flex: 1,
    marginTop:15,
    textAlign: 'center',
    fontSize: normalize(10),
    textAlignVertical: 'center',
    fontWeight: '400',
    color: 'gray',
  },
  containerNumber: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
