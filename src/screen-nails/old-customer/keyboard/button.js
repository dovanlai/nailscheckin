import React, { Component, } from 'react';

import { StyleSheet, TouchableOpacity, Dimensions, Image, } from 'react-native';
import { normalize, } from '../../../utils/FontSize';
import { TextCmp, } from '../../../common-components/index';

export default (props = {}) => {
  return (
    <TouchableOpacity
      {...props}
      activeOpacity={0.9}
      onPress={props.onPress}
      style={[styles.containerBtn, props.containerStyle,]}
    >
      {props.ImagesIcons == null ? (
        <TextCmp style={[styles.txt, props.textStyles,]}>{props.text}</TextCmp>
      ) : (
        <Image
          source={props.ImagesIcons}
          style={{
            width: '40%',
            height: '40%',
          }}
        ></Image>
      )}
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  containerBtn: {
    width:Dimensions.get('window').width<Dimensions.get('window').height? (Dimensions.get('window').width/8):(Dimensions.get('window').height/8), //(((Dimensions.get("window").height * 5) / 6) * 1) / 6.2 - 5,
    height:Dimensions.get('window').width<Dimensions.get('window').height? (Dimensions.get('window').width/8):(Dimensions.get('window').height/8), //(((Dimensions.get("window").height * 5) / 6) * 1) / 6.2 - 5,
    backgroundColor: '#EBEBEB',
    borderRadius: 100,
    borderWidth: 0.3,
    borderColor: 'gray',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    marginHorizontal: 15,
  },
  txt: { color: 'black', fontSize: normalize(10), },
});
