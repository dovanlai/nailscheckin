
import { Provider, store, rehydrateStore, startSaga, } from './redux';
import * as AppsController from './AppController';

import { Logg, FiFetch, } from './utils';
import { registerScreens, } from './screens';
import screenHOC from './app-navigation/screenHOC';
import { AppNavigation, } from './app-navigation';
import { selectors, } from './stores';

const logger = Logg.create('App');

// IMPORTANT: register screens on native level, must be excuted before other code.
registerScreens(screenHOC, store, Provider);

const startApp = async () => {
  
  await rehydrateStore(store);
  AppNavigation.setDefaultOptions();

  startSaga();
   AppsController.startlogin();
   // AppsController.startHome();
};

export default { start: startApp, };