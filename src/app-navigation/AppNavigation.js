import { Navigation, } from 'react-native-navigation';

import { IDs, } from '../screens';
import { Fonts, Colors, } from '../theme';
import { CommonIcons, } from '../assets';

const showCommonDialog = ({
  title,
  desc,
  cancelText,
  submitText,
  onCancel,
  onSubmit,
  renderContent,
  tapToDismiss = true,
}) => {
  Navigation.showOverlay({
    component: {
      id: '1',
      name: IDs.OverlayComponent,
      passProps: {
        tapToDismiss,
        title,
        desc,
        cancelText,
        submitText,
        onCancel,
        onSubmit,
        renderContent,
      },
      options: {
        overlay: {
          interceptTouchOutside: false,
        },
        layout: {
          backgroundColor: 'transparent',
        },
      },
    },
  });
};

const dismissOverlay = (componentID) => {
  Navigation.dismissOverlay(componentID);
};

const showModal = (params) => {
  Navigation.showModal({
    stack: {
      children: [
        {
          component: {
            options: {
              topBar: {
                visible: false,
              },
            },
            ...params,
          },
        },
      ],
    },
  });
};

const dismissModal = (componentID) => {
  Navigation.dismissModal(componentID);
};
const dismissAllModals = () => {
  Navigation.dismissAllModals();
};

const changeTab = (tabIndex) => {
  Navigation.mergeOptions('BottomTabsId', {
    bottomTabs: {
      currentTabIndex: tabIndex,
    },
  });
};

const setDefaultOptions = () => {
  Navigation.setDefaultOptions({
    statusBar: {
      backgroundColor: Colors.statusbarcolor,
      drawBehind: false,
      visible: true,
    },
    topBar: {
      buttonColor: '#fff',
      visible: true,
      background: {
        color: Colors.HeaderColor,
      },
      title: {
        color: '#fff',
        fontFamily: Fonts.Default.bold,
      },
      backButton: {
        color: '#fff',
      },
    },
    layout: {
      backgroundColor: Colors.backgroundL3,
      orientation: ['landscape',],
    },
    bottomTab: {
      iconColor: Colors.blurWhite,
      textColor: Colors.blurWhite,
      selectedTextColor: Colors.selectedColor,
      fontFamily: Fonts.Default.regular,
    },
    bottomTabs: {
      backgroundColor: Colors.backgroundL1,
    },
  });
};

export {
  showCommonDialog,
  dismissOverlay,
  showModal,
  dismissModal,
  changeTab,
  setDefaultOptions,
  dismissAllModals
};
