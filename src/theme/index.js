/**
 * @author: thai.nguyen 
 * @date: 2018-11-29 17:13:55 
 *  
 * 
 */
import Colors from './Colors';
import Metrics from './Metrics';
import Fonts from './Fonts';
import TabbarStyle from './TabbarStyle';
import * as Styles from './Styles';
// More theme items here...

export { Colors, Metrics, Fonts, TabbarStyle, Styles};