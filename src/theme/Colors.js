/**
 * @author: thai.nguyen 
 * @date: 2018-11-29 17:17:15 
 *  
 * 
 */
const StatusBarTextColors = { dark: 'dark', light: 'light', };

export default {
  statusBarTextColors: StatusBarTextColors,
  primary: '#f79741',
  secondPrimary: '#fa6152',
  accent: '#2b7800',
  statusbarcolor:'#9b0000',
  inActiveBtmTabColor: '#999999',

  separator: '#f0ddcb',
  colorOpacity: (opacity) => `rgba(0,0,0,0.${opacity})`,
  mainColor: '#113A7A',
  HeaderColor:'#d50000',
  backgroundL1: '#1D3055',
  backgroundL2: '#ffff',
  backgroundL3: '#ffff',
  selectedColor: '#d50000',

  blurWhite: '#A4A8B3',
  upColor: '#18B774',
  downColor: '#DB005D',
  line: '#E1E2E1',
  btn:'red',
  
  darkBlue: '#1A55A4',
  blue: '#189FBF',
  orange: '#F48210',

  blueBack: '#4D8CB7',
  // text0: '#000000',
  // text: '#414141', // title
  // text2: '#4a4a4a',
  // text3: '#9b9b9b',
  // text4: '#d8d8d8',
};
