/**
 * @author: thai.nguyen
 * @date: 2018-11-29 17:17:48
 *
 *
 */
const ProximaNova = {
  regular: 'ProximaNova-Regular',
  regularItalic: 'ProximaNova-RegularIt',
  bold: 'ProximaNova-Bold',
  boldItalic: 'ProximaNova-BoldIt',
};
const Jura = {
  regular: 'ProximaNova-Regular',
  regularItalic: 'ProximaNova-RegularIt',
  bold: 'ProximaNova-Bold',
  boldItalic: 'ProximaNova-BoldIt',
};

export default { Default:  ProximaNova,Jura, };
