/**
 * @author: thai.nguyen
 * @date: 2018-11-29 17:29:22
 *
 *
 */

import { Navigation, } from 'react-native-navigation';

import ScreenIDs from './screens/ScreenIDs';
import { Colors, Fonts, } from './theme';
import { BottomBarAssets, CommonIcons, } from './assets';

// export const startlogin =()=>{
//   Navigation.setRoot({
//     root: {
//       component: {
//         // name:ScreenIDs.LoginGeso,
//         name:ScreenIDs.Welcome,
//       },
//     },
//   });

// };
export const startlogin = () => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              id: ScreenIDs.Welcome + 'id',
              name: ScreenIDs.Welcome,
              options: {
                topBar: {
                  layout: {
                    orientation: ['landscape',],
                  },
                  visible: false,
                  drawBehind: true,
                },
              },
            },
          },
        ],
      },
    },
  });
};

export const startHome = null;

// = () => {
//   Navigation.setRoot({
//     root: {
//       bottomTabs: {
//         id: 'BottomTabsId',
//         children: [
//           {
//             stack: {
//               children: [
//                 {
//                   component: {
//                     name: ScreenIDs.Home,
//                   },
//                 },
//               ],
//               options: {

//                 topBar: {
//                   buttonColor: '#fff',
//                   visible: true,
//                   background: {
//                     color: Colors.HeaderColor,
//                   },
//                   title: {
//                     color: '#fff',
//                     fontFamily: Fonts.Default.bold,
//                     text: 'Trang Chủ',
//                     // alignment: 'center',
//                   },
//                   // rightButtons: [
//                   //   {
//                   //     id: 'btntopmenu',
//                   //     icon: CommonIcons.dots,
//                   //   },
//                   // ],
//                   // subtitle:{
//                   //   color: '#fff',
//                   //   fontFamily: Fonts.Default.bold,
//                   //   text: 'nhân viên: ',
//                   // },
//                   // backButton: {
//                   //   color: '#fff',
//                   // },
//                 },
//                 bottomTab: {
//                   text: 'Home',
//                   icon: BottomBarAssets.home,
//                   iconInsets: { top: 5, left: 0, bottom: -5, right: 0, },
//                   selectedIconColor: Colors.selectedColor,
//                   selectedTextColor: Colors.selectedColor,
//                 },
//               },
//             },
//           },
//           {
//             stack: {
//               children: [
//                 {
//                   component: {
//                     name: ScreenIDs.KHN,
//                     passProps: {
//                       text: 'stack with one child',
//                     },
//                   },
//                 },
//               ],
//               options: {
//                 topBar: {
//                   title: {
//                     text: 'Kế Hoạch Ngày',
//                     color: '#fff',
//                     fontFamily: Fonts.Default.bold,
//                   },
//                   background: {
//                     color: Colors.HeaderColor,
//                   },
//                 },
//                 bottomTab: {
//                   text: 'Kế Hoạch Ngày',
//                   icon: BottomBarAssets.khngay,
//                   iconInsets: { top: 5, left: 0, bottom: -5, right: 0, },
//                   selectedIconColor: Colors.selectedColor,
//                   selectedTextColor: Colors.selectedColor,
//                 },
//               },
//             },
//           },
//           {
//             stack: {
//               children: [
//                 {
//                   component: {
//                     name: ScreenIDs.KHTHANG,
//                   },
//                 },
//               ],
//               options: {
//                 topBar: {
//                   title: {
//                     text: 'Kế Hoạch Tháng',
//                     color: '#fff',
//                     fontFamily: Fonts.Default.bold,
//                   },
//                   background: {
//                     color: Colors.HeaderColor,
//                   },
//                 },
//                 bottomTab: {
//                   text: 'Kế Hoạch Tháng',
//                   icon: BottomBarAssets.khthang,
//                   iconInsets: { top: 5, left: 0, bottom: -5, right: 0, },
//                   selectedIconColor: Colors.selectedColor,
//                   selectedTextColor: Colors.selectedColor,
//                 },
//               },
//             },
//           },
//           {
//             stack: {
//               children: [
//                 {
//                   component: {
//                     name: ScreenIDs.MenuBC,
//                   },
//                 },
//               ],
//               options: {
//                 topBar: {
//                   title: {
//                     text: 'Báo cáo',
//                     color: '#fff',
//                     fontFamily: Fonts.Default.bold,
//                   },
//                   background: {
//                     color: Colors.HeaderColor,
//                   },
//                 },
//                 bottomTab: {
//                   text: 'Báo Cáo',
//                   icon: BottomBarAssets.bacao,
//                   iconInsets: { top: 5, left: 0, bottom: -5, right: 0, },
//                   selectedIconColor: Colors.selectedColor,
//                   selectedTextColor: Colors.selectedColor,
//                 },
//               },
//             },
//           },
//           {
//             stack: {
//               children: [
//                 {
//                   component: {
//                     name: ScreenIDs.Account,
//                   },
//                 },
//               ],
//               options: {
//                 // topBar: {
//                 //   visible: false,
//                 //   drawBehind:true,
//                 //   // background: {
//                 //   //   color: Colors.backgroundL1,
//                 //   // },
//                 // },
//                 topBar: {
//                   title: {
//                     text: 'Tài Khoản',
//                     color: '#fff',
//                     fontFamily: Fonts.Default.bold,
//                   },
//                   background: {
//                     color: Colors.HeaderColor,
//                   },
//                 },
//                 bottomTab: {
//                   text: 'Tài Khoản',
//                   icon: BottomBarAssets.Account,
//                   iconInsets: { top: 5, left: 0, bottom: -5, right: 0, },
//                   selectedIconColor: Colors.selectedColor,
//                   selectedTextColor: Colors.selectedColor,
//                 },
//               },
//             },
//           },
//         ],
//       },
//     },
//   });
// };
