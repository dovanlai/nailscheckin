import { StatusBar, } from 'react-native';
import { Navigation, } from 'react-native-navigation';
import IDs from './ScreenIDs';

import MarketScreen from './market';
import TradeScreen from './trade';
import FundScreen from './fund';

import LoginScreen from './Nails/Login';
import OverlayComponent from './overlay';
import RegisterScreen from './register';
//import LoginGeso from './Geso/login';
import LoaddingView from './Geso/Loadding';
//
//
import HomeScreen from './Geso/home';
import KHTHANG from './Geso/KeHoachThang/kehoachthang_container';
import Account from './Geso/account';
import MenuBC from './Geso/menuBaoCao';
import NppScene from './Geso/nhapp/nhaphanphoi.container';
import Bcltbh from './Geso/Lotrinhbanhang/baocao/Bc_ltbh_container';
import mapLtbhScene from './Geso/Lotrinhbanhang/map_ltbh/map_ltbh_container';
import BcLtOnlineScene from './Geso/LotrinhOnline/BaocaoLT/Bc_ltonline_container';
import mapLtOnlineScene from './Geso/LotrinhOnline/Map_ltOn/map_ltonline_container';
import KHN from './Geso/KeHoachNgay/kehoachNgay_container';
import CreateCus from './Geso/customers/create/CreateContainer';
import xdtdmap from './Geso/customers/create/map';
import BCNGAY from './Geso/Baocao/Baocao_ql_ngay/Bc_ql_ngay_container';
import BCTHANG from './Geso/Baocao/Baocao_ql_thang/Bc_ql_thang_container';
import BCCHITIETDH from './Geso/Baocao/Baocao_ql_chitietdh/Bc_ql_ctdh_container';
import ThongBaoMenu from './Geso/ThongBaoMenu';
import XemThongbao from './Geso/XemThongbao';
import XemWebFile from './Geso/XemWebFile';
import Khaosat from './Geso/KhaoSat/Khaosat';
import BCDOPHUSP from './Geso/Baocao/Baocaodophusp/Bc_ql_dophuso_container';
import BCKPI from './Geso/Baocao/BaocaoKPI/Bc_ql_kpi_container';
import BCKHTUYEN from './Geso/Baocao/BaoCaoKHTuyen';
import BCKPINV from './Geso/Baocao/BaocaoKPINhanvien';
import BCScore from './Geso/Baocao/Baocaoscore';
import BCChamcong from './Geso/Baocao/Baocaochamcong';
import BCChamcongQL from './Geso/Baocao/BaocaochamacongQL';
/////nails
import CreateAcount from './Nails/CreateAccount';

//-----
import Welcome from '../screen-nails/welcome';
import NewCustomer from '../screen-nails/new-customer';
import NewCustomerInfo from '../screen-nails/new-customer-info';
import OldCustomer from '../screen-nails/old-customer';
import SelectAppointment from '../screen-nails/select-appointment';
import YourNailsTech from '../screen-nails/your-nails-tech';
import YourService from '../screen-nails/your-prefer-service';
import ConfirmAppointmentTime from '../screen-nails/confirm-appointment-time';
import QRcode from '../screen-nails/QRcode'

const screens = {
  [IDs.Home]: HomeScreen,
  [IDs.Market]: MarketScreen,
  [IDs.Trade]: TradeScreen,
  [IDs.Fund]: FundScreen,
  //[IDs.Account]: AccountScreen,
  // [IDs.Login]: LoginScreen,
  [IDs.OverlayComponent]: OverlayComponent,
  [IDs.Register]: RegisterScreen,
  [IDs.LoginGeso]: LoginScreen,
  [IDs.LoaddingView]: LoaddingView,
  [IDs.KHTHANG]: KHTHANG,
  [IDs.Account]: Account,
  [IDs.MenuBC]: MenuBC,
  [IDs.NppScene]: NppScene,
  [IDs.BcLtbhScene]: Bcltbh,
  [IDs.mapLtbhScene]: mapLtbhScene,
  [IDs.BcLtOnlineScene]: BcLtOnlineScene,
  [IDs.mapLtOnlineScene]: mapLtOnlineScene,
  [IDs.KHN]: KHN,
  [IDs.CreateCus]: CreateCus,
  [IDs.xdtdmap]: xdtdmap,
  [IDs.BCNGAY]: BCNGAY,
  [IDs.BCTHANG]: BCTHANG,
  [IDs.BCCHITIETDH]: BCCHITIETDH,
  [IDs.ThongBaoMenu]: ThongBaoMenu,
  [IDs.XemThongbao]: XemThongbao,
  [IDs.XemWebFile]: XemWebFile,
  [IDs.Khaosat]: Khaosat,
  [IDs.BCDOPHUSP]: BCDOPHUSP,
  [IDs.BCKPI]: BCKPI,
  [IDs.BCKHTUYEN]: BCKHTUYEN,
  [IDs.BCKPINV]: BCKPINV,
  [IDs.BCScore]: BCScore,
  [IDs.BCChamcong]: BCChamcong,
  [IDs.BCChamcongQL]: BCChamcongQL,
  [IDs.CreateAcount]: CreateAcount,
  //---
  [IDs.Welcome]: Welcome,
  [IDs.NewCustomer]: NewCustomer,
  [IDs.NewCustomerInfo]: NewCustomerInfo,
  [IDs.OldCustomer]: OldCustomer,
  [IDs.SelectAppointment]: SelectAppointment,
  [IDs.YourNailsTech]: YourNailsTech,
  [IDs.YourService]: YourService,
  [IDs.ConfirmAppointmentTime]: ConfirmAppointmentTime,
  [IDs.QRcode]:QRcode
};

const registerScreens = (
  enhancers: Function = (a) => a,
  store: Object,
  Provider: Object
) => {
  // Loop through the array and register every screen in it.
  Object.keys(screens).map((screenID) => {
    Navigation.registerComponentWithRedux(
      screenID,
      () => enhancers(screens[screenID]),
      Provider,
      store
    );
    // Navigation.registerComponent(screenID, () => enhancers(screens[screenID]), store, Provider);
  });
};

// const registerScreens = (store: Object, Provider: Object) => {
//   Navigation.registerComponentWithRedux(ScreenIDs.Home, ()=> HomeScreen, Provider, store);
// }

StatusBar.setBarStyle('light-content');

export { registerScreens, IDs };
