import React from 'react';
import {
  Alert,
  View,
  Text,
  StyleSheet,
  Platform,
  PermissionsAndroid,
  BackHandler,
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import HomePresenter from './view/view';
import { IDs, } from '../..';
import { AppNavigation, } from '../../../app-navigation';
import ViewDialog from './../DialogView/DialogView';
import { CommonIcons, } from '../../../assets';
import { Logg } from '../../../utils';
import { Colors } from '../../../theme';
import { Navigation, } from 'react-native-navigation';

export default class HomeScreen extends React.Component {
  static options = {
    statusBar: {
      backgroundColor:  Colors.statusBar,
      // drawBehind: true,
      visible: true,
    },
    topBar: {
      visible: true,
      background: {
        color: Colors.HeaderColor,
      },
      title: {
	      text: 'Trang Chủ',
	    },
      rightButtons: [
        {
          id: 'btntopmenu',
          icon: CommonIcons.dots,           
        },
      ],
    },
  };
  constructor(){
    super();
    this.state={
      visiblePanel: false,
      currentCustomer: '',
      searchKeyword: '',
      location: null,
      currentDistance: '',
      noteVisit: '',
      isExit: true,
      step: 1,
  
      latitude: '',
      longitude: '',
  
      error: null,
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount(){
    this.navigationEventListener =   Navigation.events().bindComponent(this);
    this.requestLocationPermission();
    this.laydoado ();

  }
  navigationButtonPressed({ buttonId, }) {
    if (buttonId === 'btntopmenu') {
      let { navigator, } = this.props;
      navigator._push({
        name:IDs.Account,  //.Register,
        passProps: {
          data:this.state.location,
        },
        options: {         
          topBar: {
            title: {
              text:'Tài Khoản',
            },
          
          },
          bottomTabs:{
            visible: false,
            ...Platform.select({ android: { drawBehind: true, }, }),
          },
        },
       
      }); 


    }
  }
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
  
    return true;
  }
  laydoado () {

    let hasLocationPermission = true ;

    if (hasLocationPermission) {

      Geolocation.getCurrentPosition(
        (position) => {
          this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            location:position.coords,
  
          });
        },
        (error) => Logg.error(JSON.stringify(error)),
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000,}
      );

      
/////

      this.watchId =  Geolocation.watchPosition((position) => {
        Logg.info('position'+ JSON.stringify(position) );
      
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          location:position.coords,

        });
      },
      (error) => {
        // See error code charts below.
        Logg.info(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 0, distanceFilter: 3,}
      );
    }



   
  }
  componentWillUnmount = () => {
    Geolocation.clearWatch(this.watchID);
    this.setState({ isanimation: false, });
  }
  requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'SalesUp đang lấy vị trí của bạn',
          'message': 'Di chuyển để cập nhật vị trí chính xác hơn.' +
		', Otherwise app will not work',
		
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.laydoado ();
        console.log('You can use the location');
      } else {
        //Alert.alert('Thông báo','Không thể cập nhật được vị trí (1)',false,() => { });
        console.log('Location permission denied');
      }
    } catch (err) {
      //Alert.alert('Thông báo','Không thể cập nhật được vị trí (2)',false,() => { });
      console.log('SalesUp'+err);
		  console.warn(err);
    }
  }

  chooseGridMenu(menu){
    let { navigator, } = this.props;
 

    const menuLTBH = [
      { ten: 'Theo dõi bản đồ', key: '1', },
      { ten: 'Xem báo cáo', key: '2', },
    ];
    const menuLTOnline = [
      { ten: 'Theo dõi bản đồ', key: '1', },
      { ten: 'Xem báo cáo', key: '2', },
    ];

    switch (menu.key) {
    
   
       
      case 'noti':
        navigator._push({
          name:IDs.ThongBaoMenu,  //.Register,
          passProps: {
            data:this.state.location,
          },
          options: {         
            topBar: {
              title: {
                text:'Thông Báo',
              },
            
            },
            bottomTabs:{
              visible: false,
              ...Platform.select({ android: { drawBehind: true, }, }),
            },
          },
         
        }); 
        break;
      case 'review':
        navigator._push({
          name:IDs.Khaosat ,  //.Register,
          passProps: {
            data:this.state.location,
          },
          options: {         
            topBar: {
              title: {
                text:'Khảo sát',
              },
              
            },
            bottomTabs:{
              visible: false,
              ...Platform.select({ android: { drawBehind: true, }, }),
            },
          },
           
        });
        break;
  
      case 'Nhaphanphoiql':
        navigator._push({
          name:IDs.NppScene,  //.Register,
          passProps: {
            data:this.state.location,
         
          },
          options: {         
            topBar: {
              title: {
                text:'Nhà Phân Phối',
              },
             
            },
            bottomTabs:{
              visible: false,
              ...Platform.select({ android: { drawBehind: true, }, }),
            },
          },
         
        });
        break;
      case 'LtBanhang':
        
        AppNavigation.showCommonDialog({renderContent: ()=> 
          <ViewDialog  title={'Menu Lộ Trình Bán Hàng'} 
            content={menuLTBH} 
            onPressItem ={(item)=>this.onPressItem(item,'LtBanhang')} />
          ,  });      
        break;

      case 'LtOnline':
        AppNavigation.showCommonDialog({renderContent: ()=> 
          <ViewDialog  title={'Menu Lộ Trình Online'} 
            content={menuLTOnline} 
            onPressItem ={(item)=>this.onPressItemLTOnline(item)} />
          ,  });  
        break;
      case 'customer':
        navigator._push({
          name:IDs.CreateCus,  //.Register,
          passProps: {
            data:this.state.location,
          },
          options: {         
            topBar: {
              title: {
                text:'Tạo Khách Hàng',
              },
              rightButtons: [
                {
                  id: 'btntop',
                  icon: CommonIcons.done, 
                  passProps: {
                    size: 24,
                    color: 'black',
                    onPress: this.handlePressSettings,
                  },          
                },
              ],
            },
            bottomTabs:{
              visible: false,
              ...Platform.select({ android: { drawBehind: true, }, }),
            },
          },
         
        });
        break;


    }
  }

  onPressItem(item,loai){
    let { navigator, } = this.props;
    if(item){
      if(loai==='LtBanhang'){
        if(item.ten==='Xem báo cáo'){
          navigator._push({
            name:IDs.BcLtbhScene,  //.Register,
            // passProps: {
            //   data:this.state.location,
           
            // },
            options: {         
              topBar: {
                title: {
                  text:'Báo cáo LTBH',
                },
                rightButtons: [
                  {
                    id: 'btntop',
                    icon: CommonIcons.done,           
                  },
                ],
              },
              bottomTabs:{
                visible: false,
                ...Platform.select({ android: { drawBehind: true, }, }),
              },
            },
           
          });
        }
        else if(item.ten ==='Theo dõi bản đồ'){
        
          navigator._push({
            name:IDs.mapLtbhScene,  
            options: {         
              topBar: {
                title: {
                  text:'Lộ trình BH',
                },
                rightButtons: [
                  {
                    id: 'btntop',
                    icon: CommonIcons.done,           
                  },
                ],
              },
              bottomTabs:{
                visible: false,
                ...Platform.select({ android: { drawBehind: true, }, }),
              },
            },
          } );
        }
      }
    }
  }
  onPressItemLTOnline(item){
    let { navigator, } = this.props;
    if(item){
      if(item.ten==='Xem báo cáo'){
        navigator._push({
          name:IDs.BcLtOnlineScene, 
 
          options: {         
            topBar: {
              title: {
                text:'Báo cáo LT Online',
              },
              rightButtons: [
                {
                  id: 'btntop',
                  icon: CommonIcons.done,           
                },
              ],
            },
            bottomTabs:{
              visible: false,
              ...Platform.select({ android: { drawBehind: true, }, }),
            },
          },
         
        });
      }
      else if(item.ten ==='Theo dõi bản đồ'){
        
        navigator._push({
          name:IDs.mapLtOnlineScene,  
          options: {         
            topBar: {
              title: {
                text:'Lộ trình Online',
              },
              rightButtons: [
                {
                  id: 'btntop',
                  icon: CommonIcons.done,           
                },
              ],
            },
            bottomTabs:{
              visible: false,
              ...Platform.select({ android: { drawBehind: true, }, }),
            },
          },
        } );
      }
    }
  }

  render(){
    return (
      <HomePresenter
         	chooseGridMenu={(menu)=>this.chooseGridMenu(menu)}
      ></HomePresenter>
    );
  }
}
