
import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Platform,
  ScrollView,
} from 'react-native';

import Icons from '../../../../assets/Icons';
import Colors from '../../../../theme/Colors';
import { TextCmp, } from '../../../../common-components';

//menu  gridMenus QL
const gridMenus = [
  { key: 'Nhaphanphoiql', label: 'Nhà Phân Phối', icon: 'radar',subtext:'Viếng thăm, rời đi , chụp hình nhà phân phối',  },
  { key: 'LtBanhang', label: 'LT Bán Hàng', icon: 'feedback',subtext:'Xem báo cáo hoặc bản đồ các khác hàng của cấp dưới',    },
  { key: 'noti', label: 'Thông báo', icon: 'bell',subtext:'Các thông báo từ trung tâm về các chương trình khuyến mãi, các chính sách mới... ',   },
  { key: 'LtOnline', label: 'LT Online', icon: 'setlocation', subtext:'Xem lộ trình của cấp dưới theo thời gian thực, báo cáo tình hình đi khách hàng của cấp dưới',   },//target
  { key: 'review', label: 'Khảo Sát', icon: 'target',subtext:'Khảo sát từ trung tâm thông qua các câu hỏi trắc ngiệm ',    },
  { key: 'customer', label: 'Khách hàng', icon: 'username',subtext:'Tạo mới khách hàng ',    },
];

export default class HomePresenter extends React.Component  {

  componentDidMount(){
    //StatusBar.setHidden(false);
  }
  shouldComponentUpdate(){
    return false;
  }
  // press=()=>{
  //   alert('xxx');
  // }
  render(){
    const{
      chooseGridMenu,
    }=this.props;
    return (
      <View style={styles.container}>
     
        <View style={styles.fill}>
          {gridMenus.map((menu,index) => {
            return (
              <TouchableOpacity
                style={styles.items}
                key={index+''}
                onPress={()=>chooseGridMenu(menu)}
              >
                <Icons name={menu.icon} color={ Colors.HeaderColor} size={55} />
                <TextCmp style={styles.text}>
                  {menu.label}
                </TextCmp>
                {/* <TextCmp style={styles.textsub} numberOfLines ={3}>
                  {menu.subtext}
                </TextCmp> */}
              </TouchableOpacity>
            );
          })}
 
        </View>

       
      </View>
    );

    
  }
  
}
var { height, width, } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent:'center',
    flex: 1,
    backgroundColor:Colors.backgroundL1,
    width,
    
  },
  text:{
    fontSize: 16,
    fontWeight: '400',
    color: 'black',
    marginTop: 15,
  //fontFamily: "Panettone",
  },
  textsub:{
    fontSize: 14,
    textAlign:'center',
    padding:5,
    color:'gray',
    //marginTop:3,
  },
  fill:{ marginTop:15, flexDirection: 'row', flexWrap: 'wrap',  alignItems: 'center',justifyContent:'center', width,   flex: 1,},
  items: {

    width: width/2-15,
    height: height/4-10,
    margin:6,
    justifyContent:'center',
    alignItems:'center',
    // ios
    borderRadius: 10,
    backgroundColor: '#FCFCFF',
    
    shadowOffset: { width: 0, height: 13, },
    shadowOpacity: 0.3,
    shadowRadius: 6,
  

    //android (Android +5.0)
    elevation: 3,

    borderColor:Colors.HeaderColor,
    //borderWidth: Platform.OS==='android'?1:0,
  },

});
