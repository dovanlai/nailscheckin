import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  Keyboard,
  Image,
  ActivityIndicator,
  StatusBar,
  Dimensions
} from 'react-native';
import Icon from '../../../assets/Icons';
import { Colors, } from '../../../theme';
import { TextCmp, } from '../../../common-components';
import{CheckBox,}from 'react-native-elements';
import { BottomBarAssets, ImagesIcons, } from '../../../assets';
import Checkupdate from './checkupdate';

const {width}= Dimensions.get('window')
export default class LoginView extends React.Component{

  componentDidMount(){
    // StatusBar.setHidden(true);
  }
  shouldComponentUpdate(userName,pass,checked){
    return userName!==this.props.userName || pass!==this.props.pass ||checked!==this.props.checked;
  }
  render(){
    const { onLogin,
      onChangeText,
      pass,
      userName,
      checked,
      checkedclick,Loading,}= this.props;
    return (
      <TouchableOpacity activeOpacity={1} onPress={() => Keyboard.dismiss()} style={styles.container}>  
         
         <Image style={{width:width/2,height:width/2,}} source={ ImagesIcons.logooneone}></Image>
        <View style={styles.viewInput}>
          {/* <Image source={BottomBarAssets.Account} style={styles.icon_account} ></Image> */}
          <Icon name="username" size={18} />
          <View style={{ flex: 5, marginLeft: 10, }}>
            <TextInput
              underlineColorAndroid="transparent"
              underlineColorAndroid="transparent"
              placeholder="Tai khoan"
              style={styles.textInput}
              value={userName}
              onChangeText={(text) => onChangeText(text, 'userName')}
            />
          </View>
        </View>
        <View style={styles.viewInput}>
          {/* <Image source= {BottomBarAssets.lock} style = {styles.icon_account}></Image> */}
          <Icon name="pass" size={18} />
          <View style={{ flex: 5, marginLeft: 10, }}>
            <TextInput
              underlineColorAndroid="transparent"
              placeholder="Mat khau"
              style={styles.textInput}
              value={pass}
              secureTextEntry
              onChangeText={(text) => onChangeText(text, 'pass')}
            />
          </View>
        </View>
        <CheckBox
          center
          title='Lưu đăng nhập'
          checked={checked}
          checkedIcon='check-square'
          uncheckedIcon='square'
          checkedColor={Colors.selectedColor}
          size={14}
          containerStyle={{alignItems:'flex-end', backgroundColor: Colors.backgroundL2,width:'100%',borderColor:Colors.backgroundL2, }}
          textStyle={{fontSize:12,fontWeight:'normal',color:'gray',}}
          onPress={checkedclick}
        />


        <TouchableHighlight
          disabled={Loading}
          onPress={onLogin}
          style={styles.btnLogin}
          underlayColor="transparent"
        >
          <View>
            {Loading?  <ActivityIndicator size="small" color="#00ff00" />:
              <TextCmp style={styles.loginColor}>Login</TextCmp>}
          </View>
          
        </TouchableHighlight>
        <Checkupdate></Checkupdate>
        {/* <Text onPress={onLogin}>Click to Login</Text>
                    <Text>{loginStatus}</Text> */}
                    
        {/* <Text style={{ fontSize: 8, margin: 20, color: 'gray', }}>{mystring.vs_name + mystring.mode}</Text> */}

      </TouchableOpacity>
    );
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 30,
    paddingRight: 30,
    backgroundColor: Colors.backgroundL2,
  },
  viewInput: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#9e9e9e',
    marginBottom: 10,
  },
  textInput: { color: '#9e9e9e', height: 40, },
  btnLogin: {
    flexDirection: 'row',
    width: '100%',
    height: 40,
    backgroundColor: Colors.selectedColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    marginTop: 10,
  },
  loginColor: { color: Colors.backgroundL1, },
  icon_account:{width:20,height:20,tintColor:'gray',},
});
