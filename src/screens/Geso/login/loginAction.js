import { post, } from '../../../utils/netwworking';
export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
import { Logg, } from '../../../utils';

export function login(params) {
  return (dispatch) => {
    // alert(JSON.stringify(params));
    return post('NhanVien_DangNhap', params)
      .then(({ error, data, }) => {
        if (!error) {
          if (data[0].RESULT == '1') {
            dispatch({
              type: LOGIN_SUCCESS,
              payload: {
                userName: params.userName,
                ddkdId: data[0].DDKDID,
              },
            });

            return data[0];
          } else {
            dispatch({
              type: LOGIN_FAIL,
              message: data[0].MSG,
            });
            return data[0];
          }

        } else {
          dispatch({
            type: LOGIN_FAIL,
            message: data,
          });
          return data[0];
        }
      })
      .catch((e) => {
        // dispatch(appActions.showLoading(false));
        dispatch({
          type: LOGIN_FAIL,
          message: '',
        });
        return false;
      });
  };
}
export function getversion(params) {
  return () => {
    Logg.info('CheckVersion', params);
    return post('CheckVersion', params)
      .then(({ data, error, }) => {
        Logg.info('CheckVersion data', data);
        if (!error) {
          // alert(data)
          return data;
        } else {
          Logg.info('getPromotion fail');
          return false;
        }
      })
      .catch((e) => {
        Logg.info('getPromotion catch', e);
        return false;
      });
  };
}