import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  Keyboard,
  Platform,
  AsyncStorage,
} from 'react-native';
import * as AppsController from '../../../AppController';
import { TextCmp, } from '../../../common-components';
import * as LoginAction from './loginAction';
import { AppNavigation, } from '../../../app-navigation';
import {Logg,} from './../../../utils';
import LoginView from './LoginView';
import ViewLoadding from './../Loadding/index';
import { IDs, } from '../..';
import { saveListBranch, } from '../../../stores/app/action';
import codePush from 'react-native-code-push';

let codePushOptions = { checkFrequency: codePush.CheckFrequency.MANUAL, };

class LoginGeso extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false, 
      user: {
        userName: '',
        pass: '',
        buildVersion:'1.0',
        isAndroid: Platform.OS === 'android' ? 1 : 0,
      },
      Skiplogin: 1,
    };
  }
  onChangeText = (text, name) => { 
    const { user, } = this.state;
    user[name] = text;
    this.setState({ user, });
  };

onPressLogin = () => {
  const { userName, pass, } = this.state.user;
  if (userName !== '' && pass !== '') {
    this.setState({Loading:true,});
    this.props.dispatch(LoginAction.login(this.state.user)).then((_) => {
      this.setState({Loading:false,});
      if (_.RESULT === '1') {
        //luudata login
        this.props.dispatch(saveListBranch(_));
        this.goHome();
      } else {
        AppNavigation.showCommonDialog({
          title: 'Đăng nhập lỗi',
          desc: ''+_.MSG,
        });
      }
    }).catch( (e) => Logg.error('login'+e));

    /////save data login
    const user = this.state.user;
    let multi_set_pairs = [
      ['userName', '' + user.userName,],
      ['pass', '' + user.pass,],
    ];

    if (this.state.checked) {
      AsyncStorage.multiSet(multi_set_pairs, (e) => {
        Logg.error(e);
      });
    }
  } else {
    AppNavigation.showCommonDialog({
      title: 'Đăng nhập không thành công',
      desc: 'Không được bỏ trống tên Tài khoản và Mật khẩu',
    });
  
  }
};
componentDidMount(){
  const user = this.state.user;
  AsyncStorage.getAllKeys((err, keys) => {
    AsyncStorage.multiGet(keys, (err, stores) => {
      stores.map((result, i, store) => {
        // get at each store's key/value so you can work with it
        let key = store[i][0];
        let val = store[i][1];

        if (key === 'userName' && val.length > 0) {
          user.userName = val;
          this.setState({ user, checked: true, });
        }
        if (key === 'pass' && val.length > 0) {
          user.pass = val;
          this.setState({ user, });
        }

      });
    });
  });
}
checkedclick() {

  const user = this.state.user;
  let multi_set_pairs = [
    ['username', '' + user.userName,],
    ['pass', '' + user.pass,],
  ];
  let multi_set_ = [
    ['username', '',],
    ['pass', '',],
  ];

  if (!this.state.checked) {
    AsyncStorage.multiSet(multi_set_pairs, (err) => {
    });
  } else {
    AsyncStorage.multiSet(multi_set_, (err) => {
    });
   // this.onChangeText('', 'userName');
   // this.onChangeText('', 'pass');
  }
  this.setState({ checked: !this.state.checked, });

}
goHome(){
  AppsController.startHome();
}

getversion(){
  this.props.dispatch(LoginAction.getversion({code:'mystring.vs_code',name:'mystring.vs_name',})).then((_) => {
    if(_===0){
      if(Platform.OS==='android')
      {this.setState({isshowupdate:true,});}
    } 
  }).catch();

}
render(){
  let {  loginStatus, } = this.props;
  const { userName, pass, } = this.state.user;
  return (
    <LoginView
      onPressStatus={() => {
        //Toast.show('abc', Toast.POSITION.CENTER, Toast.LENGTH.SHORT);
      }}
      onLogin={this.onPressLogin}
      loginStatus={loginStatus}
      onChangeText={this.onChangeText}
      userName={userName}
      pass={pass}
      checked={this.state.checked}
      checkedclick={() => this.checkedclick()}
      SkipLogin= {this.state.Skiplogin}
      Loading={this.state.Loading}
    />
  );
}
}
export default codePush(codePushOptions)(LoginGeso);