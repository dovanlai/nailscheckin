import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  WebView,
} from 'react-native';
import { Colors, } from '../../../theme';
import { TextCmp, } from '../../../common-components';

export default class SceneQuyenloi extends React.Component {
  static options = {
    statusBar: {
      backgroundColor: Colors.statusBar,
      visible: true,
    },
    topBar: {
      visible: true,
      background: {
        color: Colors.HeaderColor,
      },
    
      title: {
        text: 'Quyền Lợi ShopKey',
      },
      backButton: {
        color: '#fff',
      },
    },
  };
  render(){
    return (
      <View style={{flex:1,}}>     
        <WebView
          source={{
            uri: `https://docs.google.com/gview?embedded=true&url=${this.props.data}`,
          }}
        />        
      </View>
    );
  }
}
