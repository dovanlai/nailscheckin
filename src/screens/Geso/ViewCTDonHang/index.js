
import React from 'react';
import {
  View,
  ActivityIndicator,
  StyleSheet,
  Alert,
  TouchableOpacity,
  WebView
} from 'react-native';
import {connect,}from 'react-redux';
import { getchitietdonhang, ChotDhQL, } from './actions';
//import { WebView, } from 'react-native-webview';
import formatter from './../fm';
import { Colors, } from '../../../theme';
import { TextCmp, } from '../../../common-components';
import { SnackBar, } from '../../../utils';
class XemCTDH extends React.Component {
  constructor(){
    super();
    this.state={
      report:'',
    };
  }
  componentDidMount(){
    this.getdata();

  }
  getdata(){
    let params={
      dhId:this.props.data.sodonhang,
    }; 
    this.props.dispatch(getchitietdonhang(params)).then((res)=>{
     
      if(res[0].RESULT=='1'){
        this.setState({report:res[0].MSG,}); 
      }
    });
  }
  chot =()=>{
    //ChotDhQL(string dhId,string nppId,string ddkdId)
    let params={
      dhId:this.props.data.sodonhang,
      nppId:this.props.data.nppId,
      ddkdId: this.props.data.ddkdId,
    }; 
    // alert(JSON.stringify(params));
    this.props.dispatch(ChotDhQL(params)).then((res)=>{

      if(res[0].RESULT=='1'){
        SnackBar.showSuccess(res[0].MSG);
      }
      else{
        Alert.alert('Thông báo',res[0].MSG);
      }

    });
  }
  render(){
    //alert( this.state.report);
    const item = this.props.data;
    return (
      <View style={styles.container}>
        <View style={styles.viewif}>
          <TextCmp >KH: {this.props.data.khTen}</TextCmp>
          <TextCmp style={styles.subtext} >ĐC: {this.props.data.DIACHI}</TextCmp>
          <TextCmp style={styles.subtext}>Ngày nhập: {item.ngaynhap}</TextCmp>
          <TextCmp style={styles.subtext}>Tổng tiền: { formatter.format(item.tonggiatri)} vnđ</TextCmp>
          <TextCmp style={styles.subtext}>Số ĐH: {item.sodonhang}</TextCmp>

        </View>
       
        {/* <TextCmp >Sản phẩm đơn hàng: </TextCmp> */}
        <View  style={styles.viewst}>
          {this.state.report===''?<ActivityIndicator size={'large'} color={Colors.HeaderColor} />:
            <WebView
              originWhitelist={['*',]}
              source={{ html: this.state.report, baseUrl: '', }}
              //scalesPageToFit
            />}
        </View>
        <TouchableOpacity onPress= {this.chot} style={styles.btnchot}>
          <TextCmp  style={{color:'white',}}>Chốt</TextCmp>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  btnchot:{
    position:'absolute', bottom:3,
    height:35, justifyContent:'center',alignItems:'center', 
    margin:5,borderColor:Colors.HeaderColor,borderWidth:1,
    backgroundColor:Colors.HeaderColor,marginHorizontal:20,
    width:'90%',
    shadowOffset: { width: 0, height: 2, },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    //android
    elevation: 1,
  },
  viewst:{ flex:1, 
    //marginBottom:45, 
   },
  viewif:{
    margin:5,

    shadowOffset: { width: 0, height: 5, },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    //android
    elevation: 1,
  },
  subtext:{
    fontSize:14,
    marginTop:4,
    
  },
});

export default connect(
  (state) => ({
    //   listBranch: state.app.listBranch,
    //   SearchData: state.SearchData,
  }),
  (dispatch) => ({ dispatch, }),
)(XemCTDH);