import React from 'react';
import { connect, } from 'react-redux';
import { report, } from './action';
import moment from 'moment';
import { Navigation, } from 'react-native-navigation';
import htmlloading from '../../../../common-components/htmlLoading';
import ViewWebBC from '../ViewWebBC';

class BaocaoTonNpp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentBranch: 'Chọn NPP',
      visiblePanel: false,
      searchKeyword: '',
      currentCustomer: '',
      payload: {

        tungay: moment(new Date()).format('YYYY-MM-DD'),
        denngay: moment(new Date()).format('YYYY-MM-DD'),

      },
      report: '',
      ansearch:false,
    };
  }

  componentDidMount() {
    this.navigationEventListener =   Navigation.events().bindComponent(this);
    // this.getReport();
  }
  navigationButtonPressed({ buttonId, }) {
    if (buttonId === 'btntop') {
      this.getReport();
    }
  }
	getReport = () => {
	  // (String nppId, String ddkdId, String type, String tungay, String denngay, String id, String userId, String loai)
	  let params = {
	    nppId: this.props.SearchData.NppId ==null?'':this.props.SearchData.NppId,
	    ddkdId: this.props.SearchData.NvbhId ==null?'':this.props.SearchData.NvbhId,
	    type:'',
	    tungay: this.state.payload.tungay,
	    denngay: this.state.payload.denngay,
	    id:this.props.listBranch.ID==null?'':this.props.listBranch.ID,
	    userId: this.props.listBranch.USERID,
	    loai:this.props.listBranch.LOAI,

        
	  };
	
	    const { dispatch, } = this.props;
	    this.setState({report:htmlloading,});
	    dispatch(report(params)).then((res) => {
	      if (res[0].RESULT=='1') {
	        this.setState({ report: res[0].content, });
	      } else this.setState({ report: '<h1>Không có dữ liệu</h1>', });
	    });


	};



	setToTime = (date) => {
	  const { payload, } = this.state;
	  payload.denngay = date;
	  this.setState({ payload, });
	};

	setFromTime = (date) => {
	  const { payload, } = this.state;
	  payload.tungay = date;
	  this.setState({ payload, });
	};
	ansearch=()=>{
	  const a=this.state.ansearch;
	  this.setState({ansearch:!a,});
	}

	render() {
	  return (
	    <ViewWebBC 
	      setToTime={this.setToTime}
	      setFromTime={this.setFromTime}
	      getReport={this.getReport}
	      time={this.state.payload}
	      report={this.state.report}
	      ansearch={this.ansearch}
	      isan={this.state.ansearch}
	      antungaydenngay={true}
	      anchonnvbh={true}
	    />
	  );
	}
}

export default connect(
  (state) => ({
    listBranch: state.app.listBranch,
    SearchData: state.SearchData,
  }),
  (dispatch) => ({ dispatch, }),
)(BaocaoTonNpp);
