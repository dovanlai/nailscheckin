import React from 'react';
import { connect, } from 'react-redux';
import CreateView from './CreatePresenter';
import {Alert,Platform,}from 'react-native';
import { getNewInfo, getQuanHuyen, createCustomer, getTuyen, getphuongxa,} from './create.action';
import ImagePicker from 'react-native-image-picker';
import Geolocation from 'react-native-geolocation-service';
import ViewDialog from './../../DialogView/DialogView';
import { AppNavigation, } from '../../../../app-navigation';
import { IDs, } from '../../..';
import { Logg, } from '../../../../utils';
import { CommonIcons, } from '../../../../assets';
import { Navigation, } from 'react-native-navigation';
import ViewLoadding from './../../Loadding/index';

const createListDropdown = (string) => {
  let list = [];

  let index = string.indexOf(';');
  let indexOfHyphen = null;

  // string = string.slice(index + 1, string.length);
  // index = string.indexOf(';');

  while (index !== -1) {
    let temp = string.slice(0, index);
    indexOfHyphen = temp.indexOf('-');
    list.push({
      value: temp.slice(0, indexOfHyphen),
      label: temp.slice(indexOfHyphen + 1, temp.length),
    });
    string = string.slice(index + 1, string.length);
    index = string.indexOf(';');
  }
	
  indexOfHyphen = string.indexOf('-');
  list.push({
    value: string.slice(0, indexOfHyphen),
    label: string.slice(indexOfHyphen + 1, string.length),
  });
  return list;
};


class CreateContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
		
      data: {
        npp: {
          value: '',
          text: '',
          isValidate: false,
          label: 'Nhà Phân Phối',
          title: 'Nhà Phân Phối',
          err: 'Vui lòng chọn npp',
          list:[],
        },
        nvbh: {
          value: '',
          text: '',
          isValidate: false,
          label: 'Nhân viên',
          title: 'Nhân viên',
          err: 'Vui lòng chọn nvbh',
          list:[],
        },

        ten: {
          value: '',
          isValidate: false,
          label: 'Tên đơn vị(*)',
          err: 'Vui lòng nhập tên đơn vị',
        },
        ngdd: {
          value: '',
          isValidate: false,
          label: 'Tên người đại diện(*)',
          err: 'Vui lòng nhập tên người đại diện',
        },
        diachi: {
          value: '',
          isValidate: false,
          label: 'Địa chỉ (*)',
          err: 'Vui lòng nhập địa chỉ ',
        },
        vtkh: {
          value: '',
          text: '',
          isValidate: false,
          label: 'Vị trí KH(*)',
          title: 'Chọn vị trí khách hàng',
          err: 'Vui lòng nhập vị trí khách hàng',
          list:[],
        },
				
        hangkh: {
          value: '',
          text: '',
          isValidate: true,
          label: 'Hạng KH(*)',
          title: 'Chọn hạng khách hàng',
          err: 'Vui lòng chọn hạng khách hàng',
          list:[],
        },
        tinhthanh: {
          value: '',
          text: '',
          isValidate: false,
          label: 'Tỉnh/Thành(*)',
          title: 'Chọn tỉnh thành',
          err: 'Vui lòng nhập tỉnh thành',
          list:[],
        },
        quanhuyen: {
          value: '',
          text: '',
          isValidate: false,
          label: 'Quận/Huyện(*)',
          title: 'Chọn quận huyện',
          err: 'Vui lòng nhập quận huyện',
          list:[],
        },
        // xa: {
        //   value: '',
        //   isValidate: true,
        //   label: 'Phường/Xã',

        // },
        xa: {
          value: '',
          text: '',
          isValidate: true,
          label: 'Chọn phường xã',
          title: 'Chọn phường xã',
		  err: 'Vui lòng chọn phường xã',
		  list:[],
        },
        dienthoai: {
          value: '',
          isValidate: true,
          label: 'Số điện thoại',
        },
        kenhbh: {
          value: '',
          text: '',
          isValidate: true,
          label: 'Kênh bán hàng(*)',
          title: 'Chọn kênh bán hàng',
          err: 'Vui lòng chọn kênh bán hàng',
          list:[],
        },
        tanso: {
          value: '',
          text: '',
          isValidate: false,
          label: 'Tần số viếng thăm(*)',
          title: 'Chọn tần số viếng thăm',
          err: 'Vui lòng nhập tần số viếng thăm',
          list:[],
        },
        msthue: {
          value: '',
          isValidate: true,
          label: 'Mã số thuế',
          err: 'Vui lòng nhập mã số thuế',
        },
        toaDo: {
          value: '',
          isValidate: false,
          err: 'Vui lòng nhập tọa độ',
        },
        chuphinh: {
          value: '',
          isValidate: false,
          source: null,
          err: 'Vui lòng chụp ảnh đại diện',
        },
        trangthai: {
          value: '',
          isValidate: true,
          label: 'Hoạt động',
        },
        nvgn: {
          value: '',
          text: '',
          isValidate: true,
          label: 'NVGN',
          title: 'Chọn NVGN',
          err: 'Vui lòng nhập NVGV',
          list:[],
        },
        loaikh: {
          value: '',
          text: '',
          isValidate: false,
          label: 'Loại hàng khách(*)',
          title: 'Chọn loại hàng khách',
          err: 'Vui lòng chọn loại hàng khách',
          list:[],
        },
        nhomkh: {
          value: '',
          text: '',
          isValidate: true,
          label: 'Nhóm hàng khách(*)',
          title: 'Chọn nhóm hàng khách',
          err: 'Vui lòng chọn nhóm hàng khách',
          list:[],
        },
        tuyenbh: {
          value: '',
          text: '',
          isValidate: true,
          label: 'Tuyến bán hàng',
          title: 'Chọn Tuyến BH',
          err: 'Vui lòng chọn Tuyến BH',
          list:[], 
        },
	  },
	  location:null,
    };
  }

  componentDidMount() {
    this.navigationEventListener =   Navigation.events().bindComponent(this);
    this.laytoado();
    const { dispatch, } = this.props;
    const { data, } = this.state;
    dispatch(
      getNewInfo({ ddkdId: this.props.listBranch.USERID , nppId:'', }, (dt) => {
        if (dt) {
					
          data.vtkh.list = createListDropdown(dt[0].HANGCUAHANG);

          // data.hangkh.list = createListDropdown(dt[0].VITRIKH);
          data.kenhbh.list = createListDropdown(dt[0].KENHBANHANG);
          data.tinhthanh.list = createListDropdown(dt[0].TINHTHANH);
          data.nvgn.list = createListDropdown(dt[0].NVGN);
          data.tanso.list = createListDropdown(dt[0].TANSO);
          data.loaikh.list = createListDropdown(dt[0].LOAICUAHANG);
		  data.nhomkh.list = createListDropdown(dt[0].NHOMKHACHHANG);
		  data.phuongxa.list = [];

		  
          this.setState({ data, });
        }
      }),
    );

    //npp
    const datasv = JSON.parse(this.props.listBranch.NPPLIST);
    datasv.map((item) => {
      item2 = {
        label: item.ten,
        value: item.pk_seq,
      };
      data.npp.list.push(item2);
    });
    //nhanvienbh
    const datasvnv = JSON.parse(this.props.listBranch.SRLIST);
    datasvnv.map((item) => {
      item2 = {
        label: item.ten,
        value: item.pk_seq,
        nppSearch: item.nppSearch,
      };
      data.nvbh.list.push(item2);
    });
    //
	

  }
  navigationButtonPressed({ buttonId, }) {
    if (buttonId === 'btntop') {
	  this.createCustomer();
    }
  }
  laytoado(){
    let hasLocationPermission = true ;
    if (hasLocationPermission) {

	
      Geolocation.getCurrentPosition(
        (position) => {
          this.setState({location:position.coords,});
        },
        (error) => Logg.create(JSON.stringify(error)),
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000,}
      );

      
      /////

      this.watchId =  Geolocation.watchPosition((position) => {
        Logg.info('position'+ JSON.stringify(position) );
      
        this.setState({location:position.coords,});
      },
      (error) => {
        // See error code charts below.
        Logg.info(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 0, distanceFilter: 3,}
      );
    }
  }
  getlisttuyen(){
    //homeActions
    // alert('xx'+this.state.data.npp.value.length+this.state.data.nvbh.value.length)
	

    if(this.state.data.npp.value!=='' && this.state.data.nvbh.value !== ''){
			
      let param ={
        ischonnpp: 1,
        nppId: this.state.data.npp.value,
        ddkdId: this.state.data.nvbh.value,
      };
      this.props.dispatch( getTuyen(param)).then((data) => {
        if (data) {	
          //alert(JSON.stringify(data))
          let listTuyen = [];
          data.map((x)=>{
            let t = {label:x.tbhTen,value:x.tbhId,}; 
            listTuyen.push(t);
          });
          this.setState({listTuyen,});
        } else {
          //Notify.show('Lỗi', 'Không lấy được thông tin tuyến bán hàng');
        }
      });
		
		
				
    }else{
      //	Notify.show('', 'vui lòng chọn n);
    }

  }

 
	onChangeText = (text, name) => {
	  const { data, } = this.state;

	  data[name].value = text;

	  if (data[name].err && text === '') {
	    data[name].isValidate = false;
	  }

	  if (data[name].err && text !== '') {
	    data[name].isValidate = true;
	  }

	  this.setState({ data, });
	};

	onChangeDate = (date, name) => {
	  const { data, } = this.state;

	  data[name].value = date;
	  data[name].isValidate = true;

	  this.setState({ data, });
	};
	capnhattuyen(listTuyen){
	  const { data, } = this.state;
	  data['tuyenbh'].value='';
	  data['tuyenbh'].text = '';
	  let dem = 0;
	  listTuyen.map((item)=>{
	    if(item.isselect==true){
	      dem++;
	      data['tuyenbh'].text +=' - '+ item.label;
	      data['tuyenbh'].value +='#'+ item.value;
	    }
	  });

	
	  data['tanso'].list.map((TanSoList)=>{
	    switch(dem)
	    {
	      case 1:

	        break;
	      case 2:
	        //alert(JSON.stringify(TanSoList) )
	        if(TanSoList.label=='F8')
	        {
	          data['tanso'].text = TanSoList.label;
	          data['tanso'].value = TanSoList.value;
	          data['tanso'].isValidate = true;
	        }
	        break;
	      case 3:
	        if(TanSoList.label=='F12')
	        {
	          data['tanso'].text = TanSoList.label;
	          data['tanso'].value = TanSoList.value;
	          data['tanso'].isValidate = true;
	        }

	        break;
	      case 4:
	        if(TanSoList.label=='F16')
	        {
	          data['tanso'].text = TanSoList.label;
	          data['tanso'].value = TanSoList.value;
	          data['tanso'].isValidate = true;
	        }
	        break;
	      case 5:
	        if(TanSoList.label=='F20')
	        {
	          data['tanso'].text = TanSoList.label;
	          data['tanso'].value = TanSoList.value;
	          data['tanso'].isValidate = true;
	        }
	        break;
	      case 6:
	        if(TanSoList.label=='F24')
	        {
	          data['tanso'].text = TanSoList.label;
	          data['tanso'].value = TanSoList.value;
	          data['tanso'].isValidate = true;
	        }
	        break;
	      case 7:
	        if(TanSoList.label=='F28')
	        {
	          data['tanso'].text = TanSoList.label;
	          data['tanso'].value = TanSoList.value;
	          data['tanso'].isValidate = true;
	        }
	        break;
			
	    }
	  });
	  this.setState({ data, });

	}
	onPressDropdown = (name) => {
	  const { data,listTuyen, } = this.state;
	  if (name === 'quanhuyen' && !data.quanhuyen.list) {
	    return Alert.alert('Thông báo', 'Vui lòng chọn tỉnh thành trước');
	  }
	  else if (name === 'tuyenbh'&&listTuyen ) {
	    AppNavigation.showCommonDialog(
	      {renderContent: ()=>   
	        <ViewDialog 			
	          title={data[name].title} 
	          content={listTuyen} 
	          onPressItem ={(item)=>{
	            let tam=[];
	            listTuyen.map((x)=>{						
	              let t = x;
	              if(item.label==x.label)
	              {t.isselect=item.isselect;}		
	              tam.push(t);
	            });
	            this.setState({ data,listTuyen:tam, });
	            this.capnhattuyen(tam);
			  	}}
			  mutidata={true}
	        />,
	      });
	    // Notify.show(data[name].title, {
	    //   items: listTuyen,
	    //   onPressItem: (item) => {
	    //     let tam=[];
	    //     listTuyen.map((x)=>{						
	    //       let t = x;
	    //       if(item.label==x.label)
	    //         {t.isselect=item.isselect;}		
	    //       tam.push(t);
	    //     });
	    //     this.setState({ data,listTuyen:tam, });
	    //     this.capnhattuyen(tam);
				
	    //   },
	    //   mutidata:true,
	    // });
			
	  }
	  else{
	    AppNavigation.showCommonDialog(
	      {renderContent: ()=> 
	        <ViewDialog 			
	          title={data[name].title} 
	          content={data[name].list} 
	          onPressItem ={(item)=>{
	            data[name].text = item.label;
	            data[name].value = item.value;
	            data[name].isValidate = true;
		
	            if (name === 'tinhthanh') {
	              data.quanhuyen.list = null;
	              data.quanhuyen.isValidate = false;
	              data.quanhuyen.text = '';
		
	              this.props.dispatch(
	                getQuanHuyen({ tinhthanhId: item.value, }, (dt) => {
	                  data.quanhuyen.list = dt.map((item) => {
	                    item.label = item.TEN[0];
	                    item.value = item.PK_SEQ[0];
	                    return item;
	                  });
	                  this.setState({ data, });
	                }),
	              );
	            }
	            if (name === 'quanhuyen') {
	              data.xa.list = null;
	              data.xa.isValidate = false;
	              data.xa.text = '';

	              this.props.dispatch(
	                getphuongxa({ quanhuyenId: item.value, }, (dt) => {
	                  data.xa.list = dt.map((item) => {
	                    item.label = item.TEN[0];
	                    item.value = item.PK_SEQ[0];
	                    return item;
	                  });
	                  this.setState({ data, });
	                }),
	              );
	            }
				
	            else {
	              this.setState({ data, });
	            }
		



	            if(name === 'nvbh'||name === 'npp'){
	              this.getlisttuyen();
	            }
	          }}
	        />,
	      });
	
	  }
	};

	takePicture = (index) => {
	  const options = {
	    quality: 1.0,
	    maxWidth: 500,
	    maxHeight: 500,
	    storageOptions: {
	      skipBackup: true,
	    },
	  };

	  ImagePicker.launchCamera(options, (response) => {
	    console.log('Response = ', response);

	    if (response.didCancel) {
	      console.log('User cancelled photo picker');
	    } else if (response.error) {
	      console.log('ImagePicker Error: ', response.error);
	    } else if (response.customButton) {
	      console.log('User tapped custom button: ', response.customButton);
	    } else {
	      let source = { uri: response.uri, data: response.data, };

	      // You can also display the image using data:
	      // let source = { uri: 'data:image/jpeg;base64,' + response.data };
	      const { data, } = this.state;
	      data.chuphinh.source = source;
	      data.chuphinh.value = 1;
	      data.chuphinh.isValidate = true;
	      this.setState({ data, });
	    }
	  });
	};

	createCustomer = () => {
	  //public string TaoMoiKhachHang(string ten, string dienthoai, string diachi, string nppId, string ddkdId, string tinhthanh, string quanhuyen,
	  //	string xa, string ngdd, string msthue, string loaikh, string vtkh, string nhomkh, string nvgn,
	  //	 String tbhId, String tanso, String trangthai, string kenhbh, String lat, String lon, byte[] bytehinh, String chuphinh)


	  const { data, } = this.state;
	  const {  dispatch, } = this.props;
	  let payload = { tbhId: data.tuyenbh.value,   };

	  for (var key in data) {
	    if (!data[key].isValidate) {
	    
	      return Alert.alert('Thông báo', data[key].err);
	    }
	    if (key !== 'toaDo') {
	      payload[key] = data[key].value;
	    }
	  }
	  // Notify.show('Thông báo', 'ok');

	  payload.lat = data.toaDo.value.lat;
	  payload.lon = data.toaDo.value.long;

	  payload.bytehinh = data.chuphinh.source.data;

	  if (data.trangthai) {
	    payload.trangthai = '1';
	  } else {
	    payload.trangthai = '0';
	  }
	  AppNavigation.showCommonDialog({renderContent: ()=>< ViewLoadding />, });
	  dispatch(createCustomer(payload)).then((res) => {
	    AppNavigation.dismissOverlay('1');
	    if (res) {
	      if (res[0].RESULT == 1) {
	        Alert.alert('Thông báo', 'Tạo mới khách hàng thành công');
	      } else {
	        Alert.alert('Thông báo', res[0].MSG);
	      }
	    } else {
	      Alert.alert('Thông báo', res[0].MSG);
	    }
	  });
	};

	onPressCheckBox = () => {
	  const { data, } = this.state;
	  data.trangthai.value = !data.trangthai.value;
	  this.setState({ data, });
	};

	pressXDTD = () => {
	  const {location,}=this.state;
	  if(location){
	    this.props.navigator._push({
	        name: IDs.xdtdmap,  //.Register,
	        passProps: {
	        location:location,
	        setLocation:()=>{this.setLocation(location.latitude,location.longitude);},
	        },
	        options: {         
			  topBar: {
	            title: {
				  text:'Xác định tọa độ',
	            },
			    rightButtons: [
	            {
					  id: 'btntop',
					  icon: CommonIcons.done,           
	            },
				  ],
			  },
			  bottomTabs:{
	            visible: false,
	            ...Platform.select({ android: { drawBehind: true, }, }),
			  },
	        },
		   
		  });
	  }else{
	    Alert.alert('Thông báo',' Chưa lấy dc tạo độ , vui lòng đợi');
	  }
	      
	  

	};

	setLocation = (lat, long) => {
	  const { data, } = this.state;
	  data.toaDo.isValidate = true;
	  data.toaDo.value = { lat, long, };
	  this.setState({ data, });
	  Alert.alert('Thông báo', 'Xác định tọa độ thành công');
	};

	render() {
	  return (
	    <CreateView
	     
	      data={this.state.data}
	      onChangeText={this.onChangeText}
	      onChangeDate={this.onChangeDate}
	      onPressDropdown={this.onPressDropdown}
	      onPressCheckBox={this.onPressCheckBox}
	      pressXDTD={this.pressXDTD}
	      createCustomer={this.createCustomer}
	      takePicture={this.takePicture}
	    />
	  );
	}
}

export default connect(
  (state) => ({
    ddkdId: state.app.currentNPP.ddkd_fk,
    nppId: state.app.currentNPP.npp_fk,
    loaiNpp: state.app.currentNPP.loaiNPP,
    listTuyen: state.home.listTuyen,
    currentTuyen: state.home.currentTuyen,
    listBranch: state.app.listBranch,
  }),
  (dispatch) => ({ dispatch, }),
)(CreateContainer);
