import React from 'react';
import {
  View,
  StyleSheet,

  ScrollView,
} from 'react-native';

import {
  Input,
  DropdownInput,
  CheckBox,
  TakePictureAndCoordinates,
} from '../components';

export default class  CreateView extends React.Component {

  render(){
    const {
      data,
      onChangeText,
      onPressDropdown,
      onPressCheckBox,
      pressXDTD,
	  takePicture, }=this.props;

    return (
      <View style={styles.container}>
        <ScrollView style={styles.content}>
          <DropdownInput
				  label={data.npp.label}
				  text={data.npp.text}
				  onPress={() => onPressDropdown('npp')}
          />
          <DropdownInput
				  label={data.nvbh.label}
				  text={data.nvbh.text}
				  onPress={() => onPressDropdown('nvbh')}
          />
          <Input
				  label={data.ten.label}
				  value={data.ten.value}
				  onChangeText={(text) => onChangeText(text, 'ten')}
          />
          <Input
				  label={data.dienthoai.label}
				  value={data.dienthoai.value}
				  onChangeText={(text) => onChangeText(text, 'dienthoai')}
          />
          <Input
				  label={data.ngdd.label}
				  value={data.ngdd.value}
				  onChangeText={(text) => onChangeText(text, 'ngdd')}
          />
          <Input
				  label={data.diachi.label}
				  value={data.diachi.value}
				  onChangeText={(text) => onChangeText(text, 'diachi')}
          />
          <DropdownInput
				  label={data.vtkh.label}
				  text={data.vtkh.text}
				  onPress={() => onPressDropdown('vtkh')}
          />
          {/* <DropdownInput
				  label={data.hangkh.label}
				  text={data.hangkh.text}
				  onPress={() => onPressDropdown('hangkh')}
          /> */}
          <DropdownInput
				  label={data.tinhthanh.label}
				  text={data.tinhthanh.text}
				  onPress={() => onPressDropdown('tinhthanh')}
          />
          <DropdownInput
				  label={data.quanhuyen.label}
				  text={data.quanhuyen.text}
				  onPress={() => onPressDropdown('quanhuyen')}
          />
          <DropdownInput
					label={data.xa.label}
					text={data.xa.text}
					onPress={() => onPressDropdown('xa')}
				/>
          {/* <Input
				  label={data.xa.label}
				  value={data.xa.value}
				  onChangeText={(text) => onChangeText(text, 'xa')}
          /> */}
		
          {/* <DropdownInput
				  label={data.kenhbh.label}
				  text={data.kenhbh.text}
				  onPress={() => onPressDropdown('kenhbh')}
          /> */}
          <DropdownInput
				  label={data.tuyenbh.label}
				  text={data.tuyenbh.text}
				  onPress={() => onPressDropdown('tuyenbh')}
          />
          <DropdownInput
				  label={data.tanso.label}
				  text={data.tanso.text}
				  onPress={() => onPressDropdown('tanso')}
          />
		
          <DropdownInput
				  label={data.loaikh.label}
				  text={data.loaikh.text}
				  onPress={() => onPressDropdown('loaikh')}
          />
          {/* <DropdownInput
				  label={data.nhomkh.label}
				  text={data.nhomkh.text}
				  onPress={() => onPressDropdown('nhomkh')}
          /> */}
          {/* <DropdownInput
				  label={data.nvgn.label}
				  text={data.nvgn.text}
				  onPress={() => onPressDropdown('nvgn')}
          /> */}
          {/* <Input
				  label={data.msthue.label}
				  value={data.msthue.value}
				  onChangeText={(text) => onChangeText(text, 'msthue')}
          /> */}
          <TakePictureAndCoordinates
				  takePicture={takePicture}
				  pressXDTD={pressXDTD}
				  source={data.chuphinh.source}
          />
          <CheckBox
				  style={{margin:10,padding :15,}}
				  value={data.trangthai.value}
				  label={data.trangthai.label}
				  onPress={onPressCheckBox}
          />
			  </ScrollView>
      </View>
    );
  }

  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    paddingHorizontal:10,
    marginBottom:5,

  },
 
});
