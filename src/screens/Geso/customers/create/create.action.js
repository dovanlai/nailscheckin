import { post,modifyXML, } from '../../../../utils/netwworking';
var parseString = require('react-native-xml2js').parseString;

export function getNewInfo(payload, callback) {
  return () => { 
    return post('KhachHang_GetNewInfo', payload)
      .then(({ data, error, }) => {
        if (!error) {
          if (data) {
            callback(data);
          } else {
            callback(false);
          }
        } else {
          return false;
        }
      })
      .catch((e) => {
        return false;
      });
  };
}

export function getQuanHuyen(payload, callback) {
  return () => {
    return post('getQuanhuyenList', payload)
      .then(({ data, error, }) => {
        if (!error) {
          const xml = modifyXML(data);
          parseString(xml, function(err, dt) {
            if (dt) {
              callback(dt.DocumentElement.Table);
            } else {
              callback(false);
            }
          });
        } else {
         
          return false;
        }
      })
      .catch((e) => {
       
        return false;
      });
  };
}
export function getTuyen(param){
  return ()=>{
	  return post('getTuyenBanHangList',param )
	  .then((result)=>{
        if(!result.error){
		  return result.data;
        }else{
		  return false;
        }
	  }).catch((_)=>{
        return false;
	  });
  };
}
export function createCustomer(payload) {
  return () => {
    return post('TaoMoiKhachHang', payload)
      .then(({ data, error, }) => {

        if (!error) {
					
          return data;
        } else {
          return false;
        }
      })
      .catch((e) => {
      
        return false;
      });
  };
}
export function getphuongxa(payload, callback) {
	return dispatch => {
		return post('getphuongxa', payload)
			.then(({ data, error }) => {
				if (!error) {
					const xml = modifyXML(data);
					parseString(xml, function(err, dt) {
						console.log('get getphuongxa', err, dt);
						if (dt) {
							callback(dt.DocumentElement.Table);
						} else {
							callback(false);
						}
					});
				} else {
					console.log('get getphuongxa fail', data);
					return false;
				}
			})
			.catch(e => {
				console.log('get getphuongxa catch', e);
				return false;
			});
	};
}