export function testlai(payload) {
  return {
    type: 'test',
    payload,
  };
}
export function setnpp(payload) {
  return {
    type: 'setnpp',
    payload,
  };
}
export function setnnvbh(payload) {
  return {
    type: 'setnvbh',
    payload,
  };
}
export function resetdatasearch(payload) {
  return {
    type: 'resetdatasearch',
    payload,
  };
}
