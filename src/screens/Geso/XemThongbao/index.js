
import React from 'react';
import {
  View,
  Text,
  Platform,
  Keyboard,
} from 'react-native';
import {connect, } from 'react-redux';
import { getListAnswer, postAnswer, } from './actions';
import { SnackBar, } from '../../../utils';
import NotifyView from './NotifyView';
import { IDs, } from '../..';

class XemThongbao extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowContent: true,
      listAnswer: null,
      answer: '',
    };
  }

  componentDidMount() {
    this.getAnswer();
  }

	getAnswer = () => {
	  this.props.dispatch(
	    getListAnswer(
	      {
	        manhanvien: this.props.ddkdId,
	        thongbao_fk: this.props.data.PK_SEQ,
	      },
	      (dt) => {
	        if (dt) {
	          this.setState({ listAnswer: dt, });
	        } else {
	          this.setState({ listAnswer: [], });
	        }
	      }
	    )
	  );
	};

	saveAnswer = () => {
	  this.props
	    .dispatch(
	      postAnswer({
	        ddkdId: this.props.ddkdId,
	        thongbao_fk: this.props.data.PK_SEQ,
	        noidung: this.state.answer,
	      })
	    )
	    .then((res) => {
	      if (res) {
	        // Notify.show('Thông báo', 'Gửi câu trả lời thành công');
	        SnackBar.showSuccess( 'Gửi câu trả lời thành công');
	        this.getAnswer();

	        this.setState({ answer: '', });
	        Keyboard.dismiss();
	      } else {
	        //Notify.show('Thông báo', 'Gửi câu trả lời không thành công');
	        SnackBar.showError( 'Gửi câu trả lời không thành công');
	        Keyboard.dismiss();
	      }
	    });
	};

	changeContent = () => this.setState({ isShowContent: !this.state.isShowContent, });

	onChangeAnswer = (text) => this.setState({ answer: text, });

	viewDocument = (filename, linkUrl) => {
	//   this.props.dispatch(
	//     push({
	//       routeName: RouteKey.ViewDocument,
	//       params: { filename, linkUrl, },
	//     })
	//   );
	  this.props.navigator._push({
	    name: IDs.XemWebFile,  //.Register,
	    passProps: {
		  data:linkUrl,
	    },
	    options: {         
		  topBar: {
	        title: {
			  text:filename,
	        },
		  
		  },
		  bottomTabs:{
	        visible: false,
	        ...Platform.select({ android: { drawBehind: true, }, }),
		  },
	    },
	   
	  });

	};

	render(){
	  return (
		  <View style={{flex:1,}}>
	      <NotifyView 
	    
	        answer={this.state.answer}
	        saveAnswer={this.saveAnswer}
	        data={this.props.data}
	        onChangeAnswer={this.onChangeAnswer}
	        isShowContent={this.state.isShowContent}
	        changeContent={this.changeContent}
	        listAnswer={this.state.listAnswer}
	        getAnswer={this.getAnswer}
	        viewDocument={this.viewDocument}
	  />
		  </View>
	   
	  );
	}
}
export default connect(
  (state) => ({
    //ddkdId: state.app.currentNPP.ddkd_fk,
    nppId: state.app.currentNPP.npp_fk,
    ddkdId: state.app.listBranch.USERID,
  }),
  (dispatch) => ({ dispatch, })
)(XemThongbao);