import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Keyboard,
  Image,
  Dimensions,
} from 'react-native';
import { TextCmp, } from '../../../common-components';
import { Colors, } from '../../../theme';
import { HomeICon, } from './../../../assets/index';

const {height,}=Dimensions.get('window');


export default class NotifyView extends React.Component {

  render(){
    const {
      data,
      answer,

      saveAnswer,
      isShowContent,
      changeContent,
      listAnswer,
      viewDocument,
      onChangeAnswer,
    }=this.props;

    if (isShowContent) {
      return (
        <View style={styles.container}>
         
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => Keyboard.dismiss()}
            style={[styles.content,]}
          >
            <ScrollView>
              <View style={{height:height-130,}}>
            
              
                <TextCmp style={styles.textContent}>{data.TIEUDE}</TextCmp>
                {data.filename !== '0'&& 
              <TouchableOpacity
                style={styles.save}
                onPress={() =>
                  viewDocument(data.filename, data.linkUrl)
                }
              >
                <Image source={HomeICon.filepdfbox} style={{width:18,height:18,tintColor:Colors.HeaderColor,}}></Image>
                <TextCmp style={{color:Colors.HeaderColor,}}>Xem file</TextCmp>
              </TouchableOpacity>}
                <TextCmp>{data.NOIDUNG}</TextCmp>
                
            
            
              </View>
            </ScrollView>
          </TouchableOpacity>

          <TouchableOpacity style={styles.btnShow} onPress={changeContent}>
            <TextCmp>XEM CÁC CÂU TRẢ LỜI</TextCmp>
          </TouchableOpacity>
          
        
          <View style={[styles.row,]}>
            <TextInput
              underlineColorAndroid="transparent"
              style={[
                styles.btnShow,
                {
                  flex: 4,
                  textAlign: 'center',
                  paddingTop: 10,
                },
              ]}
              placeholder="Nhập câu trả lời"
              multiline={true}
              onChangeText={onChangeAnswer}
              value={answer}
            />
            <TouchableOpacity
              style={[styles.btnShow, { flex: 1, },]}
              onPress={saveAnswer}
            >
              {/* <TextCmp>Trả lời</TextCmp> */}
              <Image source={HomeICon.send} style={styles.imgsend}></Image>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    else
    {return (
      <View style={styles.container}>
            
        <View
          activeOpacity={1}
          onPress={() => Keyboard.dismiss()}
          style={[styles.content,]}
        >
          <ScrollView>
            {!listAnswer || listAnswer.length === 0 ? (
              <TextCmp style={styles.textContent}>Chưa có câu trả lời</TextCmp>
            ) : (
              listAnswer.map((item, index) => {
                return (
                  <View key={index}>
                    <Text>
                      {(typeof item.TRALOI[0] == 'string' && item.TRALOI[0]) ||
                                                    ''}
                    </Text>
                    <Text style={{ textAlign: 'right', }}>
                                                ( {item.TEN[0]} - {item.THOIDIEM[0]} )
                    </Text>
                  </View>
                );
              })
            )}
          </ScrollView>
        </View>
        <TouchableOpacity style={styles.btnShow} onPress={changeContent}>
          <TextCmp>XEM NỘI DUNG THÔNG BÁO</TextCmp>
        </TouchableOpacity>
        <View style={[styles.row,]}>
          <TextInput
            style={[
              styles.btnShow,
              {
                flex: 4,
                textAlign: 'center',
                paddingTop: 10,
              },
            ]}
            underlineColorAndroid="transparent"
            placeholder="Nhập câu trả lời"
            multiline={true}
            onChangeText={onChangeAnswer}
            value={answer}
          />
          <TouchableOpacity
            style={[styles.btnShow, { flex: 1, },]}
            onPress={saveAnswer}
          >
            {/* <TextCmp>Trả lời</TextCmp> */}
            <Image source={HomeICon.send} style={styles.imgsend}></Image>
          </TouchableOpacity>
        </View>
      </View>
    );}
  }
 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  
  },

  row: {
    flexDirection: 'row',
  },
  content: {
    margin:10,
    flex: 1,
    //backgroundColor: '#ccc',
  },
  textContent: { color: 'blue', marginVertical:10, },

  save:{  flexDirection: 'row',marginVertical:10,},
  btnShow: {
    borderTopWidth: 1,
    borderColor:Colors.line,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgsend:{width:18,height:18,tintColor:'gray',},
});
