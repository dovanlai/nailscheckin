/**
 * @author: thai.nguyen
 * @date: 2018-11-29 16:24:38
 *
 *
 */
import { connect, } from 'react-redux';

import MarketScreen from './MarketScreen';
const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(
  null,
  mapDispatchToProps
)(MarketScreen);
