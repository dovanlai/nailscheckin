import React, { Component, } from 'react';
import {
  Modal,
  Text,
  TouchableHighlight,
  View,
  Alert,
  Dimensions,
  ScrollView,
  StyleSheet,
} from 'react-native';
import { Colors, } from '../../../theme';
import { TextCmp, } from '../../../common-components';
const { width, height, } = Dimensions.get('window');
import moment from 'moment';
export default class ModalListSignIn extends Component {
  // state = {
  //   modalVisible: true,
  // };

  // setModalVisible(visible) {
  //   this.setState({modalVisible: visible});
  // }

  render() {
    const {
      setclosemodel,
      ishowWhydownloadApp,
      SignInList,
      paramcus,
    } = this.props;
    return (
      <View
        style={{
          alignContent: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          flex: 1,
          backgroundColor: 'red',
          // padding: 10,
        }}
      >
        <Modal
          animationType="slide"
          transparent={true}
          visible={ishowWhydownloadApp}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}
        >
          <View
            style={{
              width: '100%',
              //height: '100%',
              alignContent: 'center',
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}
          >
            <View
              style={{
                borderRadius: 0,
                backgroundColor: 'white',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <View
                style={{
                  marginHorizontal: 5,
                  borderColor: 'gray',
                  borderBottomWidth: 0.5,
                  backgroundColor: Colors.blue,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: 40,
                }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    justifyContent: 'space-between',
                    minHeight: 75,
                    alignItems: 'center',
                  }}
                >
                  <View style={styles.tttitleview}>
                    <TextCmp> Count</TextCmp>
                  </View>
                  <View style={styles.column}>
                    <TextCmp style={{ fontSize: 20, }}>Customer Name</TextCmp>
                  </View>

                  <View style={styles.column}>
                    <TextCmp style={{ fontSize: 20, }}>Sign in time</TextCmp>
                  </View>

                  <View style={styles.column}>
                    <TextCmp style={{ fontSize: 20, }}>Appointment</TextCmp>
                  </View>

                  <View style={styles.column}>
                    <TextCmp style={{ fontSize: 20, }}>Nails Technician</TextCmp>
                  </View>

                  <View style={styles.column}>
                    <TextCmp style={{ fontSize: 20, }}>Services</TextCmp>
                  </View>
                  <View style={styles.column}>
                    <TextCmp style={{ fontSize: 20, }}>Status</TextCmp>
                  </View>
                </View>
              </View>

              <ScrollView style={{ height: height - 150, width, }}>
                <View style={{}}>
                  {SignInList &&
                    SignInList.length > 0 &&
                    SignInList.map((item, index) => {
                      return (
                        <View
                          key={index+''}
                          style={{
                            marginHorizontal: 5,
                            borderColor: 'gray',
                            borderBottomWidth: 0.5,
                          }}
                        >
                          <View
                            style={{
                              flexDirection: 'row',
                              width: width,
                              justifyContent: 'space-between',
                              minHeight: 50,
                              alignItems: 'center',
                            }}
                          >
                            <View
                              style={{
                                minHeight: 75,
                                fontSize: 20,
                                width: '5%',
                                marginHorizontal: 5,
                                borderRightColor: 'gray',
                                borderRightWidth: 0.5,
                              }}
                            >
                              <TextCmp>{SignInList.length - index}</TextCmp>
                            </View>
                            <View style={styles.column}>
                              <TextCmp style={{ fontSize: 20, }}>
                                {item.name}{' '}
                              </TextCmp>
                              <TextCmp style={{ fontSize: 20, }}>
                                {' '}
                                ******{item.phone.slice(6, 10)}
                              </TextCmp>
                            </View>

                            <View style={styles.column}>
                              {/* <TextCmp style={{ fontSize: 20, }}>
                                Sign in time:
                              </TextCmp> */}
                              <TextCmp style={{ fontSize: 20, }}>
                                {item.appoimentTime !== null
                                  ? item.appoimentTime
                                  : ''}
                              </TextCmp>
                            </View>

                            <View style={styles.column}>
                              {/* <TextCmp style={{ fontSize: 20, }}>
                                Appointment:{' '}
                              </TextCmp> */}
                              <TextCmp style={{ fontSize: 20, }}>
                                {item.haveAppt}{' '}
                                {item.haveAppt === 'Yes'
                                  ? item.appoimentTime
                                  : ''}{' '}
                              </TextCmp>
                            </View>

                            <View style={styles.column}>
                              {/* <TextCmp style={{ fontSize: 20, }}>
                                Nails Technician
                              </TextCmp> */}
                              <TextCmp style={{ fontSize: 20, }}>
                                {item.staffName}
                              </TextCmp>
                            </View>

                            <View style={styles.column}>
                              {/* <TextCmp style={{ fontSize: 20, }}>
                                Services
                              </TextCmp> */}
                              <TextCmp style={{ fontSize: 20, }}>
                                {item.serviceName}
                              </TextCmp>
                            </View>
                            <View style={styles.column}>
                              <TextCmp style={{ fontSize: 20, }}>
                                {item.status}
                              </TextCmp>
                            </View>
                          </View>
                        </View>
                      );
                    })}
                </View>
              </ScrollView>

              <TouchableHighlight
                style={{
                  width: width / 4,
                  height: height / 14,
                  borderRadius: 5,
                  //backgroundColor:'#344566',
                  borderWidth: 1,
                  borderColor: Colors.mainColor,
                  alignItems: 'center',
                  justifyContent: 'center',
                  margin: 30,
                }}
                onPress={() => {
                  setclosemodel();
                }}
              >
                <Text style={{ fontSize: 20, }}>Close</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>

        <TouchableHighlight
          onPress={() => {
            this.setModalVisible(true);
          }}
        >
          <Text>Show Modal</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  column: {
    minHeight: 75,
    width: '15%',
    marginHorizontal: 5,
    borderRightColor: 'gray',
    borderRightWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tttitleview: {
    minHeight: 75,
    fontSize: 20,
    width: '10%',
    paddingHorizontal: 5,
    borderRightColor: 'gray',
    borderRightWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
