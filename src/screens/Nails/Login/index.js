
import React from 'react';
import {
  View,
  Alert,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  AlertIOS,
  Keyboard,
  AsyncStorage,
  Platform,
  Dimensions,
  Text,
  Image,
} from 'react-native';
import { connect, } from 'react-redux';
import { Colors, } from '../../../theme';
import LeftScreen from './Left';
import RightScreen from './Right';
import ViewRightScreen from './Right/ViewLoginTe';
import {
  AppCheckIn_TechnicianCheckIn, AppCheckIn_TechnicianCheckOut,
  AppCheckIn_EmployeeCheckIn, AppCheckIn_EmployeeCheckOut, AppCheckIn_CustomerSignIn,
  AppCheckIn_CustomerCheckExits, AppCheckIn_ConfirmAppoimentTime,
  AppCheckIn_CheckOwnerPIN, AppCheckIn_CustomerSignUp,
  AppCheckIn_SignInList, AppCheckIn_GetStaffs, AppCheckIn_GetServices,
  AppCheckIn_GetAppoimentTime, AppCheckIn_WhyDownLoadApp, AppCheckIn_StoreSetting,
} from './action';
import { Icon, } from 'react-native-elements';
import ViewRightInputScreen from './Right/Viewinputphone';
import ModalTime from './ModalTime';
import ModelwhyDowndapp from './ModelWhydowloadApp.js';
import ModalRefTechnician from './ModalRefTechnician';
import { IDs, } from '../..';
import CreateAccount from '../CreateAccount';
import ModalCreateStoreCdoe from './ModalCreateStoreCdoe';
import Icons from '../../../assets/Icons';
import { CommonIcons, } from '../../../assets';
import ModalListSignIn from './ModalListSignIn';
import moment from 'moment';
import { Logg, } from '../../../utils';
import ModalListServices from './ModalListServices';
import Checkupdate from '../../Geso/login/checkupdate';
import ModalQuickMenu from './ModalQuickMenu';
const { width, } = Dimensions.get('window');

class mainScene extends React.Component {
  constructor() {
    super();
    this.state = {
      indexbtn: 0,
      storeCode: '',//'MAX12898' 4179
      PINCode: '',
      loadding: false,
      paramcus: {
        phone: 'Phone nummer',
        firstName: '',
        lastName: '',
        birthday: '',
      },
      isshowinputnumber: false,
      istime: { appoimentId: '', appoimentTime: '', },
      modalVisible: false,
      Datatime: null,//JSON.parse('[{\"appoimentId\":100078,\"appoimentTime\":\"11:15\"},{\"appoimentId\":100077,\"appoimentTime\":\"1:00P\"}]')
      ishowWhydownloadApp: false,
      textwhydownapp: '',
      showlistTechnician: false,
      showCreateAccount: false,
      showEnterStoreCode: false,
      showlistsign: false,
      modalVisibleStaffsList: false,
      HaveAnAppoiment: false,
      xoaparmtaokh: true,
      checkin_setting:''

    };
  }
  
  componentDidMount() {
////

    AsyncStorage.getAllKeys((err, keys) => {
      AsyncStorage.multiGet(keys, (err, stores) => {
        stores.map((result, i, store) => {
          // get at each store's key/value so you can work with it
          let key = store[i][0];
          let val = store[i][1];
          if (key === 'storeCode' && val.length > 0) {
      
            this.getsetting(val)
            this.setState({ storeCode: val, });
            this.AppCheckIn_SignInList({ storeCode: val, });
            this.AppCheckIn_GetStaffs({ storeCode: val, });
            this.AppCheckIn_GetServices({ storeCode: val, });
            this.AppCheckIn_WhyDownLoadApp({ storeCode: val, });
           
          }
          if (key === 'PINCode' && val.length > 0) {
            this.setState({ PINCode: val, });
          }
          if (key === 'Logo' && val.length > 0) {
            this.setState({ Logo: val, });
          }
          if (key === 'WelcomeText' && val.length > 0) {
            this.setState({ WelcomeText: val, });
          }
        });
      });
    });




  }
  keypress(x) {
    if (x.id != 10 && x.id != 12) {
      let a = this.state.PINCode + x.ten;
      this.setState({ PINCode: a, });
    }
    else {
      if (x.id == 10) {
        if (this.state.indexbtn == 1) { this.TechnicianCheckIn(); }
        else {
          this.AppCheckIn_EmployeeCheckIn();
        }

      } else {
        if (this.state.indexbtn == 1) { this.TechnicianCheckOut(); }
        else {
          this.AppCheckIn_EmployeeCheckOut();
        }
      }

    }

  }
  keypress2(x) {
    if (x.id != 10 && x.id != 12) {
      let a = this.state.paramcus.phone + x.ten;
      // this.setState({PINCode:a})

      this.handleChangeText(a, 'phone');
    }
    else {
      if (x.id == 10) {
        this.setState({ isshowinputnumber: false, });
        this.AppCheckIn_CustomerCheckExits();

      } else {
        this.handleChangeText('', 'phone');
      }

    }

  }

  goCreateAc() {
    // alert('xxx')
    this.setState({ showCreateAccount: true, xoaparmtaokh: true, });

  }
  AppCheckIn_CustomerCheckExits() {
    let params = {
      phone: this.state.paramcus.phone,
      storeCode: this.state.storeCode,
    };
    this.setState({ loadding: true, });
    this.props.dispatch(AppCheckIn_CustomerCheckExits(params)).then((result) => {
      //alert(JSON.stringify(result))
      if (result.dataArray && result.dataArray[1].ErrorMessege) {

        if (result.dataArray[1].ErrorMessege == 'Customer not exit') {
          Alert.alert('Customer not exit', 'You are new to us, to better serve you please take a moment to fill out the form',
            [
              // {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              { text: 'OK', onPress: () => { this.goCreateAc(); }, },
            ],
            { cancelable: false, },

          );


        }
        else { Alert.alert('', result.dataArray[1].ErrorMessege); }
      } else {
        if (result.dataArray && result.dataArray[0].Status == 'Success') {
          const input = result.dataArray[2].Data;

          const [firstName, lastName, unit, city, state, zip,] = input.split(' ');
          this.setState((state) => ({
            paramcus: {
              ...state.paramcus,
              firstName,
              lastName,
            },
          }));
          this.AppCheckIn_GetAppoimentTime();
        } else { Alert.alert('Alert', JSON.stringify(result)); }

      }
      this.setState({ loadding: false, });
      this.AppCheckIn_SignInList({ storeCode: this.state.storeCode, });

    });
  }
  TechnicianCheckIn() {
    let params = {
      PINCode: this.state.PINCode,
      storeCode: this.state.storeCode,
    };
    this.setState({ loadding: true, });
    this.props.dispatch(AppCheckIn_TechnicianCheckIn(params)).then((result) => {

      if (result.dataArray && result.dataArray[1].ErrorMessege) {
        if (result.dataArray[0].Status == 'Success') { this.xoa(); }
        Alert.alert('', result.dataArray[1].ErrorMessege);
      } else {

        if (result.dataArray && result.dataArray[0].Status) {
          Alert.alert('', result.dataArray[0].Status);

        }
        else {
          Alert.alert('', JSON.stringify(result));

        }

      }
      this.setState({ loadding: false, });
    });
  }
  TechnicianCheckOut() {
    let params = {
      PINCode: this.state.PINCode,
      storeCode: this.state.storeCode,
    };
    this.setState({ loadding: true, });
    this.props.dispatch(AppCheckIn_TechnicianCheckOut(params)).then((result) => {

      if (result.dataArray && result.dataArray[1].ErrorMessege) {
        if (result.dataArray[0].Status == 'Success') { this.xoa(); }
        Alert.alert('', result.dataArray[1].ErrorMessege);
      } else {
        if (result.dataArray && result.dataArray[0].Status) {
          this.xoa();
          Alert.alert('', result.dataArray[0].Status);
        } else {
          Alert.alert('', JSON.stringify(result));

        }

      }
      this.setState({ loadding: false, });
    });
  }
  AppCheckIn_EmployeeCheckIn() {
    let params = {
      PINCode: this.state.PINCode,
      storeCode: this.state.storeCode,
    };
    this.setState({ loadding: true, });
    this.props.dispatch(AppCheckIn_EmployeeCheckIn(params)).then((result) => {

      if (result.dataArray && result.dataArray[1].ErrorMessege) {
        if (result.dataArray[0].Status == 'Success') { this.xoa(); }
        Alert.alert('', result.dataArray[1].ErrorMessege);
      } else {
        if (result.dataArray && result.dataArray[0].Status) {
          Alert.alert('', result.dataArray[0].Status);

        }
        else { Alert.alert('', JSON.stringify(result)); }

      }
      this.setState({ loadding: false, });
    });
  }
  AppCheckIn_EmployeeCheckOut() {
    let params = {
      PINCode: this.state.PINCode,
      storeCode: this.state.storeCode,
    };
    this.setState({ loadding: true, });
    this.props.dispatch(AppCheckIn_EmployeeCheckOut(params)).then((result) => {

      if (result.dataArray && result.dataArray[1].ErrorMessege) {
        if (result.dataArray[0].Status == 'Success') { this.xoa(); }
        Alert.alert('', result.dataArray[1].ErrorMessege);
      } else {
        if (result.dataArray && result.dataArray[0].Status) {
          Alert.alert('', result.dataArray[0].Status);
          this.xoa();
        } else { Alert.alert('', JSON.stringify(result)); }

      }
      this.setState({ loadding: false, });
    });
  }
  xoa = () => {
    this.setState({ PINCode: '', });
  }
  handleChangeText(text, key) {
    this.setState((state) => ({
      paramcus: {
        ...state.paramcus,
        [key]: text,
      },
    }));
  }
  AppCheckIn_CheckOwnerPIN(PINCode, storeCode) {
    let params = {
      PINCode: PINCode,
      storeCode: storeCode,
    };

    this.setState({ loadding: true, showEnterStoreCode: false, });
    this.props.dispatch(AppCheckIn_CheckOwnerPIN(params)).then((result) => {
      Logg.info('AppCheckIn_CheckOwnerPIN' + JSON.stringify(result));
      //alert(JSON.stringify(result))
      if (result.dataArray && result.dataArray[1].ErrorMessege) {

        Alert.alert('', result.dataArray[1].ErrorMessege);
      } else {
        if (result.dataArray && result.dataArray[0].Status) {
          if (result.dataArray[0].Status == 'Success') {

            let multi_set_pairs = [
              ['storeCode', '' + storeCode,],
              ['PINCode', '' + PINCode,],
              ['Logo', '' + result.dataArray[2].Logo,],
              ['WelcomeText', '' + result.dataArray[3].WelcomeText,],
            ];
            AsyncStorage.multiSet(multi_set_pairs, (err) => {
            });

            let Logo = null, WelcomeText = null;
            if (result.dataArray[2].Logo) {
              Logo = result.dataArray[2].Logo;
            }
            if (result.dataArray[3].WelcomeText) {
              WelcomeText = result.dataArray[3].WelcomeText;
            }
            this.setState({ storeCode: storeCode, Logo, WelcomeText, PINCode: PINCode, });


            this.AppCheckIn_GetStaffs({ storeCode: storeCode, });
            this.AppCheckIn_GetServices({ storeCode: storeCode, });
            this.AppCheckIn_WhyDownLoadApp({ storeCode: storeCode, });
          }


          Alert.alert('', result.dataArray[0].Status);
        } else {
          Alert.alert('', JSON.stringify(result));

        }

      }
      this.setState({ loadding: false, });
    });
  }
  createStoreid = () => {
    this.setState({ showEnterStoreCode: true, });
    // AlertIOS.prompt(
    //   'Enter Store Code',
    //   '',
    //   [
    //     {
    //       text: 'Cancel',
    //       onPress: () => console.log('Cancel Pressed'),
    //       style: 'cancel',
    //     },
    //     {
    //       text: 'Save',
    //       onPress: (password) =>{ 

    //         this.AppCheckIn_CheckOwnerPIN(password)
    //       },
    //     },
    //   ],
    //   'plain-text',
    // );
    //   AlertIOS.prompt('Enter Store Code', null, (text) =>
    //   this.setState({storeCode:text})
    // );
    // alert(this.state.storeCode)
  }
  AppCheckIn_WhyDownLoadApp(params) {
    params.date = moment(new Date()).format('YYYY-MM-DD'),
    this.props.dispatch(AppCheckIn_WhyDownLoadApp(params)).then((result) => {
      //alert(JSON.stringify(result));
      // Logg.info('AppCheckIn_WhyDownLoadApp lai xxxx '+JSON.stringify(result));
      // this.setState({ServicesList:result,});

      // if(result.dataArray && result.dataArray[1].ErrorMessege){
      //   Alert.alert('', result.dataArray[1].ErrorMessege);

      // }else{
      //   if(result.dataArray && result.dataArray[0].Status){
      //     // Alert.alert('', result.dataArray[0].Status )

      //   }
      //   else
      //   {Alert.alert('', JSON.stringify(result)  );}

      // }
      // this.setState({loadding:false,});
    });
  }
  AppCheckIn_GetServices(params) {
    params.date = moment(new Date()).format('YYYY-MM-DD'),
    this.props.dispatch(AppCheckIn_GetServices(params)).then((result) => {
      //alert(JSON.stringify(result))
      Logg.info('Services lai xxxx ' + JSON.stringify(result));
      let listserveceSearch = [{ 'catname': 'All Services', 'id': -1, 'name': '', },];
      if (result) {

        result.map((item) => {
          if (item.id == '-1') {
            listserveceSearch.push(item);
          }
        });

      }

      this.setState({ ServicesList: result, listserveceSearch: listserveceSearch, });

      if (result.dataArray && result.dataArray[1].ErrorMessege) {
        Alert.alert('', result.dataArray[1].ErrorMessege);

      } else {
        if (result.dataArray && result.dataArray[0].Status) {
          // Alert.alert('', result.dataArray[0].Status )

        }
        else { Alert.alert('', JSON.stringify(result)); }

      }
      this.setState({ loadding: false, });
    });
  }
  AppCheckIn_GetStaffs(params) {
    params.date = moment(new Date()).format('YYYY-MM-DD'),
    this.props.dispatch(AppCheckIn_GetStaffs(params)).then((result) => {
      //alert(JSON.stringify(result))
      Logg.info('Staffs lai xxxx ' + JSON.stringify(result));

      this.setState({ StaffsList: result, });

      if (result.dataArray && result.dataArray[1].ErrorMessege) {
        Alert.alert('', result.dataArray[1].ErrorMessege);

      } else {
        if (result.dataArray && result.dataArray[0].Status) {
          // Alert.alert('', result.dataArray[0].Status )

        }
        else { Alert.alert('', JSON.stringify(result)); }

      }
      this.setState({ loadding: false, });
    });
  }
  AppCheckIn_SignInList(params) {

    params.date = moment(new Date()).format('YYYY-MM-DD'),
    //this.xoa()
    // this.setState({loadding:true})
    this.props.dispatch(AppCheckIn_SignInList(params)).then((result) => {
      Logg.info('lai xxxx AppCheckIn_SignInList ' + JSON.stringify(result));
      // if(JSON.stringify(result)!='[]')
      this.setState({ SignInList: result, });

      if (result.dataArray && result.dataArray[1].ErrorMessege) {
        Alert.alert('', result.dataArray[1].ErrorMessege);

      } else {
        if (result.dataArray && result.dataArray[0].Status) {
          // Alert.alert('', result.dataArray[0].Status )

        }
        else { Alert.alert('', JSON.stringify(result)); }

      }
      this.setState({ loadding: false, });
    });

  }
  sigin = (x) => {

    let params = this.state.paramcus;
    params.storeCode = this.state.storeCode;
    params.haveAppt = this.state.HaveAnAppoiment ? '1' : '0';
    if (x === 'khongxoadata') {
      params.haveAppt = '1';
    }
    else {
      if (!this.state.Staffs && params.haveAppt == 0) { return Alert.alert('', 'Please Select Prefer Nails Technician'); }
      if (!this.state.Services && params.haveAppt == 0) { return Alert.alert('', 'Please Select Prefer Services'); }
    }

    if (!this.state.HaveAnAppoiment && this.state.Staffs && this.state.Services) {

      let sv = '';
      this.state.Services.map((item) => {
        sv += item.id + '_';
      });
      let idStaffs = '';
      this.state.Staffs.map((item) => {
        idStaffs += item.id + '_';
      });
      params.staffId = idStaffs;
      params.serviceId = sv;
    }
    else {
      params.staffId = '';
      params.serviceId = '';
    }
    //[{"Status":"Success"},{"ErrorMessege":""},{"Data":"[]"}]

    Logg.info('LAi xxxx sigin parma' + JSON.stringify(params));
    this.setState({ loadding: true, });
    this.props.dispatch(AppCheckIn_CustomerSignIn(params)).then((result) => {
      this.setState({ loadding: false, });
      if (result) {

        Logg.info('LAi xxxx sigin result ' + JSON.stringify(result));
        if (result.dataArray[0].Status === 'Success' && result.dataArray[2].Data.length === 0) {
          //this.setState({CheckinId:result.dataArray[3].CheckinId,});
        } else {
          if (result.dataArray[2].Data) {
            this.setState({
              CheckinId: result.dataArray[3].CheckinId,
              //Datatime:JSON.parse(result.dataArray[2].Data),
              //istime: JSON.parse(result.dataArray[2].Data)[0],
            }
            );
          }
          if (result.dataArray[0].Status !== 'Success') {
            //thất bại  
            if (x == 'khongxoadata') Alert.alert('', result.dataArray[1].ErrorMessege);
          }
        }
        if (x !== 'khongxoadata' && result.dataArray[0].Status === 'Success') {
          let paramcus = {
            phone: 'Phone nummer',
            firstName: '',
            lastName: '',
            birthday: '',
          };
          let istime = { appoimentId: '', appoimentTime: '', };
          this.setState({
            paramcus,
            istime, HaveAnAppoiment:
              false, Staffs: null, Services: null,
          });
          Alert.alert('', result.dataArray[1].ErrorMessege);
        }
        this.AppCheckIn_SignInList({ storeCode: this.state.storeCode, });
      }
      else { Alert.alert('', 'ErrorMessege'); }
    });

  }

  AppCheckIn_ConfirmAppoimentTime(index) {

    const { Datatime, paramcus, istime, storeCode, } = this.state;
    if (Datatime && Datatime.length === 0 || !istime) { return this.setState({ modalVisible: false, }); }
    this.setState({ istime: Datatime ? Datatime[index] : istime, modalVisible: false, });
    let prams = {
      phone: paramcus.phone,
      appoimentId: istime.appoimentId,
      storeCode: storeCode,
      checkinId: this.state.checkinId,
    };
    this.setState({ loadding: true, });
    this.props.dispatch(AppCheckIn_ConfirmAppoimentTime(prams)).then((result) => {

      if (result.dataArray && result.dataArray[1].ErrorMessege) {
        if (result.dataArray[0].Status == 'Success') {
          Alert.alert(result.dataArray[1].ErrorMessege, 'Please download Customer App to keep track your reward points.');
          let paramcus = {
            phone: 'Phone nummer',
            firstName: '',
            lastName: '',
            birthday: '',
          };
          let istime = { appoimentId: '', appoimentTime: '', };
          this.setState({
            paramcus, istime, checkinId: '', Datatime: null,
          });
        }
        else {
          Alert.alert('', result.dataArray[1].ErrorMessege);
        }
        //if(result.dataArray[2].Data)
        //this.setState({Datatime:JSON.parse(result.dataArray[2].Data),istime: JSON.parse(result.dataArray[2].Data)[0]})
      } else {
        if (result.dataArray && result.dataArray[0].Status) {
          Alert.alert('', result.dataArray[0].Status);

        }
        else { Alert.alert('', JSON.stringify(result)); }

      }
      this.setState({ loadding: false, });
    });
  }
  AppCheckIn_CustomerSignUp(params, loai) {

    params.isCheckin = loai === '2' ? '1' : '0';
    params.storeCode = this.state.storeCode;

    if (params.phone == '') { return Alert.alert('', 'Please Enter Phone'); }
    else if (params.firstName == '') { return Alert.alert('', 'Please Enter First Name'); }
    else if (params.lastName == '') { return Alert.alert('', 'Please Enter Last Name'); }
    else {
      this.setState({ showCreateAccount: false, });
      this.setState({ loadding: true, });
      this.props.dispatch(AppCheckIn_CustomerSignUp(params)).then((result) => {
        Logg.info('AppCheckIn_CustomerSignUp result = ' + JSON.stringify(result));
        if (result.dataArray && result.dataArray[1].ErrorMessege) {
          Alert.alert('', result.dataArray[1].ErrorMessege);

        } else {
          if (result.dataArray && result.dataArray[0].Status) {
            Alert.alert('', result.dataArray[0].Status);

          }
          else { Alert.alert('', JSON.stringify(result)); }

        }
        this.setState({ loadding: false, });
        if (result.dataArray[0].Status == 'Success') {
          this.AppCheckIn_SignInList({ storeCode: this.state.storeCode, });
          this.setState({ xoaparmtaokh: false, });
        }
      });
    }

  }
  AppCheckIn_GetAppoimentTime() {
    const { paramcus, storeCode, } = this.state;


    let params = {
      phone: paramcus.phone,
      storeCode: storeCode,
    };
    this.props.dispatch(AppCheckIn_GetAppoimentTime(params)).then((result) => {
      Logg.info('AppCheckIn_GetAppoimentTime result = ' + JSON.stringify(result));
      // if(result.dataArray[2].Data){
      this.setState({
        Datatime: result,
        istime: result[0],
      });
      // }
    }
    );


  }
  getsetting(storeCode){

    let params = { storeCode };
    this.props.dispatch(AppCheckIn_StoreSetting(params)).then((res) => {
   
     if (res) {
       this.setState({ checkin_setting: res.dataArray[2].checkin_setting, });
      // alert(JSON.stringify(res.dataArray[2].checkin_setting));
     }
     else {
       //Alert.alert('','')
       showError('error api');
     }
   });

  }
  setindexbtn(x) {
    this.setState({ indexbtn: x, });
  }
  render() {
    const { isshowinputnumber, istime, paramcus, Datatime,checkin_setting } = this.state;
    return (
      <TouchableOpacity activeOpacity={1} style={styles.containt} onPress={Keyboard.dismiss} accessible={false}>

        <Checkupdate />

        <View style={styles.lr}>
          <LeftScreen
            ishowWhydownloadApp={() => { this.setState({ ishowWhydownloadApp: true, }); }}
            setindexbtn={(x) => this.setindexbtn(x)} indexbtn={this.state.indexbtn}
            Logo={this.state.Logo}
          >

          </LeftScreen>
        </View>
        <View style={styles.lr}>
          {
            this.state.indexbtn == 0 && !isshowinputnumber &&
            <RightScreen indexbtn={this.state.indexbtn}
              handleChangeText={(text, type) => this.handleChangeText(text, type)}
              paramcus={this.state.paramcus}
              sigin={() => this.sigin()}
              showinput={() => {
                this.setState({ isshowinputnumber: true, });
                if (paramcus.phone == 'Phone nummer') { this.handleChangeText('', 'phone'); }
              }
              }
              istime={istime}
              showtimeSelect={() => { this.setState({ modalVisible: Datatime ? true : false, }); }}
              ishowWhydownloadApp={() => { this.setState({ ishowWhydownloadApp: true, }); }}
              showlistTechnician={() => { this.setState({ showlistTechnician: true, }); }}
              showServicesList={() => { this.setState({ showServicesList: true, }); }}
              SignInList={this.state.SignInList}
              createacount={() => this.goCreateAc()}
              Staffs={this.state.Staffs}
              Services={this.state.Services}
              HaveAnAppoiment={(x) => {
                this.setState({ HaveAnAppoiment: x, modalVisible: x, });
                //  if(x==true){
                //    this.sigin('khongxoadata');
                //  }
                this.AppCheckIn_GetAppoimentTime();

              }}
              WelcomeText={this.state.WelcomeText}
            ></RightScreen>
          }
          {
            this.state.indexbtn == 0 && isshowinputnumber &&
            <ViewRightInputScreen
              xoa={() => this.handleChangeText('', 'phone')} phone={this.state.paramcus.phone} keypress={(x) => this.keypress2(x)}
              indexbtn={this.state.indexbtn}></ViewRightInputScreen>
          }
          {
            this.state.indexbtn == 1 && <ViewRightScreen xoa={this.xoa} phone={this.state.PINCode} keypress={(x) => this.keypress(x)} indexbtn={this.state.indexbtn}></ViewRightScreen>
          }
          {
            this.state.indexbtn == 2 && <ViewRightScreen xoa={this.xoa} phone={this.state.PINCode} keypress={(x) => this.keypress(x)} indexbtn={this.state.indexbtn}></ViewRightScreen>
          }


        </View>
        {
          this.state.loadding && <ActivityIndicator size='large' color='#F9CF62' style={{ position: 'absolute', bottom: 100, width: '100%', }}></ActivityIndicator>
        }
        <TouchableOpacity style={{ position: 'absolute', right: 50, top: 50, }} onPress={() => this.createStoreid()}>
          <Icon
            reverse
            name='ios-cog'
            type='ionicon'
            color='#517fa4'
          />
        </TouchableOpacity>

        {this.state.modalVisible && <ModalTime
          modalVisible={this.state.modalVisible}
          setModalVisible={() => { this.setState({ modalVisible: false, }); }}
          Datatime={Datatime}
          settime={(index) => {
            this.AppCheckIn_ConfirmAppoimentTime(index);
          }}
        ></ModalTime>}

        {this.state.showlistTechnician && <ModalRefTechnician
          modalVisible={this.state.showlistTechnician}
          setModalVisible={() => { this.setState({ showlistTechnician: false, }); }}
          StaffsList={this.state.StaffsList}
          setStaffs={(listadd) => {
            this.setState({ Staffs: this.state.StaffsList ? listadd : null, showlistTechnician: false, });

          }}
        ></ModalRefTechnician>}

        {this.state.showServicesList && <ModalQuickMenu
          modalVisible={this.state.showServicesList}
          setModalVisible={() => { this.setState({ showServicesList: false, }); }}
          StaffsList={this.state.ServicesList}
          setStaffs={(item) => {
            this.setState({ Services: item, showServicesList: false, });

          }}
          listserveceSearch={this.state.listserveceSearch}
        ></ModalQuickMenu>}


        <ModelwhyDowndapp textwhydownapp={this.state.textwhydownapp}
          ishowWhydownloadApp={this.state.ishowWhydownloadApp} setclosemodel={() => { this.setState({ ishowWhydownloadApp: false, }); }}></ModelwhyDowndapp>

        {this.state.xoaparmtaokh &&
          <CreateAccount PINCode={this.state.paramcus.phone} AppCheckIn_CustomerSignUp={(pr, loai) => this.AppCheckIn_CustomerSignUp(pr, loai)} ishowWhydownloadApp={this.state.showCreateAccount} setclosemodel={() => { this.setState({ showCreateAccount: false, }); }}></CreateAccount>
        }

        <ModalCreateStoreCdoe
          PINCode={this.state.PINCode}
          storeCode={this.state.storeCode}
          AppCheckIn_CheckOwnerPIN={(PINCode, storeCode) => this.AppCheckIn_CheckOwnerPIN(PINCode, storeCode)} 
          ishowWhydownloadApp={this.state.showEnterStoreCode} 
          checkin_setting={  this.state.checkin_setting}
          setclosemodel={() => { this.setState({ showEnterStoreCode: false, }); }}
        >

        </ModalCreateStoreCdoe>
        {this.state.SignInList && <TouchableOpacity onPress={() => { this.setState({ showlistsign: !this.state.showlistsign, }); }} style={{ position: 'absolute', bottom: 5, left: width / 2 - 50, justifyContent: 'center', alignItems: 'center', }} >
          <Text style={{ color: 'white', fontSize: 20, }}>Sign List</Text>
          <Image style={{ width: 23, height: 23, }} source={CommonIcons.filter}></Image>
        </TouchableOpacity>}

        {this.state.showlistsign &&

          <ModalListSignIn
            SignInList={this.state.SignInList}
            setclosemodel={() => { this.setState({ showlistsign: false, }); }}
            paramcus={this.state.paramcus}

          />


        }
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  containt: {
    flex: 1,
    backgroundColor: Colors.backgroundL1,
    flexDirection: 'row',

  },
  lr: {
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',

  },
});

export default connect(
  (state) => ({

  }),
  (dispatch) => ({ dispatch, }),
)(mainScene);