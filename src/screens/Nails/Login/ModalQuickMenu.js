import React, { Component, } from 'react';
import {
  Modal,
  Text,
  TouchableOpacity,
  View,
  Alert,
  Dimensions,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';

import { TextCmp, } from '../../../common-components';
import { colors, } from 'react-native-elements';
import { Colors, } from '../../../theme';
import { CommonIcons, } from '../../../assets';

import { normalize, } from '../../../utils/FontSize';
const { width, height, } = Dimensions.get('window');
export default class ModalQuickMenu extends Component {
  state = {
    index: 0,
    listadd: [],
    catsearch: 'All Services',
    listAddPedicure: [],
    bgColor: [
      '#00a1f1',
      '#7cbb00',

    ],
    bgColor2: [
      '#ffbb00',
      '#f65314',
    ],
  };

  // setModalVisible(visible) {
  //   this.setState({modalVisible: visible});
  // }
  componentDidMount(){
    //alert(JSON.stringify(this.props.QuickMenu))
  }


  pressitem(index, item) {
    let listadd = this.state.listadd;
    let listAddPedicure = this.state.listAddPedicure;

    //console.log('listadd' + JSON.stringify(this.state.listadd));
    //console.log('listAddPedicure' + JSON.stringify(this.state.listAddPedicure));

    if (item.catname !== 'Pedicure') {
      if (listadd.findIndex((obj) => obj.name === item.name) == -1) {
        // alert(listadd.findIndex((obj) => obj.name === item.name));
        listadd.push(item);
      } else {
        listadd = listadd.filter((_item) => _item.name !== item.name);
      }
      this.setState({ index, item, listadd, listAddPedicure, });
      this.props.setStaffs(listadd);
    } else {
      if (listAddPedicure.findIndex((obj) => obj.name === item.name) == -1) {
        // alert(listadd.findIndex((obj) => obj.name === item.name));
        if (item.catname == 'Pedicure') listAddPedicure.push(item);
      } else {
        listAddPedicure = listAddPedicure.filter(
          (_item) => _item.name !== item.name
        );
      }
      this.setState({ index, item, listadd, listAddPedicure, });
      this.props.setStaffs(listAddPedicure);
    }
  }
  render() {
    const {
      modalVisible,
      setModalVisible,
      StaffsList,
      setStaffs,
      listserveceSearch,
      showok,
      QuickMenu
    } = this.props;

    let datalist = StaffsList;
    if (datalist && this.state.catsearch !== 'All Services') {
      datalist = StaffsList.filter(
        (cus) =>
          (cus.catname + '')
            .toLowerCase()
            .indexOf(this.state.catsearch.toLowerCase()) > -1
      );
    }

    return (
      <View
        style={{
          flex: 1,
          borderRadius: 10,
          backgroundColor: '#fff',
          alignItems: 'center',
          position: 'absolute',
        }}
      >
        <View
          style={{
            height: 60,
            backgroundColor: Colors.mainColor,
            width,
            //alignItems: 'center',
            justifyContent: 'center',
            paddingBottom: 5,
          }}
        >
          <TextCmp style={[styles.txtinbntac, { color: 'white', },]}>
            Please Select Services
          </TextCmp>
        </View>

        <ScrollView style={{ height: height - 180, marginBottom: 130,  }} >

          <View style={styles.fill}>
            {QuickMenu &&
              QuickMenu.map((item, index) => {
                // if(index%2==0)
                //   var xcolor = this.state.bgColor[Math.floor(Math.random()*this.state.bgColor.length)];
                //   else{
                //     var xcolor = this.state.bgColor2[Math.floor(Math.random()*this.state.bgColor.length)];
                 // }
                return (
                  <TouchableOpacity
                    key={index + ''}
                    onPress={() => this.pressitem(index, item)}
                    style={[
                      styles.item,
                      {
                        backgroundColor:item.color,
                        borderColor:
                          item.catname === 'Pedicure'
                            ? this.state.listAddPedicure.findIndex(
                              (obj) => obj.name === item.name
                            ) != -1
                              ? 'red'
                              : 'black'
                            : this.state.listadd.findIndex(
                              (obj) => obj.name === item.name
                            ) != -1
                              ? 'red'
                              : 'black',
                      },
                    ]}
                  >
                    <View
                      style={{
                        flexDirection: 'row',
                        //flex: 1,
                        alignItems: 'center',
                        justifyContent:'center'
                      }}
                    >
                      <TextCmp
                        style={styles.txttimeslac}
                        textAlign='center'
                      >
                        {item.name.toUpperCase()}
                      </TextCmp>
                    </View>
                    {/* <TextCmp style={styles.txttimesl}>
                      ${item.price}
                    </TextCmp> */}
                  </TouchableOpacity>
                );
              })}
          </View>
        </ScrollView>
        {showok &&
          <TouchableOpacity onPress={() => setModalVisible(false)}
            style={{ position: 'absolute', bottom: 10, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center', width: 200, height: 60, borderRadius: 10, }}>
            <TextCmp style={{ fontSize: 25, color: 'white', }}>ok</TextCmp>
          </TouchableOpacity>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtinbntac: {
    fontSize: 25,
    textAlign: 'center',
    width: '100%',
    textAlignVertical: 'center',
    marginBottom: 20,
    marginTop: 30,
  },
  viewbtnbottom: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    width: '100%',
    height: 100,
    position: 'absolute',
    bottom: 0,
    paddingHorizontal: 50,
  },
  txtbtnbottom: {
    fontSize: 20,
    textAlign: 'center',
    width: '100%',
    textAlignVertical: 'center',
    marginHorizontal: 20,
  },
  txttimesl: {
    fontSize: 20,
    textAlign: 'left',
    //width:width/2 -70,
    textAlignVertical: 'center',
    //marginBottom:20,
    //marginTop:30,
    color: Colors.line,
    // marginLeft:30,
    paddingHorizontal: 5,
  },
  txttimeslac: {
    width:'100%',
    fontSize: 35,
    textAlign: 'left',
    textAlign: 'center',
    //marginBottom:20,
    //marginTop:30,
    color: Colors.mainColor,
    //marginLeft:30,
    //width:width/2-70,
    paddingHorizontal: 5,
  },
  txttimeslaccatalog: {
    fontSize: 18,
    textAlign: 'left',
    width: '100%',
    textAlignVertical: 'center',
    // marginBottom:10,
    //marginTop:30,
    color: 'black',
    paddingHorizontal: 5,
  },
  item: {
    // padding: 20,
    height: 90,
    width: width / 4 - 6,
    borderWidth: 1,
    borderColor: 'gray',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,

  },
  viewrow: {
    height: 35,
    width,
    backgroundColor: '#eeee',
    justifyContent: 'center',
  },
  fill: {
    marginTop: 15,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    width:'100%',
    height: '100%',
    padding:5
    // backgroundColor:'blue'
  },
});
