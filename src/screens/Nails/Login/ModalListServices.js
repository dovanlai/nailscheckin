import React, { Component, } from 'react';
import {
  Modal,
  Text,
  TouchableOpacity,
  View,
  Alert,
  Dimensions,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';

import { TextCmp, } from '../../../common-components';
import { colors, } from 'react-native-elements';
import { Colors, } from '../../../theme';
import { CommonIcons, } from '../../../assets';

import { normalize, } from '../../../utils/FontSize';
const { width, height, } = Dimensions.get('window');
export default class ModalListServices extends Component {
  state = {
    index: 0,
    listadd: [],
    catsearch: 'All Services',
    listAddPedicure: [],
  };

  // setModalVisible(visible) {
  //   this.setState({modalVisible: visible});
  // }

  pressitem(index, item) {
    let listadd = this.state.listadd;
    let listAddPedicure = this.state.listAddPedicure;

    //console.log('listadd' + JSON.stringify(this.state.listadd));
    //console.log('listAddPedicure' + JSON.stringify(this.state.listAddPedicure));

    if (item.catname !== 'Pedicure') {
      if (listadd.findIndex((obj) => obj.name === item.name) == -1) {
        // alert(listadd.findIndex((obj) => obj.name === item.name));
        listadd.push(item);
      } else {
        listadd = listadd.filter((_item) => _item.name !== item.name);
      }
      this.setState({ index, item, listadd, listAddPedicure, });
      this.props.setStaffs(listadd);
    } else {
      if (listAddPedicure.findIndex((obj) => obj.name === item.name) == -1) {
        // alert(listadd.findIndex((obj) => obj.name === item.name));
        if (item.catname == 'Pedicure') listAddPedicure.push(item);
      } else {
        listAddPedicure = listAddPedicure.filter(
          (_item) => _item.name !== item.name
        );
      }
      this.setState({ index, item, listadd, listAddPedicure, });
      this.props.setStaffs(listAddPedicure);
    }
  }
  render() {
    const {
      modalVisible,
      setModalVisible,
      StaffsList,
      setStaffs,
      listserveceSearch,
      showok,
    } = this.props;

    let datalist = StaffsList;
    if (datalist && this.state.catsearch !== 'All Services') {
      datalist = StaffsList.filter(
        (cus) =>
          (cus.catname + '')
            .toLowerCase()
            .indexOf(this.state.catsearch.toLowerCase()) > -1
      );
    }

    return (
      <View
        style={{
          flex:1,
          borderRadius: 10,
          backgroundColor: '#fff',
          alignItems: 'center',
          position: 'absolute',
        }}
      >
        <View
          style={{
            height: 88,
            backgroundColor: Colors.mainColor,
            width,
            //alignItems: 'center',
            justifyContent: 'center',
            paddingBottom:5,
          }}
        >
          <TextCmp style={[styles.txtinbntac, { color: 'white', },]}>
            Please Select Prefer Services
          </TextCmp>
        </View>
        <ScrollView style={{   height: 90,marginBottom: 5,}} horizontal={true}>
          {listserveceSearch && (
            <View
              style={{
                marginTop: 0.5,
                // height: height / 12,
                //width,
                height: 88,
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent : 'center',
              }}
            >
              {listserveceSearch.map((item) => {
                return (
                  <TouchableOpacity
                    onPress={() => this.setState({ catsearch: item.catname, })}
                    style={{
                      backgroundColor:
                      this.state.catsearch == item.catname
                        ? Colors.mainColor
                        : '#ff6f00',
                      marginRight: 0.5,
                      width: width /6 ,//listserveceSearch.length,
                      //padding: 10,
                      height: 88,
                      marginLeft: 1,
                      alignItems : 'center' , 
                      justifyContent : 'center',
                    }}
                  >
                    <TextCmp
                      style={{
                        color: 'white',
                        fontSize: 18,
                        textAlign: 'center',
                      }}
                    >
                      {item.catname}
                    </TextCmp>
                  </TouchableOpacity>
                );
              })}
            </View>
          
          )}
        </ScrollView>
        <ScrollView style={{height:height-290,marginBottom: 100,}} >
          <View style={{height:'100%',}}>
        
            <View style={styles.fill}>
              {StaffsList &&
              datalist.map((item, index) => {
                if (item.id == '-1') {
                  return (
                    <View style={styles.viewrow} key={index + ''}>
                      <TextCmp style={styles.txttimeslaccatalog}>
                        {item.catname}
                      </TextCmp>
                     
                    </View>
                  );
                }
                return (
                  <TouchableOpacity
                    key={index + ''}
                    onPress={() => this.pressitem(index, item)}
                    style={[
                      styles.item,
                      {
                        borderColor:
                          item.catname === 'Pedicure'
                            ? this.state.listAddPedicure.findIndex(
                              (obj) => obj.name === item.name
                            ) != -1
                              ? 'red'
                              : 'black'
                            : this.state.listadd.findIndex(
                              (obj) => obj.name === item.name
                            ) != -1
                              ? 'red'
                              : 'black',
                      },
                    ]}
                  >
                    <View
                      style={{
                        flexDirection: 'row',
                        //flex: 1,
                        alignItems: 'center',
                      }}
                    >
                      <TextCmp
                        style={
                          item.catname === 'Pedicure'
                            ? this.state.listAddPedicure.findIndex(
                              (obj) => obj.name === item.name
                            ) != -1
                              ? styles.txttimeslac
                              : styles.txttimesl
                            : this.state.listadd.findIndex(
                              (obj) => obj.name === item.name
                            ) != -1
                              ? styles.txttimeslac
                              : styles.txttimesl
                        }
                      >
                        {item.name}
                      </TextCmp>
                      
                      {item.catname === 'Pedicure'
                        ? this.state.listAddPedicure.findIndex(
                          (obj) => obj.name === item.name
                        ) != -1 && (
                          <Image
                            style={{ width: 24, height: 24, }}
                            source={CommonIcons.checkok}
                          ></Image>
                        )
                        : this.state.listadd.findIndex(
                          (obj) => obj.name === item.name
                        ) != -1 && (
                          <Image
                            style={{ width: 24, height: 24, }}
                            source={CommonIcons.checkok}
                          ></Image>
                        )}

                        
                    </View>
                    <TextCmp style={styles.txttimesl}>
                        ${item.price}
                    </TextCmp>
                  </TouchableOpacity>
                );
              })}
            </View>
        

          </View>
        </ScrollView>
        {showok&&
        <TouchableOpacity onPress={()=>setModalVisible(false)} 
          style={{position:'absolute',bottom:10,backgroundColor:'red', justifyContent:'center',alignItems:'center', width:200, height:60, borderRadius:10,}}>
          <TextCmp style={{fontSize:25, color:'white',}}>ok</TextCmp>
        </TouchableOpacity>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtinbntac: {
    fontSize: 25,
    textAlign: 'center',
    width: '100%',
    textAlignVertical: 'center',
    marginBottom: 20,
    marginTop: 30,
  },
  viewbtnbottom: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    width: '100%',
    height: 100,
    position: 'absolute',
    bottom: 0,
    paddingHorizontal: 50,
  },
  txtbtnbottom: {
    fontSize: 20,
    textAlign: 'center',
    width: '100%',
    textAlignVertical: 'center',
    marginHorizontal: 20,
  },
  txttimesl: {
    fontSize: 20,
    textAlign: 'left',
    //width:width/2 -70,
    textAlignVertical: 'center',
    //marginBottom:20,
    //marginTop:30,
    color: Colors.blurWhite,
    // marginLeft:30,
    paddingHorizontal: 5,
  },
  txttimeslac: {
    fontSize: 18,
    textAlign: 'left',
    textAlignVertical: 'center',
    //marginBottom:20,
    //marginTop:30,
    color: Colors.mainColor,
    //marginLeft:30,
    //width:width/2-70,
    paddingHorizontal: 5,
  },
  txttimeslaccatalog: {
    fontSize: 18,
    textAlign: 'left',
    width: '100%',
    textAlignVertical: 'center',
    // marginBottom:10,
    //marginTop:30,
    color: 'black',
    paddingHorizontal: 5,
  },
  item: {
    padding: 20,
    height: 100,
    width: width / 4 - 6,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 3,
  },
  viewrow: {
    height: 35,
    width,
    backgroundColor: '#eeee',
    justifyContent: 'center',
  },
  fill: {
    marginTop: 15,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    width,

    height:'100%',
  },
});
