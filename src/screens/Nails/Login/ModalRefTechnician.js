import React, { Component, } from 'react';
import {
  Modal,
  Text,
  TouchableOpacity,
  View,
  Alert,
  Dimensions,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import { Avatar, } from 'react-native-elements';
import { TextCmp, } from '../../../common-components';
import { colors, } from 'react-native-elements';
import { Colors, } from '../../../theme';
import { Rating, AirbnbRating, } from 'react-native-ratings';
import { CommonIcons, } from '../../../assets';
import { API_URL_IMG, } from '../../../values/AppValue';
const { width, height, } = Dimensions.get('window');
export default class ModalRefTechnician extends Component {
  state = {
    index: 0,
    listadd: [],
  };

  pressitem(index, item) {
    let listadd = this.state.listadd;

    if (listadd.findIndex((obj) => obj.id === item.id) == -1) {
      // alert(listadd.findIndex((obj) => obj.name === item.name));
      listadd.push(item);
    } else {
      listadd = listadd.filter((_item) => _item.id !== item.id);
    }

    this.setState({ index: index, item, listadd, });
    this.props.setStaffs(listadd);
  }
  render() {
    const { modalVisible, setModalVisible, StaffsList, setStaffs, } = this.props;
    return (
      <ScrollView
        style={{
         
        }}
      >
        
        <View style={styles.fill}>
          {StaffsList &&
              StaffsList.map((item, index) => {
                return (
                  <TouchableOpacity
                    style={[
                      styles.btnViewTechnician,
                      {
                        borderColor:
                          this.state.listadd.findIndex(
                            (obj) => obj.id === item.id
                          ) != -1
                            ? 'red'
                            : 'black',
                      },
                    ]}
                    key={index + ''}
                    onPress={() => this.pressitem(index, item)}
                  >
                    {item.avatar&&item.avatar.length>0?
                      <Image style={{width:80,height:80,}} source={{uri:API_URL_IMG+item.avatar,}}></Image>:
                      <Avatar
                        rounded
                        icon={{ name: 'user', type: 'font-awesome', }}
                        size={80}
                        activeOpacity={0.7}
                      />
                      
                    }
                    

                    <TextCmp
                      style={
                        this.state.listadd.findIndex(
                          (obj) => obj.id === item.id
                        ) != -1
                          ? styles.txttimeslac
                          : styles.txttimesl
                      }
                    >
                      {item.first_name+item.last_name}
                    </TextCmp>
                    {this.state.listadd.findIndex(
                      (obj) => obj.id === item.id
                    ) != -1 && (
                      <Image
                        style={{
                          width: 20,
                          height: 20,
                          position: 'absolute',
                          right: 5,
                          bottom: 5,
                        }}
                        source={CommonIcons.checkok}
                      ></Image>
                    )}
                  </TouchableOpacity>
                );
              })}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  txtheader: {
    color: 'white',
    fontSize: 25,
    textAlign: 'center',
    width: '100%',
    textAlignVertical: 'center',
    marginBottom: 20,
    marginTop: 30,
  },
  txtinbntac: {
    fontSize: 25,
    textAlign: 'center',
    width: '100%',
    textAlignVertical: 'center',
    marginBottom: 20,
    marginTop: 30,
  },
  viewbtnbottom: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    width: '100%',
    height: 100,
    position: 'absolute',
    bottom: 0,
    paddingHorizontal: 50,
  },
  txtbtnbottom: {
    fontSize: 20,
    textAlign: 'center',
    width: '100%',
    textAlignVertical: 'center',
    marginHorizontal: 20,
  },
  txttimesl: {
    fontSize: 20,
    textAlign: 'center',
    width: '100%',
    textAlignVertical: 'center',
    marginBottom: 10,
    // marginTop:30,
    color: Colors.blurWhite,
  },
  txttimeslac: {
    fontSize: 20,
    textAlign: 'center',
    width: '100%',
    textAlignVertical: 'center',
    marginBottom: 20,
    //marginTop:30,
    color: Colors.mainColor,
  },
  btnViewTechnician: {
    height: height/5,
    width: '20%',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal:3 ,
    marginTop :3 , 
  },
  fill: {
    marginTop: 10,
    // marginBottom:70,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1 ,
  },
});
