import React, {Component,} from 'react';
import {Modal, Text, TouchableHighlight, View, Alert,Dimensions,} from 'react-native';
const {width,height,}=Dimensions.get('window');
export default  class ModelwhyDowndapp extends Component {
  // state = {
  //   modalVisible: true,
  // };

  // setModalVisible(visible) {
  //   this.setState({modalVisible: visible});
  // }

  render() {
    const {setclosemodel,ishowWhydownloadApp,textwhydownapp,}= this.props;
    return (
      <View style={{marginTop: 22,alignContent:'center',justifyContent:'center',alignItems:'center',flex:1,}}>
        <Modal
          animationType="slide"
          transparent={true}
          
          visible={ishowWhydownloadApp}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
            
          <View style={{marginTop: 22,width:'100%',height:'100%',alignContent:'center',justifyContent:'center',alignItems:'center',}}>
            <View style={{borderRadius:10,backgroundColor:'white',width:'70%', justifyContent:'center',alignItems:'center',}}>
              <Text style ={{fontSize:25,margin:20, }}>{textwhydownapp}</Text>

              
              <TouchableHighlight
                style={{ width:width/4,
                  height:height/14,
                  borderRadius:5,
                  backgroundColor:'#344566',
                  alignItems:'center',
                  justifyContent:'center',
                  margin: 10,}}
                onPress={() => {
                  setclosemodel();
                }}>
                <Text style={{fontSize:20,color:'white',}}>Close</Text>
              </TouchableHighlight>
             
            </View>
           
          </View>
        </Modal>

        <TouchableHighlight
          onPress={() => {
            this.setModalVisible(true);
          }}>
          <Text>Show Modal</Text>
        </TouchableHighlight>
      </View>
    );
  }
}