import React, {Component,} from 'react';
import {Modal, Text, TouchableHighlight, View, Alert,Dimensions,TextInput,StyleSheet,} from 'react-native';
import { Colors, } from '../../../theme';
import { KeyboardAwareScrollView, } from 'react-native-keyboard-aware-scroll-view';
import { APPVERSION, } from '../../../values/AppValue';
import { AppCheckIn_StoreSetting } from './action';
const {width,height,}=Dimensions.get('window');
export default  class ModalCreateStoreCdoe extends Component {
  constructor(props){
    super(props);
    this.state={
      StoreCode: props.storeCode, /// MAX39859 1688
      PINCode:props.PINCode,
    
    };
  }
 

  save(){
    const {StoreCode,PINCode,}= this.state;
    this.props.AppCheckIn_CheckOwnerPIN(PINCode,StoreCode);
  }

  componentDidMount(){
    // alert(this.props.PINCode+'  '+this.props.storeCode);
    setTimeout(() => {
      this.setState({PINCode:this.props.PINCode,StoreCode:this.props.storeCode,});
      //alert('xxx'+this.props.storeCode)
    
    }, 2000);
  
    
  }
 
  render() {
    // PINCode={this.state.PINCode}
    // storeCode={this.state.storeCode}
    const {setclosemodel,ishowWhydownloadApp,PINCode,storeCode,checkin_setting}= this.props;
    return (
      // <View style={{marginTop: 22,alignContent:'center',justifyContent:'center',alignItems:'center',flex:1,}}>
      <Modal
        animationType="slide"
        transparent={true}
          
        visible={ishowWhydownloadApp}
      >
        <KeyboardAwareScrollView>
          <View style={styles.viewModal}>
            <View style={styles.viewChild}>

              <Text style ={{fontSize:25,margin:20,}}>Management Code/Owner Code </Text>
              
               
              <TextInput
                placeholder={PINCode!==''?PINCode: 'Enter Code/Owner Code'}
                // placeholderTextColor={'white'}
                onChangeText={(text) => this.setState({PINCode:text,})}
                value={this.state.PINCode}
                style ={styles.btn}>
                  
              </TextInput>
              <TextInput
                placeholder={ storeCode!==''?storeCode:'Enter Store Code'}
                // placeholderTextColor={'white'}
                onChangeText={(text) => this.setState({StoreCode:text,})}
                value={this.state.StoreCode}
                style ={styles.btn}>
                    
              </TextInput>
              
              
              <View style={{flexDirection:'row', marginTop:10,}}>
                <TouchableHighlight
                  style={{ width:width/5,
                    height:height/15,
                    borderRadius:5,
                    borderColor:Colors.mainColor,
                    borderWidth:1,
                    alignItems:'center',
                    justifyContent:'center',
                    margin: 10,}}
                  onPress={() => {
                    setclosemodel();
                  }}>
                  <Text style={{fontSize:20,color:Colors.mainColor,}}>Cancel</Text>
                </TouchableHighlight>
                <TouchableHighlight
                  style={{ width:width/5,
                    height:height/15,
                    borderRadius:5,
                    borderColor:Colors.mainColor,
                    borderWidth:1,
                    alignItems:'center',
                    justifyContent:'center',
                    margin: 10,}}
                  onPress={() => {
                    this.save();
                  }}>
                  <Text style={{fontSize:20,color:Colors.mainColor,}}>Save</Text>
                </TouchableHighlight>
              </View>
              <Text style={{fontSize:10,color:Colors.mainColor, margin:10,}}>{APPVERSION} - setting: { checkin_setting}</Text>
              
            </View>
           
          </View>
        

        </KeyboardAwareScrollView>
          
      </Modal>

        
    // </View>
    );
  }
}

const styles= StyleSheet.create({
  btn:{
    width:width/2,
    height:height/14,
    borderRadius:5,
    // backgroundColor:'#344566',
    alignItems:'center',
    justifyContent:'center',
    margin: 10,
    color:Colors.mainColor,
    borderColor:Colors.mainColor,
    borderWidth:1,
    paddingHorizontal:20,

    
  },
  viewModal:{flex:1,
    justifyContent:'center',alignItems:'center',backgroundColor:'white', },
  viewChild:{backgroundColor:'#eeeeee',borderRadius:10,
    width:'100%',height:'100%', justifyContent:'center'
    ,alignItems:'center',},
});