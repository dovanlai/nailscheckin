import React, {Component,} from 'react';
import {Modal, Text, TouchableOpacity, View, Alert,
  Dimensions,StyleSheet,
  ScrollView,
} from 'react-native';

import { TextCmp, } from '../../../common-components';
import { colors, } from 'react-native-elements';
import { Colors, } from '../../../theme';
import LogoView from '../../../screen-nails/old-customer/LogoView';

const {width,height,}= Dimensions.get('window');
export default class ModalTime extends Component {
  constructor(){
    super();
    this.state = {
      index: 0,
    };
  }


  // setModalVisible(visible) {
  //   this.setState({modalVisible: visible});
  // }

  render() {
    const{
      modalVisible,
      setModalVisible,
      Datatime,
      settime,
    }=this.props;
    return (
      // <View style={{marginTop: 22,alignContent:'center',justifyContent:'center',}}>
    // <Modal
    //   animationType="slide"
    //   transparent={true}
    //   visible={modalVisible}
    //  >
      <View style={{
        width,//:width/2,
        height:height,//:width/3,
        borderRadius:10,backgroundColor:'#eeeeee',
        // marginLeft: width/4,
        // marginTop:height/4,
        alignItems:'center',
        position:'absolute',
      }}>
        <LogoView></LogoView>
        <TextCmp style={styles.txtinbntac}>Please select appointment time</TextCmp>
        <ScrollView style={{marginBottom: 70,}}>
          <View>
            {Datatime&&Datatime.length>0 &&Datatime.map((item,index)=>{
              return(
                <TouchableOpacity key= {index+''} onPress={()=>{this.setState({index:index,});}}>
                  <TextCmp style={ index==this.state.index? styles.txttimeslac:styles.txttimesl}>{item.appoimentTime}</TextCmp>
                </TouchableOpacity>
              );
            })}
          </View>
             
        </ScrollView>
        <View style ={styles.viewbtnbottom}>
          <TouchableOpacity  onPress={setModalVisible}  >
            < Text style ={styles.txtbtnbottom}>Cancel</Text>
                
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{settime(this.state.index);
          }} >
            < Text style ={styles.txtbtnbottom}>  Ok  </Text>
          </TouchableOpacity>
        </View>
      </View>
    // </Modal>

        
    // </View>
    );
  }
}

const styles= StyleSheet.create({

  txtinbntac:{
    fontSize:25,
    textAlign:'center',
    width:'100%',
    textAlignVertical:'center',
    marginBottom:20,
    marginTop:30,
  },
  viewbtnbottom:{
    alignItems:'center',
    justifyContent:'flex-end',
    flexDirection:'row',
    width:'100%',
    height:200,
    position:'absolute',
    bottom:0,
    paddingHorizontal:50,
  },
  txtbtnbottom:{
    fontSize:20,
    textAlign:'center',
    width:'100%',
    textAlignVertical:'center',
    marginHorizontal:20,
   
  },
  txttimesl:{
    fontSize:40,
    textAlign:'center',
    width:'100%',
    textAlignVertical:'center',
    marginBottom:20,
    marginTop:30,
    color:Colors.blurWhite,
  },
  txttimeslac:{
    fontSize:40,
    textAlign:'center',
    width:'100%',
    textAlignVertical:'center',
    marginBottom:20,
    marginTop:30,
    color:Colors.mainColor,
  },
});