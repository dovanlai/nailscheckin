import React, {Component,} from 'react';
import { Modal, Text, View, 
  Alert, Dimensions, TextInput, StyleSheet, TouchableOpacity,  Image, } from 'react-native';
import { Colors, } from '../../../theme';
import { TextCmp, } from '../../../common-components';
import { ImagesIcons, CommonIcons, } from '../../../assets';
const {width,height,}=Dimensions.get('window');
export default  class ModalQRView extends Component {

  // componentDidMount(){
  //   setTimeout(() => {
  //     this.props.setclosemodel();
  //   }, 10000);
  // }
  render() {
    const {showReview,closeModalQRView,}= this.props;
    return (
      // <View style={{marginTop: 22,alignContent:'center',justifyContent:'center',alignItems:'center',flex:1,}}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={showReview}>
        <View style={styles.contant}>
          <View style={styles.viewmodal}>
            <TouchableOpacity onPress={closeModalQRView} style={{position:'absolute',top:20, right:0,width:55, height:55,}}>
              <Image style={{width:20, height:20, padding:10,tintColor:'gray',}} source={CommonIcons.close}></Image>
            </TouchableOpacity>
            {/* <TextCmp>How  would you rate our service today?</TextCmp> */}
            <Image style={styles.img} source={this.props.qr}>
            </Image>    
             
          </View>
           
        </View>
        

 
      </Modal>

        
    // </View>
    );
  }
}

const styles= StyleSheet.create({
  contant:{
    width:'100%',height:'100%',alignContent:'center',
    justifyContent:'center',alignItems:'center',paddingBottom : 30, 
    backgroundColor:'gray',
  },
  btnReview: {borderColor:'white',borderRadius:5, borderWidth:1,flex:1, margin:20,
    justifyContent:'center', alignItems:'center',
    backgroundColor:'white',
  },
  img:{width:400, height:400,},
  viewmodal:{backgroundColor:'#eeeeee',
    borderRadius:10,width:550,height:550, justifyContent:'center',alignItems:'center',},
  
});