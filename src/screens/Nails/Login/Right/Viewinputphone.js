
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,

} from 'react-native';
import { ImagesIcons, } from '../../../../assets';
import { Colors, } from '../../../../theme';
import { TextCmp, } from '../../../../common-components';
import { Icon, } from 'react-native-elements';
import Icons from '../../../../assets/Icons';
const {height,width,}= Dimensions.get('window');

export default class ViewRightInputScreen extends React.Component {
 
  render(){
    const arr= [
      {id:1,ten:1,},
      {id:2,ten:2,},
      {id:3,ten:3,},
      {id:4,ten:4,},
      {id:5,ten:5,},
      {id:6,ten:6,},
      {id:7,ten:7,},
      {id:8,ten:8,},
      {id:9,ten:9,},
      {id:10,ten:'OK',},
      {id:11,ten:0,},
      {id:12,ten:'',},
    ];
    const {keypress,phone,xoa,}= this.props;
    return (
      <View  style={styles.containt}>
        <View style={styles.logoview}>
          
          <TextCmp style={styles.txtinbnt}>Welcome to nganhnails </TextCmp>
          <TextCmp style={styles.txtinbntac}>Type your phone </TextCmp>
        </View>
        {/* <View style={styles.TimeView}>
          
        </View> */}
        <View style={styles.btngroupview}>
          <TouchableOpacity  style ={styles.btnac} >
            <TextCmp style={styles.txtshowmore}>{phone}</TextCmp>
          </TouchableOpacity>
          {/* <TouchableOpacity style ={{position:'absolute', right:60, top:32}} onPress={xoa}>
            
              
              <Icons
                  name={ 'disconnect'}
                  size={25}
                  color={Colors.blurWhite}
                />
            </TouchableOpacity> */}
          <View style={styles.keyboard}>
            {arr.map((x)=>{
              if(x.id===12)
              {return(
                <TouchableOpacity style ={styles.btnkey} onPress = {()=>keypress(x)}>
                  <Icons
                    name={ 'disconnect'}
                    size={25}
                    color={Colors.blurWhite}
                  />
                </TouchableOpacity>
              );}
              return(
                <TouchableOpacity style ={styles.btnkey} onPress = {()=>keypress(x)}>
                  <Text style={x.id==10||x.id==12? styles.txtchechin:styles.txtinbnt}>{x.ten}</Text>
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
       
        

        {/* <View style={styles.logoview}>
           <Image style={styles.imgdownApp} source={ImagesIcons.downloadApp} />
        </View> */}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  containt:{
    flex:1, 
    justifyContent:'center',
    alignItems:'center',
  },
  btnkey:{
    width:(width/4-20)/3 - 5,
    height:(width/4-20)/3 - 5,
    borderRadius:((width/4-20)/3 - 5)/2,
    margin:10,
    borderColor:Colors.blurWhite,
    borderWidth:2,
    alignItems:'center',
    justifyContent:'center',
    
  },
  keyboard:{
    width:width/4+40,
    height:width/3,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 20 ,
    // borderColor:Colors.blurWhite,
    // borderWidth:2,
    alignItems:'center',
  },
  imgPicture:{
    width:500,
    height:100,
  },
  logoview:{
    width:width/2,
    justifyContent:'center',
    height:height/4,
    alignItems:'center',
    marginTop:50,
    
  },
  TimeView:{
    width:width/2,
    justifyContent:'center',
    height:height/5,
    alignItems:'center',
  },
  txttime:{
    fontSize:70,
    //fontWeight:'bold',
    color:'white',
    textAlign:'center',
    width:width/2,

  },
  txttime2:{
    fontSize:25,
    textAlign:'center',
    width:width/2,
    color:Colors.blurWhite,

  },
  btngroupview:{
    alignItems:'center',
    width:width/2,
    justifyContent:'center',
    alignContent:'center',

  },
  btn:{
    width:width/6,
    height:height/15,
    borderRadius:5,
    backgroundColor:'#344566',
    alignItems:'center',
    justifyContent:'center',
    margin: 10,
    color:'white',
  },
  btnac:{
    width:width/3,
    height:height/15,
    borderRadius:5,
    //backgroundColor:'#788399',
    alignItems:'center',
    justifyContent:'center',
    margin: 10,
    borderColor:'white',
    borderWidth:1,
    fontSize:20,
    paddingHorizontal:10,
    color:'white',
  },
  btn3:{
    width:width/4,
    height:height/15,
    borderRadius:5,
    backgroundColor:'#C2AF71',
    alignItems:'center',
    justifyContent:'center',
    margin: 10,
    borderColor:'white',
    // borderWidth:1,
    fontSize:20,
    paddingHorizontal:10,
  },
  btn:{
    width:width/6,
    height:height/14,
    borderRadius:5,
    //backgroundColor:'#788399',
    alignItems:'center',
    justifyContent:'center',
    margin: 10,
    borderColor:'white',
    borderWidth:1,
    fontSize:20,
    paddingHorizontal:10,
  },
  txtinbnt:{
    fontSize:27,
    textAlign:'center',
    width:'100%',
    color:'white',
    textAlignVertical:'center',
  },
  txtinbntac:{
    fontSize:40,
    textAlign:'center',
    width:'100%',
    color:'white',
    textAlignVertical:'center',
    marginBottom:30,
    marginTop:30,
  },
  imgdownApp:{
    width:220,
    height:200,
  },
  inputgrouptxt:{
    flexDirection:'row',
   
  },
  viewshowmore:{
    flexDirection:'row',
    width:width/2.4,
    //marginVertical:5,
    justifyContent:'flex-end',
    alignItems:'flex-end',
    
  },
  txtshowmore:{
    color:'white',
    fontSize:30,
    textAlign:'center',
    width:'100%',
  

  },
  txtchechin:{
    color:'white',
    fontSize:16,
    textAlign:'center',
    width:'100%',
  },
});