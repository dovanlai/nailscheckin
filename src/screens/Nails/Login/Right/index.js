
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Alert,

} from 'react-native';
import { ImagesIcons, } from '../../../../assets';
import { Colors, } from '../../../../theme';
import { TextCmp, } from '../../../../common-components';
import { Icon, } from 'react-native-elements';
import moment from 'moment';
const {height,width,}= Dimensions.get('window');

export default class RightScreen extends React.Component {
  state={
    ishowlistsingin:false,
    HaveAnAppoiment:false,
  }
  
  render(){
    const {createacount,SignInList,showlistTechnician,paramcus,handleChangeText,sigin,showinput,istime,showtimeSelect,ishowWhydownloadApp,
      Staffs,showServicesList,Services,HaveAnAppoiment,WelcomeText,}= this.props;
    return (
      <View  style={styles.containt}>

        <View style={{ position:'absolute',
          bottom:15,}}>
          <View style={styles.logoview}>
            <Text style={{color:'white', fontSize:20,}}>Nails Technician App</Text>
            <Image style={styles.imgdownApp} source={ImagesIcons.qrnganhnails} />
            <TouchableOpacity onPress={ishowWhydownloadApp}>
              <Text style={{color:'white', marginTop:1,}}>Why download App?</Text>
            </TouchableOpacity>
          </View>
        </View>


        <View style={styles.logoview}>
          
          <TextCmp style={styles.txtinbntac}> {WelcomeText?WelcomeText:'Welcome to nganhnails'} </TextCmp>
          
          {/* <TextCmp style={styles.txtinbntac}>Type your phone </TextCmp> */}
        </View>

        <View style={styles.btngroupview}>

          <View style={{flexDirection:'row', justifyContent:'space-between',}}>
            <TouchableOpacity style={{backgroundColor:'#7A8397', borderRadius:3, paddingVertical:10, marginHorizontal: 2,}}>
              <Text style={{color:'white',fontSize:14,marginHorizontal: 3, }}>Current Customer Sign In</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={createacount} style={{backgroundColor:'#384563', borderRadius:3, paddingVertical:10, marginHorizontal:2,}}>
              <Text style={{color:'white',fontSize:14,marginHorizontal: 3, }} >New Customer Register Here</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity  onPress= {()=>showinput()} style ={styles.btnac} >
            <TextCmp style={styles.txtshowmore2}>{paramcus.phone}</TextCmp>
          </TouchableOpacity>
          <View style ={styles.inputgrouptxt}>
            <TextInput
              placeholder={'Fist name'}
              placeholderTextColor={'white'}
              onChangeText={(text) => handleChangeText(text, 'firstName')}
              value={paramcus.firstName}
              style ={styles.btn}>
              
            </TextInput>
            <TextInput
              placeholder={'Last name'}
              placeholderTextColor={'white'}
              onChangeText={(text) => handleChangeText(text, 'lastName')}
              value={paramcus.lastName}
              style ={styles.btn}>
              
            </TextInput>
          </View>
          <View style={{justifyContent:'center',marginVertical:5,}}>
            <TextCmp style={styles.txtshowmore2}>Do you have an appointment ?.</TextCmp>
            <View style={{flexDirection:'row' ,justifyContent:'center',}}>
              <TouchableOpacity style={!this.state.HaveAnAppoiment? styles.btnYN:styles.btnYN2}  onPress={()=>{
                this.setState({HaveAnAppoiment:false,});
                HaveAnAppoiment(false);
              }}>
                <TextCmp style={styles.txtshowmore2}>Yes</TextCmp>
              </TouchableOpacity >

              <TouchableOpacity style={this.state.HaveAnAppoiment? styles.btnYN:styles.btnYN2} onPress={()=>{
                this.setState({HaveAnAppoiment:false,});
                HaveAnAppoiment(false);
              }}>
                <TextCmp style={styles.txtshowmore2}>No</TextCmp>
              </TouchableOpacity>
            </View>
          </View>

          {!this.state.HaveAnAppoiment&& 
          <View tyle={{flexDirection:'row' ,justifyContent:'center',}}>
            <TouchableOpacity onPress ={showlistTechnician}   style ={styles.btnac} >
              <TextCmp style={styles.txtshowmore2}>{Staffs?//Staffs.name:
                (Staffs.map((item,index)=>{
                  if(index===Staffs.length-1)
                  {return item.name;}
                  return item.name+' - ';
                })):
                'Please Select Prefer Nails Technician'}</TextCmp>
            </TouchableOpacity>
            <TouchableOpacity onPress ={showServicesList}   style ={styles.btnac} >
              <TextCmp style={styles.txtshowmore2}> {Services?(Services.map((item,index)=>{
                if(index===Services.length-1)
                {return item.name;}
                return item.name+' - ';
              })):'Please Select Prefer Services'}</TextCmp>
            </TouchableOpacity>
          </View>
          }

          <View>
        
            {this.state.HaveAnAppoiment&& this.state.ishowlistsingin&& SignInList&&SignInList.length>0 &&  SignInList.map((item)=>{
              return(
                <View style={styles.viewshowmore} >
                  <TextCmp style={styles.txtshowmore}>  {moment(item.siginInDate).format('YYYY ddd MMM DD HH:mm') }</TextCmp>
                </View>
              );
            })}

          </View>

          <View style={{width:width/2,alignItems:'center',justifyContent:'center',margin:0,}}>
            {this.state.HaveAnAppoiment &&<TouchableOpacity onPress={showtimeSelect}>
              <TextCmp style={{fontSize: 18,color:'white', width:'100%', textAlign:'center',}}> {istime&& istime.appoimentTime!==''? 'Please select time':''}</TextCmp>
              <TextCmp style={{fontSize: 18,color:'white', width:'100%', textAlign:'center',marginVertical: 5,}}>       { istime? istime.appoimentTime:''} </TextCmp>
            </TouchableOpacity>}


            <TouchableOpacity style ={styles.btn3} onPress ={()=>sigin()}>
              <TextCmp style={[styles.txtinbnt,{color:'#00001F',},]}>Sign in</TextCmp>
            </TouchableOpacity>

          </View>
     
        </View>
        {/* <TouchableOpacity style={styles.viewshowmore} onPress={()=> this.setState({ishowlistsingin:!this.state.ishowlistsingin})}>
          <TextCmp style={styles.txtshowmore}>Sign List</TextCmp>
        </TouchableOpacity> */}

      </View>
    );
  }
}
const styles = StyleSheet.create({
  containt:{
    flex:1, 
    justifyContent:'center',
  },
  btnYN:{
    width:70,
    height:40,
    margin:10,
    backgroundColor:'#344566',
    alignItems:'center',
    justifyContent:'center',
    borderRadius:5,

  },
  btnYN2:{
    width:70,
    height:40,
    margin:10,
    backgroundColor:'#788399',
    alignItems:'center',
    justifyContent:'center',
    borderRadius:5,
  },

  logoview:{
    width:width/2,
    justifyContent:'center',
    //height:200,
    alignItems:'center',
    marginTop:20,
    
  },


  btngroupview:{
    alignItems:'center',
    width:width/2,
    justifyContent:'center',
    marginBottom:height/7,
  },

  btnac:{
    width:width/3+20,
    height:height/20,
    borderRadius:5,
    //backgroundColor:'#788399',
    alignItems:'center',
    justifyContent:'center',
    margin: 10,
    borderColor:'white',
    borderWidth:1,
    fontSize:20,
    paddingHorizontal:10,
    color:'white',
  },
  btn3:{
    width:width/4,
    height:height/20,
    borderRadius:5,
    backgroundColor:'#C2AF71',
    alignItems:'center',
    justifyContent:'center',
    // margin: 10,
    borderColor:'white',
    // borderWidth:1,
    fontSize:20,
    paddingHorizontal:10,
  },
  btn:{
    width:width/6,
    height:height/20,
    borderRadius:5,
    //backgroundColor:'#788399',
    alignItems:'center',
    justifyContent:'center',
    margin: 10,
    borderColor:'white',
    borderWidth:1,
    fontSize:16,
    paddingHorizontal:10,
    color:'white',
  },
  txtinbnt:{
    fontSize:27,
    textAlign:'center',
    width:'100%',
    color:Colors.blurWhite,
    textAlignVertical:'center',
  },
  txtinbntac:{
    fontSize:30,
    textAlign:'center',
    width:'100%',
    color:'white',
    textAlignVertical:'center',
    marginBottom:30,
    marginTop:30,
  },
  imgdownApp:{
    width:height/10,
    height:height/10,
    margin:20,
  },
  inputgrouptxt:{
    flexDirection:'row',
   
  },
  viewshowmore:{
    flexDirection:'row',
    width:width/2.4,
    //marginVertical:5,
    justifyContent:'flex-end',
    alignItems:'flex-end',
    
  },
  txtshowmore:{
    color:'white',
    fontSize:16,
    textAlign:'right',
    width:'100%',
  

  },
  txtshowmore2:{
    color:'white',
    fontSize:16,
    textAlign:'center',
    width:'100%',
  

  },
});