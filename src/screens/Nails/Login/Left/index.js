
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  Linking,

} from 'react-native';
import { ImagesIcons, } from '../../../../assets';
import { Colors, } from '../../../../theme';
const {height,width,}= Dimensions.get('window');
import moment from 'moment';
export default class LeftScreen extends React.Component {
  
  openLink(){
    const href = 'https://www.google.com/search?sxsrf=ACYBGNR-ObW0ZvzUdytYcCn6HVJpADDrew%3A1572126370374&ei=or60XZe4FtG0swWszIq4BQ&q=Urban+Escape+Nails+Spa%2C+College+Park+Drive%2C+The+Woodlands%2C+TX&oq=urban+escape+nails+spa&gs_l=psy-ab.1.3.0i22i30l3j38.3046517.3053245..3056693...0.4..0.97.1473.22......0....1..gws-wiz.......0i71j35i39j0j0i131j0i67j0i10.teC7AzvCN8M#lrd=0x864737357e79f843:0xc4519ef3698d599c,1,,';
    Linking.canOpenURL(href).then((supported) => {
      if (supported) {
        Linking.openURL(href);
      }
    });
  }
  
  render(){
    const {indexbtn, setindexbtn,ishowWhydownloadApp,Logo,}= this.props;
    var jun = moment(new Date()).format('HH:MM');
    var day = moment(new Date()).format('LL');
    
    return (
      <View  style={styles.containt}>
        <View style={{ 
          bottom:10,position:'absolute',}}>
          <View style={styles.logoview}>
            <Text style={{color:'white', fontSize:20,}}>Download customer app</Text>
            <Image style={styles.imgdownApp} source={ImagesIcons.qrnailspa} />
            <TouchableOpacity onPress={ishowWhydownloadApp}>
              <Text style={{color:'white', marginTop:1,}}>Why download App?</Text>
            </TouchableOpacity>
          </View>
        </View>
       

        <View style={styles.logoview}>
          {Logo?<Image resizeMode={'contain'} style={styles.imgPicture} source={{uri:Logo,}} />:
            <Image style={styles.imgPicture} source={ImagesIcons.logonails} />}
        </View>
        <View style={styles.TimeView}>
          <Text style={styles.txttime}>{jun}</Text>
          <Text style={styles.txttime2}>{day}</Text>
        </View>
        <View style={styles.btngroupview}>
          <TouchableOpacity onPress={()=>setindexbtn(0)} style ={ indexbtn===0?  styles.btnac:styles.btn}>
            <Text style={ indexbtn===0?  styles.txtinbntac: styles.txtinbnt}>Customer Sign</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>setindexbtn(1)} style ={ indexbtn===1?  styles.btnac:styles.btn}>
            <Text style={ indexbtn===1?  styles.txtinbntac: styles.txtinbnt}>Nails Technician</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>setindexbtn(2)} style ={ indexbtn===2?  styles.btnac:styles.btn}>
            <Text style={ indexbtn===2?  styles.txtinbntac: styles.txtinbnt}>Employee</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.openLink()} style ={ [ styles.btn,{backgroundColor:'#C2AF71',},]}>
            <Text style={[styles.txtinbnt,{color:'white',},]}>Reviews</Text>
          </TouchableOpacity>
        </View>
       
      </View>
    );
  }
}
const styles = StyleSheet.create({
  containt:{
    flex:1, 
  },

  imgPicture:{
    width:500,
    height:100,
  },
  logoview:{
    width:width/2,
    justifyContent:'center',
    //height:height/4,
    alignItems:'center',
    marginTop:50,
   
    
  },
  TimeView:{
    width:width/2,
    justifyContent:'center',
    //height:height/5-80,
    alignItems:'center',
    marginVertical:10,
    //backgroundColor:'blue',
  },
  txttime:{
    fontSize:70,
    //fontWeight:'bold',
    color:'white',
    textAlign:'center',
    width:width/2,

  },
  txttime2:{
    fontSize:25,
    textAlign:'center',
    width:width/2,
    color:Colors.blurWhite,

  },
  btngroupview:{
    alignItems:'center',
    width:width/2,
    justifyContent:'center',
    marginBottom:20,
  },
  btn:{
    width:width/4,
    height:60,
    borderRadius:5,
    backgroundColor:'#344566',
    alignItems:'center',
    justifyContent:'center',
    margin: 10,
  },
  btnac:{
    width:width/4,
    height:60,
    borderRadius:5,
    backgroundColor:'#788399',
    alignItems:'center',
    justifyContent:'center',
    margin: 10,
    padding:10,
  },
  txtinbnt:{
    fontSize:25,
    textAlign:'center',
    width:'100%',
    color:Colors.blurWhite,
    textAlignVertical:'center',
  },
  txtinbntac:{
    fontSize:25,
    textAlign:'center',
    width:'100%',
    color:'white',
    textAlignVertical:'center',
  },
  imgdownApp:{
    width:height/10,
    height:height/10,
    margin:20,
  },
});