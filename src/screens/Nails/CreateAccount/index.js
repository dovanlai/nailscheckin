import React, { Component, } from 'react';

import {
  Modal,
  Text,
  TouchableHighlight,
  View,
  Alert,
  Dimensions,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
const { width, height, } = Dimensions.get('window');
import { KeyboardAwareScrollView, } from 'react-native-keyboard-aware-scroll-view';
import { Colors, } from '../../../theme';
import { SnackBar, } from '../../../utils';
import Toast, { DURATION, } from 'react-native-easy-toast';
import ListPopover from 'react-native-list-popover';
import { Dropdown, } from 'react-native-material-dropdown';
import LogoView from '../../../screen-nails/old-customer/LogoView';

import ButtonBottom from '../../../screen-nails/component-check-in/button-bottom';
const items = [
  { value: '01', },
  { value: '02', },
  { value: '03', },
  { value: '04', },
  { value: '05', },
  { value: '06', },
  { value: '07', },
  { value: '08', },
  { value: '09', },
  { value: '10', },
  { value: '11', },
  { value: '12', },
];
const itemsdat = [
  { value: '01', },
  { value: '02', },
  { value: '03', },
  { value: '04', },
  { value: '05', },
  { value: '06', },
  { value: '07', },
  { value: '08', },
  { value: '09', },
  { value: '10', },
  { value: '11', },
  { value: '12', },
  { value: '13', },
  { value: '14', },
  { value: '15', },
  { value: '16', },
  { value: '17', },
  { value: '18', },
  { value: '19', },
  { value: '20', },
  { value: '21', },
  { value: '22', },
  { value: '23', },
  { value: '24', },
  { value: '25', },
  { value: '26', },
  { value: '27', },
  { value: '28', },
  { value: '29', },
  { value: '30', },
  { value: '31', },
];
export default class CreateAccount extends Component {
  state = {
    paramcus: {
      phone: '',
      referralCode: '',
      firstName: '',
      lastName: '',
      email: '',
      birthday_month: '',
      birthday_day: '',
      address: '',
      state: '',
      city: '',
      zipcode: '',
    },
    isVisible: false,
    isVisibleday: false,
    showDiaalog:false,
    sms:'',
  };
  componentDidMount() {
    if (
      this.props.PINCode.length > 0 &&
      this.props.PINCode !== 'Phone nummer'
    ) {
      this.handleChangeText(this.props.PINCode, 'phone');
    }


    AsyncStorage.getAllKeys((err, keys) => {
      AsyncStorage.multiGet(keys, (err, stores) => {
        stores.map((result, i, store) => {
          // get at each store's key/value so you can work with it
          let key = store[i][0];
          let val = store[i][1];
          
          //console.log('Logo---------' + val);

          if (key === 'StoreName' && val.length > 0) {     
            let sms='I hereby consent to receive text message advertisements from or on behalf of '+ val + ' at the telephone number provided herein. I understand that consent is not a condition of purchase or receiving a service and that I may revoke consent to receive text message advertisements at any time. I understand and agree that any text message I receive may be sent by an autodialer.';    
            this.setState({ storeCode: val,sms, });
            //alert(sms);
            // setTimeout(() => {
            //   this.setState({showDiaalog:true,});
            // }, 100);
          }
          if (key === 'PINCode' && val.length > 0) {
            this.setState({ PINCode: val, });
          }
                 
        });
      });
    });

  }
  handleChangeText(text, key) {
    this.setState((state) => ({
      paramcus: {
        ...state.paramcus,
        [key]: text,
      },
    }));
  }
  render() {
    const {
      setclosemodel,
      ishowWhydownloadApp,
      AppCheckIn_CustomerSignUp,
      onPressBtn1,
      onPressBtn2,
    } = this.props;
    const { paramcus,sms, } = this.state;
    return (
      <View style={{ flex: 1, }}>
        <View
          style={{
            marginTop: 22,
            alignContent: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}
        >

          <KeyboardAwareScrollView>
            <View
              style={{
                marginTop: 22,
                width: '100%',
                height: '100%',
                alignContent: 'center',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <View
                style={{
                  borderRadius: 10,
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#4F8CB8',
                }}
              >
                {/* <LogoView></LogoView> */}
                <View
                  style={{
                    flexDirection: 'row',
                    height: height / 10,
                  }}
                >
                  {/* <LogoView
                      containerStyles={{
                        flex: 1,
                        paddingLeft: '8%',
                      }}
                      imageStyles={{ height: '95%', width: '90%', }}
                    ></LogoView> */}
                  <View
                    style={{
                      //flex: 4,
                      justifyContent: 'flex-end',
                      paddingBottom: 10,
                      paddingLeft: '8%',
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 30,
                        color: 'white',
                      }}
                    >
                        NEW CUSTOMER INFOR
                    </Text>
                  </View>
                </View>
                <View style={{ flex:1,height:height-height/5, }}>
                  <View style={{ flexDirection: 'row', }}>
                    <TextInput
                      //editable={this.props.islock}
                      placeholder={'Phone* (required)'}
                      placeholderTextColor="black"
                      onChangeText={(text) =>
                        this.handleChangeText(text, 'phone')
                      }
                      keyboardType={'numeric'}
                      value={paramcus.phone}
                      style={styles.btn}
                    ></TextInput>
                    <TextInput
                      placeholder={'Referral code (optional)'}
                      placeholderTextColor="black"
                      onChangeText={(text) =>
                        this.handleChangeText(text, 'referralCode')
                      }
                      value={paramcus.referralCode}
                      style={styles.btn}
                    ></TextInput>
                  </View>

                  <View style={{ flexDirection: 'row', }}>
                    <View style={{ flexDirection: 'row', }}>
                      <TextInput
                        placeholder={'Firstname* (required)'}
                        placeholderTextColor="black"
                        onChangeText={(text) =>
                          this.handleChangeText(text, 'firstName')
                        }
                        value={paramcus.firstName}
                        style={[styles.btn, { width: width / 4.8-3, margin: 1, },]}
                      ></TextInput>
                      <TextInput
                        placeholder={'Lastname* (required)'}
                        placeholderTextColor="black"
                        onChangeText={(text) =>
                          this.handleChangeText(text, 'lastName')
                        }
                        value={paramcus.lastName}
                        style={[styles.btn, { width: width / 4.8, margin: 1, },]}
                      ></TextInput>
                    </View>
                    <TextInput
                      placeholder={'Email (optional)'}
                      placeholderTextColor="black"
                      onChangeText={(text) =>
                        this.handleChangeText(text, 'email')
                      }
                      value={paramcus.email}
                      style={styles.btn}
                    ></TextInput>
                  </View>

                  <Text style={{ marginTop: 15,fontSize:18, }}>Birthday (optional)</Text>
                  <View style={{ flexDirection: 'row', }}>
                    <View style={{ flexDirection: 'row', }}>
                      <View
                        style={[
                          styles.btn,
                          {
                            width: width / 2.4 + 10,
                            margin: 1,
                          },
                        ]}
                      >
                        <Dropdown
                          itemCount={8}
                          containerStyle={{ flex: 1, width: '100%', }}
                          label="Month"
                          value={paramcus.birthday_month || 'Select Month'}
                          data={items}
                          onChangeText={(item) =>
                            this.handleChangeText(item, 'birthday_month')
                          }
                        />
                      </View>
                      <View
                        style={[
                          styles.btn,
                          {
                            width: width / 2.4 + 10,
                            margin: 1,
                          },
                        ]}
                      >
                        <Dropdown
                          itemCount={8}
                          containerStyle={{ flex: 1, width: '100%', }}
                          label="Day"
                          value={paramcus.birthday_day || 'Select Day'}
                          data={itemsdat}
                          onChangeText={(item) =>
                            this.handleChangeText(item, 'birthday_day')
                          }
                        />
                      </View>
                    </View>
                  </View>

                  {/* <View style={{ flexDirection: 'row', }}>
                      <TextInput
                        placeholder={'Address'}
                        placeholderTextColor="black" 
                        onChangeText={(text) =>
                          this.handleChangeText(text, 'address')
                        }
                        value={paramcus.address}
                        style={styles.btn}
                      ></TextInput>

                      <TextInput
                        placeholder={'City'}
                        placeholderTextColor="black" 
                        onChangeText={(text) =>
                          this.handleChangeText(text, 'city')
                        }
                        value={paramcus.city}
                        style={styles.btn}
                      ></TextInput>
                    </View> */}

                  {/* <View style={{ flexDirection: 'row', }}>
                      <TextInput
                        placeholder={'State'}
                        placeholderTextColor="black" 
                        onChangeText={(text) =>
                          this.handleChangeText(text, 'state')
                        }
                        value={paramcus.state}
                        style={styles.btn}
                      ></TextInput>
                      <TextInput
                        placeholder={'code'}
                        placeholderTextColor="black" 
                        onChangeText={(text) =>
                          this.handleChangeText(text, 'zipcode')
                        }
                        value={paramcus.zipcode}
                        style={styles.btn}
                      ></TextInput>
                    </View> */}
                </View>

                {/* <View
                    style={{
                      width: width / 2,
                      height: 2,
                      margin: 20,
                      backgroundColor: 'white',
                    }}
                  ></View> */}

                {/*
          <View
             style={{
               flexDirection: "row",
               justifyContent: "space-between",
               margin: 20
             }}
           >
             <TouchableOpacity
               style={{
                 width: width / 6,
                 height: height / 14,
                 borderRadius: 5,
                 backgroundColor: "white",
                 alignItems: "center",
                 justifyContent: "center",
                 margin: 10
               }}
               onPress={() => {
                 AppCheckIn_CustomerSignUp(this.state.paramcus, "1");
               }}
             >
               <Text style={{ fontSize: 20 }}>Add Customer</Text>
             </TouchableOpacity>
             <TouchableOpacity
               style={{
                 width: width / 6,
                 height: height / 14,
                 borderRadius: 5,
                 backgroundColor: "white",
                 alignItems: "center",
                 justifyContent: "center",
                 margin: 10
               }}
               onPress={() => {
                 AppCheckIn_CustomerSignUp(this.state.paramcus, "2");
               }}
             >
               <Text style={{ fontSize: 20 }}>Add and Check In</Text>
             </TouchableOpacity>
           </View>*/}
                <Toast
                  style={{ backgroundColor: 'red', }}
                  position="top"
                  ref="toast"
                />
              </View>
            </View>

            <ListPopover
              list={items}
              isVisible={this.state.isVisible}
              onClick={(item) => this.handleChangeText(item, 'birthday_month')}
              onClose={() => this.setState({ isVisible: false, })}
            />

            <ListPopover
              list={itemsdat}
              isVisible={this.state.isVisibleday}
              onClick={(item) => this.handleChangeText(item, 'birthday_day')}
              onClose={() => this.setState({ isVisibleday: false, })}
            />
            {/* </View> */}


            {/* <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.showDiaalog}
              onRequestClose={() => {
              
                this.setState({showDiaalog:false,});
              }}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={{ marginTop: 15,fontSize:22,fontWeight:'500', }}>{this.state.sms}</Text>
                  <TouchableOpacity
                    style={[styles.button, styles.buttonClose,]}
                    onPress={() => {this.setState({showDiaalog:false,});}}
                  >
                    <Text style={styles.textStyle}>AGREE</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal> */}
          </KeyboardAwareScrollView>
           
        </View>
        <ButtonBottom
          txtBtn1="BACK"
          txtBtn2="NEXT"
          onPressBtn1={onPressBtn1}
          onPressBtn2={() => {
            AppCheckIn_CustomerSignUp(this.state.paramcus, '1',sms);
          }}
          containerStyles={{

            height: height/10,
            backgroundColor: '#4F8CB8',
          }}
          containerBtn={{ height: height/10, }}
        ></ButtonBottom>
      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  btn: {
    width: width / 2.4 + 10,
    height: height / 7,
    borderRadius: 5,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    // margin: 10,
    color: Colors.mainColor,
    borderColor: Colors.mainColor,
    borderWidth: 1,
    paddingHorizontal: 20,
    marginRight: 10,
    marginTop: 10,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width:'80%',
    //height:'60%',
    margin: 50,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    margin:30,
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },

  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    //padding: 20,
    margin:20,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});
